
CREATE OR REPLACE VIEW public.mos_trip_v
AS SELECT a.id,
    b.trip_id,
    a.id_order,
    a.asal_id,
    a.tujuan_id,
    c.kota AS pengambilan,
    (c.nama::text || ', '::text) || c.nama_perusahaan::text AS dtlpengambilan,
    d.kota AS pengiriman,
    (d.nama::text || ', '::text) || d.nama_perusahaan::text AS dtlpengiriman
   FROM mt_shipper_order a,
    mt_trip_order_transaction b,
    mt_shipper_contact c,
    mt_shipper_contact d
  WHERE a.id = b.order_id AND c.id = a.asal_id AND d.id = a.tujuan_id;


CREATE OR REPLACE VIEW public.mos_drv_trip_v
AS SELECT a.id,
    a.id_trip,
    a.driver_id,
    CURRENT_DATE - a.tanggal_pengiriman AS selisih_tgl,
    a.tanggal_pengiriman,
    to_char(a.tanggal_pengiriman::timestamp with time zone, 'DD-MON-YYYY'::text) AS format_tgl,
    b.nama AS asal,
    c.nama AS tujuan,
        CASE
            WHEN (CURRENT_DATE - a.tanggal_pengiriman) < '-1'::integer OR (CURRENT_DATE - a.tanggal_pengiriman) > 0 THEN '1'::text
            WHEN f.role::text = 'KERANI'::text THEN '0'::text
            WHEN (( SELECT count(1) AS count
               FROM mt_master_trip
              WHERE mt_master_trip.driver_id = a.driver_id AND f.role::text = 'DRIVER'::text AND mt_master_trip.id <> a.id AND mt_master_trip.status::text = 'accepted'::text)) > 0 THEN '1'::text
            ELSE '0'::text
        END AS statustab,
        CASE
            WHEN (CURRENT_DATE - a.tanggal_pengiriman) < '-1'::integer THEN 'Trip belum dapat diproses'::text
            WHEN (CURRENT_DATE - a.tanggal_pengiriman) > 0 THEN 'Trip sudah kadaluarsa'::text
            WHEN f.role::text = 'KERANI'::text THEN 'Trip dapat diproses'::text
            WHEN (( SELECT count(1) AS count
               FROM mt_master_trip
              WHERE mt_master_trip.driver_id = a.driver_id AND f.role::text = 'DRIVER'::text AND mt_master_trip.id <> a.id AND mt_master_trip.status::text = 'accepted'::text)) > 0 THEN 'Trip lain aktif'::text
            ELSE 'Trip dapat diproses'::text
        END AS statustabtxt,
    to_char(a.jam_pengiriman::interval - '01:00:00'::interval, 'HH24:MI'::text) AS jam_pengiriman,
    d.plat_nomor,
    e.jenis_kendaraan,
    a.kerani_id_asal,
    a.kerani_id_tujuan,
    f.role,
    a.no_container
   FROM mt_master_trip a,
    mt_master_kota b,
    mt_master_kota c,
    mt_master_kendaraan d,
    mt_master_jenis_kendaraan e,
    mt_master_user f
  WHERE a.asal_id = b.id AND a.tujuan_id = c.id AND a.status::text = 'confirm'::text AND a.kendaraan_id = d.id AND a.jenis_kendaraan_id = e.id AND (f.id = a.driver_id AND f.role::text = 'DRIVER'::text OR f.id = a.kerani_id_asal AND f.role::text = 'KERANI'::text);



CREATE OR REPLACE VIEW public.pilihan_order_v
AS SELECT a.trip_id,
    a.order_id,
    b.id_order,
        CASE
            WHEN b.status >= 1 AND b.status <= 5 THEN 'AMBIL'::text
            ELSE 'KIRIM'::text
        END AS halaman,
        CASE
            WHEN b.status > 1 AND b.status <= 5 THEN c.lang
            ELSE d.lang
        END AS lang,
        CASE
            WHEN b.status > 1 AND b.status <= 5 THEN c.lat
            ELSE d.lat
        END AS lat
   FROM mt_trip_order_transaction a,
    mt_shipper_order b,
    mt_shipper_contact c,
    mt_shipper_contact d
  WHERE b.id = a.order_id AND c.id = b.asal_id AND d.id = b.tujuan_id AND b.status < 11;



CREATE OR REPLACE VIEW public.mos_trip_detail_v
AS SELECT b.id,
    a.trip_id,
    h.id_trip,
    a.order_id,
    b.id_order,
    c.nama AS pengambilan,
    c.alamat1 AS addr_pengambilan,
    c.lang AS lang_pengambilan,
    c.lat AS lat_pengambilan,
    d.nama AS pengiriman,
    d.alamat1 AS addr_pengiriman,
    d.lang AS lang_pengiriman,
    d.lat AS lat_pengiriman,
    b.status,
    e.nama AS statusasli,
    e.status_ind AS statustxt,
    f.status_ind AS statusnext,
    b.customer_reference,
    b.remarks,
    b.instruksi,
    b.weight,
    b.dimension,
        CASE
            WHEN (b.status = ANY (ARRAY[2::bigint, 3::bigint])) AND (( SELECT count(1) AS count
               FROM mt_trip_order_transaction aa,
                mt_shipper_order bb
              WHERE aa.trip_id = a.trip_id AND aa.order_id = bb.id AND (bb.status <> ALL (ARRAY[2::bigint, 3::bigint])))) = 0 THEN '/ Batal Pilih Trip ini'::text
            ELSE ''::text
        END AS statusbatal,
    b.jadwal_sampai,
        CASE
            WHEN b.jenis_kendaraan_id = 8 THEN b.jadwal_penjemputan
            ELSE b.jadwal_penjemputan - '01:00:00'::interval
        END AS jadwal_penjemputan,
    to_char(b.jadwal_sampai, 'DD-MON-YYYY HH24:MI'::text) AS txtjadwal_sampai,
        CASE
            WHEN b.jenis_kendaraan_id = 8 THEN to_char(b.jadwal_penjemputan, 'DD-MON-YYYY HH24:MI'::text)
            ELSE to_char(b.jadwal_penjemputan - '01:00:00'::interval, 'DD-MON-YYYY HH24:MI'::text)
        END AS txtjadwal_penjemputan,
    b.startloaded_date,
    b.startunloaded_date,
        CASE
            WHEN b.status = 3 AND b.jenis_kendaraan_id <> 8 THEN b.jadwal_penjemputan - '00:30:00'::interval
            WHEN b.status = 3 AND b.jenis_kendaraan_id = 8 THEN b.jadwal_penjemputan
            WHEN b.status = 4 THEN b.jadwal_penjemputan
            WHEN b.status = 5 AND b.jenis_kendaraan_id <> 8 THEN b.jadwal_penjemputan + b.dimension * '00:01:30'::interval
            WHEN b.status = 6 AND b.jenis_kendaraan_id <> 8 THEN b.jadwal_penjemputan + b.dimension * '00:01:30'::interval + '00:30:00'::interval
            WHEN b.status = 5 AND b.jenis_kendaraan_id = 8 THEN b.jadwal_penjemputan + b.dimension * '00:00:30'::interval
            WHEN b.status = 6 AND b.jenis_kendaraan_id = 8 THEN b.jadwal_penjemputan + b.dimension * '00:00:30'::interval + '00:30:00'::interval
            WHEN b.status = 7 THEN b.jadwal_sampai
            WHEN b.status = 8 AND b.jenis_kendaraan_id <> 8 THEN b.jadwal_sampai + '01:00:00'::interval
            WHEN b.status = 9 AND b.jenis_kendaraan_id <> 8 THEN b.jadwal_sampai + '01:00:00'::interval + b.dimension * '00:01:30'::interval
            WHEN b.status = 8 AND b.jenis_kendaraan_id = 8 THEN b.jadwal_sampai
            WHEN b.status = 9 AND b.jenis_kendaraan_id = 8 THEN b.jadwal_sampai + b.dimension * '00:00:30'::interval
            ELSE NULL::timestamp without time zone
        END AS due_date,
    b.shipment_number,
    g.kode_shipper,
    g.flag_validasi_sc,
    b.token_shipper_contact,
    d.nama AS nama_shipper_contact,
    h.kerani_id_asal,
    h.kerani_id_tujuan,
    b.naikkapal_date,
    b.turunkapal_date,
    h.no_container
   FROM mt_trip_order_transaction a,
    mt_shipper_order b,
    mt_shipper_contact c,
    mt_shipper_contact d,
    mt_shipper_order_status e,
    mt_shipper_order_status f,
    mt_master_shipper g,
    mt_master_trip h
  WHERE g.id = b.shipper_id AND b.id = a.order_id AND c.id = b.asal_id AND d.id = b.tujuan_id AND b.status = e.id AND (b.status + 1) = f.id AND h.id = a.trip_id AND h.status::text = 'accepted'::text;
  