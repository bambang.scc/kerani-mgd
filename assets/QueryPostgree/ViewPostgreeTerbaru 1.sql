create view mos_trip_v as
select a.id,b.trip_id,a.id_order,a.asal_id,a.tujuan_id,c.nama pengambilan,d.nama pengiriman 
from mt_shipper_order a, mt_trip_order_transaction b, mt_master_kota c, mt_master_kota d
where a.id=b.order_id and c.id=a.asal_id and d.id=a.tujuan_id

create view mos_drv_trip_v as
select a.id,a.id_trip,a.driver_id,(current_date-a.tanggal_pengiriman) as selisih_tgl, a.tanggal_pengiriman,
to_char(a.tanggal_pengiriman,'DD-MON-YYYY') format_tgl, b.nama asal, c.nama tujuan,
( case 
  when (current_date-a.tanggal_pengiriman) not between -1 and 0 then '1'
  when (select count(1) from mt_master_trip where driver_id=a.driver_id and id<>a.id and status='accepted')>0 then '1'
  /* planning confirmed process done reject */
  else '0'
  end ) statustab,
( case 
  when (current_date-a.tanggal_pengiriman) < -1 then 'Trip belum dapat diproses'
  when (current_date-a.tanggal_pengiriman) > 0 then 'Trip sudah kadaluarsa'
  when (select count(1) from mt_master_trip where driver_id=a.driver_id and id<>a.id and status='accepted')>0 then 'Trip lain aktif'
  else 'Trip dapat diproses'
  end ) statustabtxt, to_char(tanggal_pengiriman,'HH24:MI') as jam_pengiriman, d.plat_nomor, e.jenis_kendaraan
from mt_master_trip a, mt_master_kota b, mt_master_kota c, mt_master_kendaraan d, mt_master_jenis_kendaraan e
WHERE a.asal_id = b.id AND a.tujuan_id = c.id AND a.status::text = 'confirm'::text AND a.kendaraan_id = d.id AND a.jenis_kendaraan_id = e.id 
AND ((f.id = a.driver_id AND f.role::text = 'DRIVER'::text) OR (f.id = a.kerani_id_asal AND f.role::text = 'KERANI'::text));


create view pilihan_order_v as
select a.trip_id,a.order_Id,b.id_order,
( case when b.status between 1 and 5 then 'AMBIL' else 'KIRIM' end) halaman,
( case when b.status between 1 and 5 then c.lang else d.lang end) lang,
( case when b.status between 1 and 5 then c.lat else d.lat end) lat
from mt_trip_order_transaction a, mt_shipper_order b, mt_shipper_contact c, mt_shipper_contact d
where b.id=a.order_Id and c.id=b.asal_id and d.id=b.tujuan_id

create view mos_trip_detail_v as
select a.trip_id,a.order_Id,b.id_order,
c.nama pengambilan,c.alamat1 addr_pengambilan,
c.lang lang_pengambilan,c.lat lat_pengambilan,
d.nama pengiriman,d.alamat1 addr_pengiriman,
d.lang lang_pengiriman,d.lat lat_pengiriman,b.status, e.nama statusasli,e.status_ind statustxt,f.status_ind statusnext,
b.customer_reference, b.remarks,b.instruksi,b.weight,b.dimension
from mt_trip_order_transaction a, mt_shipper_order b, mt_shipper_contact c, mt_shipper_contact d, mt_shipper_order_status e, mt_shipper_order_status f
where b.id=a.order_Id and c.id=b.asal_id and d.id=b.tujuan_id and b.status=e.id and (b.status+1)=f.id



CREATE OR REPLACE VIEW public.mos_trip_detail_v
AS SELECT b.id,
    a.trip_id,
    h.id_trip,
    a.order_id,
    b.id_order,
    c.nama AS pengambilan,
    c.alamat1 AS addr_pengambilan,
    c.lang AS lang_pengambilan,
    c.lat AS lat_pengambilan,
    d.nama AS pengiriman,
    d.alamat1 AS addr_pengiriman,
    d.lang AS lang_pengiriman,
    d.lat AS lat_pengiriman,
    b.status,
    e.nama AS statusasli,
    e.status_ind AS statustxt,
    f.status_ind AS statusnext,
    b.customer_reference,
    b.remarks,
    b.instruksi,
    b.weight,
    b.dimension,
        CASE
            WHEN (b.status = ANY (ARRAY[2::bigint, 3::bigint])) AND (( SELECT count(1) AS count
               FROM mt_trip_order_transaction aa,
                mt_shipper_order bb
              WHERE aa.trip_id = a.trip_id AND aa.order_id = bb.id AND (bb.status <> ALL (ARRAY[2::bigint, 3::bigint])))) = 0 THEN '/ Batal Pilih Trip ini'::text
            ELSE ''::text
        END AS statusbatal,
    b.jadwal_sampai,
        CASE
            WHEN b.jenis_kendaraan_id = 8 THEN b.jadwal_penjemputan
            ELSE b.jadwal_penjemputan - '01:00:00'::interval
        END AS jadwal_penjemputan,
    to_char(b.jadwal_sampai, 'DD-MON-YYYY HH24:MI'::text) AS txtjadwal_sampai,
        CASE
            WHEN b.jenis_kendaraan_id = 8 THEN to_char(b.jadwal_penjemputan, 'DD-MON-YYYY HH24:MI'::text)
            ELSE to_char(b.jadwal_penjemputan - '01:00:00'::interval, 'DD-MON-YYYY HH24:MI'::text)
        END AS txtjadwal_penjemputan,
    b.startloaded_date,
    b.startunloaded_date,
        CASE
            WHEN b.status = 3 AND b.jenis_kendaraan_id <> 8 THEN b.jadwal_penjemputan - '00:30:00'::interval
            WHEN b.status = 3 AND b.jenis_kendaraan_id = 8 THEN b.jadwal_penjemputan
            WHEN b.status = 4 THEN b.jadwal_penjemputan
            WHEN b.status = 5 AND b.jenis_kendaraan_id <> 8 THEN b.jadwal_penjemputan + b.dimension * '00:01:30'::interval
            WHEN b.status = 6 AND b.jenis_kendaraan_id <> 8 THEN b.jadwal_penjemputan + b.dimension * '00:01:30'::interval + '00:30:00'::interval
            WHEN b.status = 5 AND b.jenis_kendaraan_id = 8 THEN b.jadwal_penjemputan + b.dimension * '00:00:30'::interval
            WHEN b.status = 6 AND b.jenis_kendaraan_id = 8 THEN b.jadwal_penjemputan + b.dimension * '00:00:30'::interval + '00:30:00'::interval
            WHEN b.status = 7 THEN b.jadwal_sampai
            WHEN b.status = 8 AND b.jenis_kendaraan_id <> 8 THEN b.jadwal_sampai + '01:00:00'::interval
            WHEN b.status = 9 AND b.jenis_kendaraan_id <> 8 THEN b.jadwal_sampai + '01:00:00'::interval + b.dimension * '00:01:30'::interval
            WHEN b.status = 8 AND b.jenis_kendaraan_id = 8 THEN b.jadwal_sampai
            WHEN b.status = 9 AND b.jenis_kendaraan_id = 8 THEN b.jadwal_sampai + b.dimension * '00:00:30'::interval
            ELSE NULL::timestamp without time zone
        END AS due_date,
    b.shipment_number,
    g.kode_shipper,
    g.flag_validasi_sc,
    b.token_shipper_contact,
    d.nama AS nama_shipper_contact,
    h.kerani_id_asal
   FROM mt_trip_order_transaction a,
    mt_shipper_order b,
    mt_shipper_contact c,
    mt_shipper_contact d,
    mt_shipper_order_status e,
    mt_shipper_order_status f,
    mt_master_shipper g,
    mt_master_trip h
  WHERE g.id = b.shipper_id AND b.id = a.order_id AND c.id = b.asal_id AND d.id = b.tujuan_id AND b.status = e.id AND (b.status + 1) = f.id AND h.id = a.trip_id
     and h.status='accepted';

