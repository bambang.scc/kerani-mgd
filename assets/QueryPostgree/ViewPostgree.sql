create view mos_trip_v as
select a.id,b.trip_id,a.id_order,a.asal_id,a.tujuan_id,c.nama pengambilan,d.nama pengiriman 
from mt_shipper_order a, mt_trip_order_transaction b, mt_master_kota c, mt_master_kota d
where a.id=b.order_id and c.id=a.asal_id and d.id=a.tujuan_id

create view mos_drv_trip_v as
select a.id,a.id_trip,a.driver_id,(current_date-a.tanggal_pengiriman) as selisih_tgl, a.tanggal_pengiriman,
to_char(a.tanggal_pengiriman,'DD-MON-YYYY') format_tgl, b.nama asal, c.nama tujuan,
( case 
  when (current_date-a.tanggal_pengiriman) not between -1 and 0 then '1'
  when (select count(1) from mt_master_trip where driver_id=a.driver_id and id<>a.id and status='accepted')>0 then '1'
  /* planning confirmed process done reject */
  else '0'
  end ) statustab,
( case 
  when (current_date-a.tanggal_pengiriman) < -1 then 'Trip belum dapat diproses'
  when (current_date-a.tanggal_pengiriman) > 0 then 'Trip sudah kadaluarsa'
  when (select count(1) from mt_master_trip where driver_id=a.driver_id and id<>a.id and status='accepted')>0 then 'Trip lain aktif'
  else 'Trip dapat diproses'
  end ) statustabtxt, to_char(tanggal_pengiriman,'HH24:MI') as jam_pengiriman, d.plat_nomor, e.jenis_kendaraan
from mt_master_trip a, mt_master_kota b, mt_master_kota c, mt_master_kendaraan d, mt_master_jenis_kendaraan e
where a.asal_id=b.id and a.tujuan_id=c.id and a.status='confirm' and a.kendaraan_id=d.id and a.jenis_kendaraan_id = e.id


create view pilihan_order_v as
select a.trip_id,a.order_Id,b.id_order,
( case when b.status between 1 and 5 then 'AMBIL' else 'KIRIM' end) halaman,
( case when b.status between 1 and 5 then c.lang else d.lang end) lang,
( case when b.status between 1 and 5 then c.lat else d.lat end) lat
from mt_trip_order_transaction a, mt_shipper_order b, mt_shipper_contact c, mt_shipper_contact d
where b.id=a.order_Id and c.id=b.asal_id and d.id=b.tujuan_id

create view mos_trip_detail_v as
select a.trip_id,a.order_Id,b.id_order,
c.nama pengambilan,c.alamat1 addr_pengambilan,
c.lang lang_pengambilan,c.lat lat_pengambilan,
d.nama pengiriman,d.alamat1 addr_pengiriman,
d.lang lang_pengiriman,d.lat lat_pengiriman,b.status, e.nama statusasli,e.status_ind statustxt,f.status_ind statusnext,
b.customer_reference, b.remarks,b.instruksi,b.weight,b.dimension
from mt_trip_order_transaction a, mt_shipper_order b, mt_shipper_contact c, mt_shipper_contact d, mt_shipper_order_status e, mt_shipper_order_status f
where b.id=a.order_Id and c.id=b.asal_id and d.id=b.tujuan_id and b.status=e.id and (b.status+1)=f.id



