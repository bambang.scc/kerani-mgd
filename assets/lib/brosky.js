import React, {Component} from 'react';
import {Modal, Text, TouchableOpacity, View} from 'react-native';
import AesCrypto from 'react-native-aes-kit';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import CryptoJS from "crypto-js";

const secretKey = '(1xkfh*2.zl^23-]';
const iv = "43i9{tdk'14045xw";

export const encrypt = (data) => new Promise((resolve)=>{
    AesCrypto.encrypt(data,secretKey, iv).then(cipher=>{
        resolve(cipher)
    }).catch(err=>{
        resolve(false)
    });
});
export const decrypt = (data) => new Promise((resolve)=>{
    AesCrypto.decrypt(data,secretKey, iv).then(plaintext=>{
        resolve(plaintext)
    }).catch(err=>{
        resolve(false)
    });
});

export const setUrl = () => new Promise(async (resolve)=>{
    console.log('set url start');
    console.log(version);
    // let url = 'https://mostrans.co.id/apimobile/';
    // if (version.type === 'dev') url += 'testing';
    let url = 'https://mostrans.co.id/apimobile';
    if (version.type === 'dev') url += 'testing/';
    console.log(url);
    let encrypted_response = await Axios.get(url).catch(err=>{
        console.log('error get dari axios');
        console.log(err);
    });
    let decrypted_response = await decrypt(encrypted_response.data);
    console.log("encrypted_response.data:" + encrypted_response.data);
    console.log("decrypted_response" + decrypted_response);
    // let decrypted_response = 'https://iby4j7r8gk.execute-api.ap-southeast-1.amazonaws.com/default/testing_select';
    await AsyncStorage.setItem('url', decrypted_response).then(value=>{
        
    });
    resolve('success');
})

export const getUrl = () => new Promise(async (resolve, reject)=>{
    console.log('getUrl()');
    await AsyncStorage.getItem('url').then(value=>{
        console.log('berhasil get url');
        resolve(JSON.parse(value));
    }).catch(err=>{
        console.log('gagal get url');
        console.log(err);
        reject('error');
    })
})

export const version = {
    app: 'KERANI', //DRIVER/KERANI
    version: '2.4',
    type: 'dev' // prod | dev
}

export default class Announcement extends Component{
    componentDidMount(){
        alert('masuk');
    }
    render(){
        return(
            <View>
                <Text>12312312312312312312312312</Text>
            </View>
        )
    }
}


export const pointencrypt=(text)=>{
    let key='redeemmostrans';
    key = CryptoJS.MD5(key).toString();
    const k1 = key.substring(0, 16);
    key = key + k1;
    const textWordArray = CryptoJS.enc.Utf8.parse(text);
    const keyHex = CryptoJS.enc.Hex.parse(key);
    
    const encrypted = CryptoJS.TripleDES.encrypt(textWordArray, keyHex, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    });
    const base64String = encrypted.toString();
    return base64String;
  }

export const pointdecrypt=(base64String)=>{
    let key='redeemmostrans';
    key = CryptoJS.MD5(key).toString();
    const k1 = key.substring(0, 16);
    key = key + k1;

    const keyHex = CryptoJS.enc.Hex.parse(key);

    const decrypted = CryptoJS.TripleDES.decrypt( {
        ciphertext: CryptoJS.enc.Base64.parse(base64String)
    }, keyHex, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    });

    return decrypted.toString(CryptoJS.enc.Utf8);
  }