import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Footer,
  FooterTab,
  Left,
  Right,
  Body,
  Badge,
  Grid,
  Col,
  Card,
  CardItem, Tabs, Tab, TabHeading, ScrollableTab, List, ListItem
} from "native-base";
import { View, Image } from 'react-native';

class TripView extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Container style={{ backgroundColor: "#ffffff" }}>
        <Header style={{ backgroundColor: "#008C45" }}>
          <Body>
            <Title>
              MOSTrans - Trip Driver</Title>
          </Body>
          <Right />
        </Header>

      </Container>
    );
  }
}

export default TripView;
