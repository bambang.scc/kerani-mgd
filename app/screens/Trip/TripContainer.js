import React, { Component } from 'react';
import TripView from './TripView';
import { connect } from 'react-redux';

class TripContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <TripView {...this.props} />;
    }
}

function mapStateToProps() {
    return {};
}
function mapDispatchToProps(dispatch) {
    return {};
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TripContainer);
