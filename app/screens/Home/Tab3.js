import React, { Component } from 'react'
import {
   StyleSheet,
   View,
   ActivityIndicator,
   FlatList,
   Text, TextInput,
   TouchableOpacity, ListView,Modal, TouchableHighlight,Alert, PermissionsAndroid, Platform, Linking,
   Image, ScrollView, AppRegistry
   } from "react-native";
   import AsyncStorage from '@react-native-community/async-storage';
import {
   Button, Icon, Spinner
} from "native-base";
import {encrypt, decrypt, getUrl, version} from '../../../assets/lib/brosky';
import Axios from 'axios';
import styles2 from './styles';
import {ToastAndroid} from 'react-native';
// @ts-ignore
import { CameraKitCameraScreen } from 'react-native-camera-kit';
import firebase from 'react-native-firebase';
import QRCode from 'react-native-qrcode';
import ImageResizer from 'react-native-image-resizer';
import SignatureCapture from 'react-native-signature-capture';

let url;
let UrutPhoto = '';

const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

// let aa="";
// let ab="";
// let ac="";
// let ad="";
// let ae="";
// let af="";

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
let myuid = 0;
let ttdShipper = '';
// let mylatitude = 0;
// let mylongitude = 0;

const kompressimage = (data) => new Promise((resolve)=>{
  ImageResizer.createResizedImage(data, 650, 650, 'JPEG', 85, 0, null)
  .then(response => {
    resolve(response.path);
  })
  .catch(err => {
    resolve(event.captureImages[0].uri);
  });
});

export default class Tab1 extends React.Component {

   constructor(props) {
    super(props);
    this.state = {
      loading: true,
      dataSource:ds,
      keyid: '0',
      latitude: null,
      longitude: null,
      // keyid: 0,
      error: null,
      modalVisible: false,
      myuidd: '0',
      myvalue: '0',
      isPermitted: false,
      isLoading: true,
      modalisLoading: false,
      urifoto1: '',
      urlfoto1: '',
      urifoto2: '',
      urlfoto2: '',
      urifoto3: '',
      urlfoto3: '',
      urifoto4: '',
      urlfoto4: '',
      urifoto5: '',
      urlfoto5: '',
      QRCodeSimantra: '',
      modalQRCodeSimantra: false,
      urifoto1A: '',
      urlfoto1A: '',
      isPermitted: false,
      selesaiCheck: false,
      nilaiBiayaKuli: '',
      tokenShipper:'',
      urlfotottd:''
    //   curTrip: 0,
        };
   }


   componentWillUnmount(){
     clearInterval(this.interval)
   }


   clearVar(){
    this.setState({nilaiBiayaKuli:''});
    this.setState({urifoto1A:''});
    this.setState({urlfoto1A:''});
    this.setState({ selesaiCheck : false });
    this.setState({ tokenShipper : '' });
    ttdShipper="";
  
  }
  


  onPressTanya(UrutPhotox){
    if (this.state.nilaiBiayaKuli == ""){
      Alert.alert('Mostrans','Isi dahulu Biaya Kuli');
    }
    else 
    {
    Alert.alert(
      'Mostrans',
      'Simpan Biaya Kuli dan Lanjutkan dengan photo Kwitansi?',
      [
        {
          text: 'Batal',
          onPress: () => this.setState({ selesaiCheck : false }),
          style: 'cancel',
        },
        {text: 'Ya', onPress: () => {
          this.setState({ selesaiCheck : true });
          this.onPress(UrutPhotox);
                }
                },
      ],
      {cancelable: false},
    );
    }
   }



   onGetCodeSimantra(nshipmentNumber){
    this.setState({QRCodeSimantra : nshipmentNumber});
    this.setState({modalQRCodeSimantra:true});
   }
   TutupSimantra(){
    this.setState({modalQRCodeSimantra:false});
  }

  // if (statusjrk=="" || version.type=='dev') {

  async _trip(visible,kunciid,statusjrk){
    if (statusjrk=="" ) {
    this.setState({keyid: String(kunciid)});
    this.setState({modalVisible: visible});
    }
  };

  async _triptutup(visible,kunciid,statusjrk){
    this.setState({keyid: String(kunciid)});
    this.setState({modalVisible: visible});
    this.clearVar();
  };

  async _tripproses(visible,kunciid,statusid,trip_id,token_shipper_contact,flag_validasi_sc){

    //alert(ttdShipper);

    if( statusid == 9 && this.state.nilaiBiayaKuli == '') {
      Alert.alert('Biaya Kuli', 'Biaya Kuli belum diisi. Isi 0 jika Biaya Kuli tidak ada'); // + kunciid);
    }
    else if( statusid == 9 && this.state.nilaiBiayaKuli != 0 && this.state.urifoto1A == '') {
      Alert.alert('Foto Kwitansi Belum Ada', 'Silahkan Click pada gambar kamera untuk memfoto Kwitansi Biaya Kuli'); // + kunciid);
    }
    else if( statusid == 9 && flag_validasi_sc=='Y' && this.state.tokenShipper == '') {
      Alert.alert('Token Belum Diisi', 'Silahkan meminta Petugas Shipper Contact untuk mengisi Token'); // + kunciid);
    }
    else if( statusid == 9 && flag_validasi_sc=='Y' && this.state.tokenShipper != token_shipper_contact) {
      Alert.alert('Token Salah', 'Silahkan meminta Petugas Shipper Contact untuk mengisi Token yang sesuai'); // + kunciid);
    }
    else if( statusid == 9 && flag_validasi_sc=='Y' && ttdShipper == '') {
      Alert.alert('Belum Ditanda Tangani', 'Silahkan meminta Petugas Shipper Contact untuk melakukan tanda tangan'); // + kunciid);
    }
    else{    

    // this.props.navigation.navigate('Trip');
    // this.props.navigation.replace('Trip');
    this._tripstatus(kunciid,statusid,trip_id);
    // this._order(kunciid);
    this.setState({keyid: String(kunciid)});
    this.queryUtama(trip_id);
    //test brosky
    //this.setState({modalVisible: visible});
    // this.componentDidMount();
    // alert("test");
    if(statusid !== '10'){
      this.setState({modalVisible: false});  
    };

  };


  };

  uploadImage = (uri, id_order, urutphoto)=> new Promise((resolve, reject)=>{
    // console.log('start');

      const imageRef1 = firebase.storage().ref('FotoPOD/').child(id_order+'_'+urutphoto);
      imageRef1.putFile(uri);
      imageRef1.getDownloadURL().then(async (result)=>{
        if (urutphoto=='1') {        
          this.setState({urlfoto1: result}); 
        }
        else if (urutphoto=='2') {        
          this.setState({urlfoto2: result}); 
        }
        else if (urutphoto=='3') {        
          this.setState({urlfoto3: result}); 
        }
        else if (urutphoto=='4') {        
          this.setState({urlfoto4: result}); 
        }
        else if (urutphoto=='5') {        
          this.setState({urlfoto5: result}); 
        }
        resolve(result);
      })
      .catch(err=>{
          reject(result);
        })        
    // else if (urutphoto=='2') {        
    //   const imageRef1 = firebase.storage().ref('FotoPOD/').child(id_order+'_2');
    //   imageRef1.putFile(uri);
    //   imageRef1.getDownloadURL().then(async (result)=>{
    //     this.setState({urlfoto2: result}); 
    //     resolve(result);
    //   })
    //   .catch(err=>{
    //       reject(result);
    //     })        
    // }
    // else if (urutphoto=='3') {        
    //   const imageRef1 = firebase.storage().ref('FotoPOD/').child(id_order+'_3');
    //   imageRef1.putFile(uri);
    //   imageRef1.getDownloadURL().then(async (result)=>{
    //     this.setState({urlfoto3: result}); 
    //     resolve(result);
    //   })
    //   .catch(err=>{
    //       reject(result);
    //     })        
    // }
    // else if (urutphoto=='4') {        
    //   const imageRef1 = firebase.storage().ref('FotoPOD/').child(id_order+'_4');
    //   imageRef1.putFile(uri);
    //   imageRef1.getDownloadURL().then(async (result)=>{
    //     this.setState({urlfoto4: result}); 
    //     resolve(result);
    //   })
    //   .catch(err=>{
    //       reject(result);
    //     })        
    // }
    // else if (urutphoto=='5') {        
    //   const imageRef1 = firebase.storage().ref('FotoPOD/').child(id_order+'_5');
    //   imageRef1.putFile(uri);
    //   imageRef1.getDownloadURL().then(async (result)=>{
    //     this.setState({urlfoto5: result}); 
    //     resolve(result);
    //   })
    //   .catch(err=>{
    //       reject(result);
    //     })        
    // }


  })


  uploadImage1 = (uri, id_order)=> new Promise((resolve, reject)=>{
    // console.log('start');
    const imageRef = firebase.storage().ref('FotoPOD/').child(id_order+'_1');
    imageRef.putFile(uri);
    let icount=0;
    for (icount = 0; icount < 30; icount++){    
    imageRef.getDownloadURL().then(async (result)=>{
      this.setState({urlfoto1: result});
      resolve(result);
      icount=31;
    })
    .catch(err=>{
      reject(result);
    })
    this.timehold();
    }
  })
  uploadImage2 = (uri, id_order)=> new Promise((resolve, reject)=>{
    // console.log('start');
    const imageRef = firebase.storage().ref('FotoPOD/').child(id_order+'_2');
    imageRef.putFile(uri);
    let icount=0;
    for (icount = 0; icount < 30; icount++){    
    imageRef.getDownloadURL().then(async (result)=>{
      this.setState({urlfoto2: result});
      resolve(result);
      icount=31;
    })
    .catch(err=>{
      reject(result);
    })
    this.timehold();
    }
  })
  uploadImage3 = (uri, id_order)=> new Promise((resolve, reject)=>{
    // console.log('start');
    const imageRef = firebase.storage().ref('FotoPOD/').child(id_order+'_3');
    imageRef.putFile(uri);
    let icount=0;
    for (icount = 0; icount < 30; icount++){    
    imageRef.getDownloadURL().then(async (result)=>{
      this.setState({urlfoto3: result});
      resolve(result);
      icount=31;
    })
    .catch(err=>{
      reject(result);
    })
    this.timehold();
    }
  })
  uploadImage4 = (uri, id_order)=> new Promise((resolve, reject)=>{
    // console.log('start');
    const imageRef = firebase.storage().ref('FotoPOD/').child(id_order+'_4');
    imageRef.putFile(uri);
    let icount=0;
    for (icount = 0; icount < 30; icount++){    
    imageRef.getDownloadURL().then(async (result)=>{
      this.setState({urlfoto4: result});
      resolve(result);
      icount=31;
    })
    .catch(err=>{
      reject(result);
    })
    this.timehold();
    }
  })
  uploadImage5 = (uri, id_order)=> new Promise((resolve, reject)=>{
    // console.log('start');
    const imageRef = firebase.storage().ref('FotoPOD/').child(id_order+'_5');
    imageRef.putFile(uri);
    let icount=0;
    for (icount = 0; icount < 30; icount++){    
    imageRef.getDownloadURL().then(async (result)=>{
      this.setState({urlfoto5: result});
      resolve(result);
      icount=31;
    })
    .catch(err=>{
      reject(result);
    })
    this.timehold();
    }
  })

  uploadImagettd = (uri, id_order)=> new Promise((resolve, reject)=>{
    console.log('start ttd: ');
    // aa='uploadImagettd 1:'+uri;
    // ab='uploadImagettd 2:'+id_order;
  
    const imageRef = firebase.storage().ref('FotoPOD/').child(id_order+'_ttd');
    imageRef.putFile(uri);
    let icount=0;
    for (icount = 0; icount < 30; icount++){  
      console.log('icount='+icount);
    imageRef.getDownloadURL().then(async (result)=>{
      this.setState({urlfotottd: result});
      resolve(result);
      console.log('result image ttd:'+result);
      icount=31;
    })
    .catch(err=>{
      // alert('Harusnya message error!')
      // alert(err.message);
      console.log('errorimage ttd: '+err);
      reject(result);
    })
    // ac='uploadImagettd 2:'+this.state.urlfotottd;
    this.timehold();
    }  
    console.log('end ttd: ');
  })



  async _tripstatus(kunciid,statusid,trip_id){

// alert('1');

this.setState({modalisLoading: true});
// alert('update mt_shipper_order set status=(status+1) where status='+statusid+' and id='+kunciid);
this.setState({ isLoading: true});
let mySQLstatus = "";
let mySQLstring = "";

if(statusid === '10' && this.state.urifoto1 == '') {
  this.setState({modalisLoading: false});

  Alert.alert('Foto Belum Ada', 'Silahkan Click pada gambar kamera untuk melanjutkan'); // + kunciid);
}
else{
  if(statusid === '10'){



    // console.log('file://' + this.state.urifoto1);
    //this.setState({modalisLoading: false});    

//ditutup dulu pindah ke kamera    
    await this.uploadImage1('file://' + this.state.urifoto1, trip_id).then(result=>{
      // console.log(result);
    });
    if (this.state.urifoto2!=''){
      await this.uploadImage2('file://' + this.state.urifoto2, trip_id).then(result=>{
        // console.log(result);
      });
    }
    if (this.state.urifoto3!=''){
      await this.uploadImage3('file://' + this.state.urifoto3, trip_id).then(result=>{
        // console.log(result);
      });
    }
    if (this.state.urifoto4!=''){
      await this.uploadImage4('file://' + this.state.urifoto4, trip_id).then(result=>{
        // console.log(result);
      });
    }
    if (this.state.urifoto5!=''){
      await this.uploadImage5('file://' + this.state.urifoto5, trip_id).then(result=>{
        // console.log(result);
      });
    }
//ditutup dulu pindah ke kamera    




    // console.log('diluar');
    this.setState({modalVisible: false});
  }
  
let mystatusnext = Number(statusid,10)+1;
// alert('mystatusnext  '+mystatusnext);
if (mystatusnext==3) {
    mySQLstatus = "accepted" ;
    // alert('3');
} else if (mystatusnext==4) {
  mySQLstatus = "arrival" ;
  // alert('4');
}  else if (mystatusnext==5) {
  mySQLstatus = "startloaded" ;
}  else if (mystatusnext==6) {
  mySQLstatus = "finishloaded" ;
}  else if (mystatusnext==7) {
  mySQLstatus = "ontheway" ;
}  else if (mystatusnext==8) {
  mySQLstatus = "dropoff" ;
}  else if (mystatusnext==9) {
  mySQLstatus = "startunloaded" ;
}  else if (mystatusnext==10) {
  mySQLstatus = "finishunloaded" ;
}  else if (mystatusnext==11) {
  mySQLstatus = "POD" ;
} 

if (this.state.nilaiBiayaKuli == 0 || statusid != 9 ) {
  this.setState({ nilaiBiayaKuli: "0"});
   this.setState({ urlfoto1A: ""});
}
else if (this.state.urifoto1A == "") { 
  this.setState({ urlfoto1A: ""});
}
else {
// pindah ke kamera
await this.uploadImage1A('file://' + this.state.urifoto1A, kunciid).then(result=>{
  // console.log(result);
});      
// pindah ke kamera
// null;
}

// dipindah ke kamera
console.log('tracking ttd 1  = '+statusid);
if (statusid==9) {
  console.log('tracking ttd 2');
  if (ttdShipper!=''){
    console.log('tracking ttd 3');
    await this.uploadImagettd('file://' + ttdShipper, trip_id).then(result=>{
      // console.log(result);
      null;
    });
    console.log('tracking ttd 4');
  }
} 
console.log('tracking ttd 5');
// dipindah ke kamera

// alert('mySQLstatus '+mySQLstatus);

if (mySQLstatus!="") {
  // alert('finish mystatusnext'+mystatusnext);
  if (mystatusnext==3) {
     mySQLstring = ","+mySQLstatus+"_date=NOW(),"+mySQLstatus+"_by="+this.state.myuidd+","+
           mySQLstatus+"_longitude="+this.state.longitude+","+
           mySQLstatus+"_latitude="+this.state.latitude;
  } else if (mystatusnext==11) {
    mySQLstring = ","+mySQLstatus+"_date=NOW()";
   } else {
    mySQLstring = ","+mySQLstatus+"_date=NOW(),"+
           mySQLstatus+"_longitude="+this.state.longitude+","+
           mySQLstatus+"_latitude="+this.state.latitude;
  }
}



    let myquery = "";
    let thisquery = "";
    thisquery = "update mt_shipper_order set dokumen_pod='"+this.state.urlfoto1+"', "
    if (this.state.urlfoto2 != '') {
      thisquery = thisquery.concat( " dokumen_pod_2='"+this.state.urlfoto2+"', " )
    }
    if (this.state.urlfoto3 != '') {
      thisquery = thisquery.concat( " dokumen_pod_3='"+this.state.urlfoto3+"', ")
    }
    if (this.state.urlfoto4 != '') {
      thisquery = thisquery.concat(" dokumen_pod_4='"+this.state.urlfoto4+"', ")
    }
    if (this.state.urlfoto5 != '') {
      thisquery = thisquery.concat(" dokumen_pod_5='"+this.state.urlfoto5+"', ")
    }
    if (this.state.urlfotottd != '') {
      thisquery = thisquery.concat(" ttd_shipper_contact='"+this.state.urlfotottd+"', ")
    }


// alert('statusid'+statusid)    ;

    if ( statusid == 9) { 
    thisquery = thisquery.concat(" status=(status+1)"+mySQLstring+',biaya_kuli_bongkar='+this.state.nilaiBiayaKuli+",kwitansi_kuli_bongkar='"+this.state.urlfoto1A+"' where status="+statusid+' and id='+kunciid )
    } else
    {
      thisquery = thisquery.concat(" status=(status+1)"+mySQLstring+' where status='+statusid+' and id='+kunciid )
    }
    
    // alert(thisquery);
    // console.log('this === '+thisquery);
// alert(thisquery);

    await encrypt(thisquery).then((result)=>{
myquery = result;
// console.log('update mt_shipper_order set dokumen_pod='+this.state.urlfoto1+', status=(status+1)'+mySQLstring+' where status='+statusid+' and id='+kunciid);
});
// console.log("== query 1 ==");
// console.log(myquery);
// console.log("mystatusnext=" + mystatusnext);
//test brosky
Axios.post(url.select,{
query: myquery
})
.then((response)=>{

  if (mystatusnext==11) {
    this._pilihanorder(kunciid,trip_id);
    this.setState({ isLoading: false});
    }

  let temp = response.data;
//  console.log(response.data);
})
.catch(error=>{
//  console.log("error+7++++++++++++++++++++++++++++++"+error);
 this.setState({modalisLoading: false});
});

// alert('mystatusnext '+mystatusnext);

// let myquery = "";
//  alert("insert into mt_log_location (lat,lang,trip_id,create_date,create_by,update_date,update_by) values ("+this.state.latitude+","+this.state.longitude+","+mycurTrip+",NOW(),"+this.state.myuidd+",NOW(),"+this.state.myuidd+")");
// alert('tab3 savetitik begin')
await encrypt("insert into mt_log_location (lat,lang,trip_id,create_date,create_by,update_date,update_by,keterangan,driver_id) values ("+this.state.latitude+","+this.state.longitude+","+trip_id+",NOW(),"+this.state.myuidd+",NOW(),"+this.state.myuidd+",'TAB3',"+this.state.myuidd+")" ).then((result)=>{
   myquery = result;
});
Axios.post(url.select,{
   query: myquery
 })
 .then((response)=>{
    let temp = response.data;
    //console.log(response.data);
 })
 .catch(error=>{
    // console.log("error+8++++++++++++++++++++++++++++++"+error);
    // alert('tab3 savetitik '+error)
    null;
 });
//  alert('tab3 savetitik end')


if (mystatusnext==7 || mystatusnext==8) {
  let data1 = {
    "id" : kunciid,
    "status" : mystatusnext
  };
  // alert(JSON.stringify(data1));  
  try {
    let response = fetch(
       "https://mostrans.co.id/NotifKontak/sendNotif",
           {
             method: "POST",
             headers: {
               "Accept": "application/json",
               "Content-Type": "application/json; charset=utf-8"
               },
                body: JSON.stringify(data1)
             }
             );
 } catch (errors) {   
 alert(errors);
 } 
};





this.setState({urifoto1: ''});
this.setState({urlfoto1: ''});
this.setState({urifoto2: ''});
this.setState({urlfoto2: ''});
this.setState({urifoto3: ''});
this.setState({urlfoto3: ''});
this.setState({urifoto4: ''});
this.setState({urlfoto4: ''});
this.setState({urifoto5: ''});
this.setState({urlfoto5: ''});

if (mystatusnext==11) {

  let ncurtrip = 0;
  this._settripid(this.state.myuidd);
  await AsyncStorage.getItem("curTrip",(error,value)=>{
    if(value!=null)  ncurtrip = value ; 
    else { ncurtrip = 0 }});

// alert("update mt_master_trip a set status='close', update_date=now() where a.status='accepted' and a.id="+ncurtrip+" and ( select count(1) from mt_trip_order_transaction aa,mt_shipper_order bb where aa.trip_id=a.id and aa.order_id=bb.id and bb.status<11 and bb.id<>"+kunciid+" )=0");
//myquery = "";
await encrypt("update mt_master_trip a set status='close', update_date=now() where a.status='accepted' and a.id="+ncurtrip+" and ( select count(1) from mt_trip_order_transaction aa,mt_shipper_order bb where aa.trip_id=a.id and aa.order_id=bb.id and bb.status<11 and bb.id<>"+kunciid+" )=0" ).then((result)=>{
myquery = result;
});
// console.log("== query 2 ==");
// console.log("update mt_master_trip a set status='close', update_date=now() where a.status='accepted' and a.id="+this.state.myvalue+" and ( select count(1) from mt_trip_order_transaction aa,mt_shipper_order bb where aa.trip_id=a.id and aa.order_id=bb.id and bb.status<11 and bb.id<>"+kunciid+" )=0" );
// console.log(myquery);
//test brosky
Axios.post(url.select,{
query: myquery
})
.then((response)=>{
 let temp = response.data;
//  console.log("ini query 2 success"); 
//  console.log(response.data);
 this.setState({modalisLoading: false});

 Alert.alert(
  'Petunjuk',
  'Trip ini sudah lengkap diproses, silahkan memilih Trip lain pada halaman TRIP',
  [
    {text: 'OK', onPress: () => console.log('OK Pressed')},
  ],
  {cancelable: false},
);


})
.catch(error=>{
//   console.log("ini query 2 error");
//  console.log("error+++++++++++++++++++++++++++++++"+error);
 this.setState({modalisLoading: false});
});
}

}//tutup else

this.clearVar();

};


async _pilihanorder(order_id,trip_id){

  let myquery = "";
  await encrypt("select id_order,halaman,jarak from (select z.* from (select trip_id,order_id,id_order,halaman, round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+this.state.longitude+" "+this.state.latitude+")',4326),ST_GeomFromText('POINT(' || lang || ' ' || " +
  "lat || ')',4326)) As numeric)/1000,2) jarak, ( case when order_id="+order_id+" then 0 else 1 end) urut_order  from pilihan_order_v) z where trip_id="+trip_id+"  order by z.jarak,z.urut_order) zz limit 1"
  ).then((result)=>{
     myquery = result;
  });

  Axios.post(url.select,{
     query: myquery
   })
   .then((response)=>{
    let temp = response.data;
    // this.setState({curTrip: temp.data[0].id});

    Alert.alert(
      'Petunjuk',
      'Langkah selanjutnya yang disarankan adalah memproses Order# '+temp.data[0].id_order
      +' dengan jarak '+temp.data[0].jarak+" KM pada halaman "+temp.data[0].halaman,
      [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );

    // AsyncStorage.setItem("curTrip", temp.data[0].id, ()=>{
    //   console.log('xxxx');
    // });
  })
  .catch(error=>{
    // this.setState({curTrip: 0});

    // alert(error.Number);
    if (error.message=="undefined is not an object (evaluating 'temp.data[0].id_order')") {
    Alert.alert(
      'Petunjuk',
      'Trip ini sudah lengkap diproses, silahkan memilih Trip lain pada halaman TRIP',
      [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
    }

    AsyncStorage.setItem("curTrip", '0', ()=>{
      
    });
  });

}



    async _settripid(kunciid){

      // alert("select id from mt_master_trip where driver_id="+kunciid+" and status='accepted' limit 1");

      // console.log("select id from mt_master_trip where driver_id="+kunciid+" and status='accepted' limit 1");

      let myquery = "";
      await encrypt("select id from mt_master_trip where driver_id="+kunciid+" and status='accepted' limit 1" ).then((result)=>{
         myquery = result;
      });

      Axios.post(url.select,{
         query: myquery
       })
       .then((response)=>{
        let temp = response.data;
        // this.setState({curTrip: temp.data[0].id});

        AsyncStorage.setItem("curTrip", temp.data[0].id, ()=>{
          
        });
      })
      .catch(error=>{


        // alert(error.message);
        if (error.message=="undefined is not an object (evaluating 'n.data[0].id')") {
        this.setState({curTrip: 0});
        AsyncStorage.setItem("curTrip", '0', ()=>{          
        });
        }

      });

    };


   async queryUtama(myvalue){
    this.setState({ isLoading: true});
    // let myquery = "select z.*, "+
    // "(case when status=6 then jarak2::text else ' ' end) infojarak2,(case when status=6 then 'KM' else ' ' end) infokm2,"+
    // "(case when (jarak>0.5 and (status+1) in (4,5,6,8,9,10)) then 'Posisi Anda belum berada di lokasi' "+
    // " when (status=5 and NOW() < (startloaded_date + gettimeloading(id) * interval '1 minutes')) then 'Waktu tunggu proses muat sedang berlangsung' " +
    // " when (status=9 and NOW() < (startunloaded_date + gettimeunloading(id) * interval '1 minutes')) then 'Waktu tunggu proses bongkar sedang berlangsung' " +
    // " when (jarak2>1 /*0.3*/ and (status+1) in (7)) then 'Posisi Anda sudah menjauh dari lokasi pengambilan' "+
    // " else '' end) as statusjrk "+
    // " from ( SELECT id,trip_id,order_id,id_order,pengiriman,addr_pengiriman,lat_pengiriman,lang_pengiriman,"+
    // "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+ this.state.longitude +" "+ this.state.latitude +")',4326)," +
    // "ST_GeomFromText('POINT(' || lang_pengiriman || ' ' || lat_pengiriman || ')',4326)) As numeric)/1000,2) jarak,"+
    // "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+ this.state.longitude +" "+ this.state.latitude +")',4326)," +
    // "ST_GeomFromText('POINT(' || lang_pengambilan || ' ' || lat_pengambilan || ')',4326)) As numeric)/1000,2) jarak2,"+
    // "status,statustxt,statusnext,customer_reference,remarks,instruksi,weight,dimension,statusasli,jadwal_sampai,txtjadwal_sampai,startloaded_date,startunloaded_date,to_char(due_date,'DD-MON-YYYY HH24:MI') as due_date "+
    // ",shipment_number,kode_shipper,token_shipper_contact,nama_shipper_contact,flag_validasi_sc FROM mos_trip_detail_v where trip_id="+myvalue+" and status between 6 and 10 ) z order by z.jadwal_sampai,z.jarak,z.id_order";

// console.log("TAB3:" + myquery);

    // await encrypt("select z.*, "+
    // "(case when (jarak>0.5 and (status+1) in (4,5,6,8,9,10)) then 'Posisi Anda belum berada di lokasi' else '' end) as statusjrk"+
    // " from ( SELECT id,trip_id,order_id,id_order,pengiriman,addr_pengiriman,lat_pengiriman,lang_pengiriman,"+
    // "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+ this.state.longitude +" "+ this.state.latitude +")',4326)," +
    // "ST_GeomFromText('POINT(' || lang_pengiriman || ' ' || lat_pengiriman || ')',4326)) As numeric)/1000,2) jarak,"+
    // "status,statustxt,statusnext,customer_reference,remarks,instruksi,weight,dimension,statusasli,jadwal_sampai,txtjadwal_sampai "+
    // " FROM mos_trip_detail_v where trip_id="+myvalue+" and status between 6 and 10 ) z order by z.jadwal_sampai,z.jarak,z.id_order"
    //      ).then((result)=>{
    //    myquery = result;
    // });

    // " when (status=5 and NOW() < (startloaded_date + gettimeloading(id) * interval '1 minutes')) then 'Waktu tunggu proses loading masih berlangsung' " +
    // " when (status=9 and NOW() < (startunloaded_date + gettimeloading(id) * interval '1 minutes')) then 'Waktu tunggu proses loading masih berlangsung' " +

    let myquery0 = "";
    let myquery1 = "";

    if (version.app=="DRIVER") { myquery1=" trip_id="+myvalue; }
    else { myquery1=" ((kerani_id_asal="+this.state.myuidd+" and turunkapal_date is null) or (kerani_id_tujuan="+this.state.myuidd+") and naikkapal_date is not null )"; }

// myquery0="select z.*, "+
// "(case when status=6 then jarak2::text else ' ' end) infojarak2,(case when status=6 then 'KM' else ' ' end) infokm2,"+
// "(case "+
// " when (status=7 and (kerani_id_asal is not null or kerani_id_asal<>0) and naikkapal_date is null) then 'Dalam perjalanan ke pelabuhan/Menunggu naik Kapal'"+
// " when (status=7 and (kerani_id_asal is not null or kerani_id_asal<>0) and turunkapal_date is null) then 'Naik Kapal ('||to_char(naikkapal_date,'DD-MON-YYYY HH12:MI:SS')||')/Kapal dalam perjalanan'"+
// " when (jarak>0.5 and (status+1) in (4,5,6,8,9,10)) then 'Posisi Anda belum berada di lokasi' "+
// " when (status=5 and NOW() < (startloaded_date + gettimeloading(id) * interval '1 minutes')) then 'Waktu tunggu proses muat sedang berlangsung' " +
// " when (status=9 and NOW() < (startunloaded_date + gettimeunloading(id) * interval '1 minutes')) then 'Waktu tunggu proses bongkar sedang berlangsung' " +
// " when (jarak2>1 /*0.3*/ and (status+1) in (7)) then 'Posisi Anda sudah menjauh dari lokasi pengambilan' "+
// " else '' end) as statusjrk, "+
// " (case when (status=7 and (kerani_id_asal is not null or kerani_id_asal<>0) and naikkapal_date is null) then '1'"+
// " when (status=7 and (kerani_id_asal is not null or kerani_id_asal<>0) and turunkapal_date is null) then '2'"+
// " when (status=7 and (kerani_id_asal is not null or kerani_id_asal<>0) and turunkapal_date is not null) then '3'"+
// " else '0' end) as statuskapal, "+
// " (case when (status=7 and (kerani_id_asal is not null or kerani_id_asal<>0) and turunkapal_date is not null) then 'Turun Kapal ('||to_char(turunkapal_date,'DD-MON-YYYY HH12:MI:SS')||')'"+
// " else '' end) as ketkapal "+
// " from ( SELECT id,trip_id,id_trip,no_container,order_id,id_order,pengiriman,addr_pengiriman,lat_pengiriman,lang_pengiriman,kerani_id_asal,kerani_id_tujuan,naikkapal_date,turunkapal_date,"+
// "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+ this.state.longitude +" "+ this.state.latitude +")',4326)," +
// "ST_GeomFromText('POINT(' || lang_pengiriman || ' ' || lat_pengiriman || ')',4326)) As numeric)/1000,2) jarak,"+
// "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+ this.state.longitude +" "+ this.state.latitude +")',4326)," +
// "ST_GeomFromText('POINT(' || lang_pengambilan || ' ' || lat_pengambilan || ')',4326)) As numeric)/1000,2) jarak2,"+
// "status,statustxt,statusnext,customer_reference,remarks,instruksi,weight,dimension,statusasli,jadwal_sampai,txtjadwal_sampai,startloaded_date,startunloaded_date,to_char(due_date,'DD-MON-YYYY HH24:MI') as due_date "+
// ",shipment_number,kode_shipper,token_shipper_contact,nama_shipper_contact,flag_validasi_sc FROM mos_trip_detail_v where  "+myquery1+" and status between 6 and 10 ) z order by z.jadwal_sampai,z.jarak,z.id_order";

// " when (jarak>1 and (status+1) in (10)) then 'Posisi Anda tidak berada di lokasi bongkar' "+
// " when (kode_shipper<>'SHP' and status=9 and NOW() < (startunloaded_date + gettimeunloading(id) * interval '1 minutes')) then 'Waktu tunggu proses bongkar sedang berlangsung' " +
// "(case when (kode_shipper<>'SHP' and status=5 and NOW() < (startloaded_date + gettimeloading(id) * interval '1 minutes')) then 'Waktu tunggu proses muat sedang berlangsung' " +

myquery0="select z.*, "+
"(case when status=6 then jarak2::text else ' ' end) infojarak2,(case when status=6 then 'KM' else ' ' end) infokm2,"+
" (case when ((naikkapal_date is null or turunkapal_date is null or kerani_id_tujuan is null) and (status+1) in (8)) then 'Tanggal Naik/Turun Kapal dan Kerani Tujuan belum diisi' "+
" else '' end) as statusjrk, "+
" (case when (status=7 and (kerani_id_asal is not null or kerani_id_asal<>0) and naikkapal_date is null) then '1'"+
" when (status=7 and (kerani_id_asal is not null or kerani_id_asal<>0) and turunkapal_date is null) then '2'"+
" when (status=7 and (kerani_id_asal is not null or kerani_id_asal<>0) and turunkapal_date is not null) then '3'"+
" else '0' end) as statuskapal, "+
" (case when (status=7 and (kerani_id_asal is not null or kerani_id_asal<>0) and turunkapal_date is not null) then 'Turun Kapal ('||to_char(turunkapal_date,'DD-MON-YYYY HH12:MI:SS')||')'"+
" else '' end) as ketkapal "+
" from ( SELECT id,trip_id,id_trip,no_container,order_id,id_order,pengiriman,addr_pengiriman,lat_pengiriman,lang_pengiriman,kerani_id_asal,kerani_id_tujuan,naikkapal_date,turunkapal_date,"+
"round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+ this.state.longitude +" "+ this.state.latitude +")',4326)," +
"ST_GeomFromText('POINT(' || lang_pengiriman || ' ' || lat_pengiriman || ')',4326)) As numeric)/1000,2) jarak,"+
"round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+ this.state.longitude +" "+ this.state.latitude +")',4326)," +
"ST_GeomFromText('POINT(' || lang_pengambilan || ' ' || lat_pengambilan || ')',4326)) As numeric)/1000,2) jarak2,"+
"status,statustxt,statusnext,customer_reference,remarks,instruksi,weight,dimension,statusasli,jadwal_sampai,txtjadwal_sampai,startloaded_date,startunloaded_date,to_char(due_date,'DD-MON-YYYY HH24:MI') as due_date "+
",shipment_number,kode_shipper,token_shipper_contact,nama_shipper_contact,flag_validasi_sc FROM mos_trip_detail_v where  "+myquery1+" and status between 6 and 10 ) z order by z.jadwal_sampai,z.jarak,z.id_order";

console.log(myquery0);

    await encrypt(myquery0
         ).then((result)=>{
       myquery = result;
    });

    Axios.post(url.select,{
       query: myquery
     })
     .then((response)=>{
       let temp = response.data;
      //  console.log(response.data);
       this.setState({dataSource: ds.cloneWithRows(temp.data)});
       this.setState({ isLoading: false});
     })
     .catch(error=>{
        // console.log("error+9++++++++++++++++++++++++++++++"+error);
        null;
     });

   }

   async componentDidMount(){
    url = await getUrl();
    // console.log('tab 3 reload');
    await AsyncStorage.getItem("uid",(error,value)=>{
      if(value!=null)  myuid=value ; 
      else { this.props.navigation.replace("Login"); }});

   let myuidd = "";
   let myvalue = 0;
   await decrypt(myuid).then(value=>{
      myuidd=value
   });
  //  console.log("myuid ==== "+myuidd);

  this.setState({myuidd: String(myuidd)});

   this._settripid(myuidd);
   await AsyncStorage.getItem("curTrip",(error,value)=>{
     if(value!=null)  myvalue = value ; 
     else { myvalue = 0 }});
   this.setState({myvalue: myvalue});


this.interval=setInterval(async () => {
  if (!this.state.modalVisible)  
  {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        'title': 'Mostrans',
        'message': 'Allow app to access your location '
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      navigator.geolocation.getCurrentPosition(
        async (position) => {
            //console.log(position);
           this.setState({
             latitude: position.coords.latitude,
             longitude: position.coords.longitude
           });

           this._settripid(myuidd);
           await AsyncStorage.getItem("curTrip",(error,value)=>{
             if(value!=null)  myvalue = value ; 
             else { myvalue = 0 }});

             this.queryUtama(myvalue);

          },
        (error) => {
            // See error code charts below.
            ToastAndroid.showWithGravityAndOffset(
              'Tidak mendapatkan sinyal GPS',
              ToastAndroid.LONG,
              ToastAndroid.BOTTOM,
              25,
              50,
            );
            // console.log("Error===============");
            // console.log(error.code, error.message);
        },
        { enableHighAccuracy: false, timeout: 360000, maximumAge: 10000 }
    );
    } else {
      // alert("Location permission denied");
      ToastAndroid.showWithGravityAndOffset(
        'Tidak mendapatkan izin GPS',
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50,
      );
    }
  } catch (err) {
    // console.log("No Access  to location" + err);
    ToastAndroid.showWithGravityAndOffset(
      'Tidak mendapatkan akses lokasi',
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50,
    );
 
  }
  //   navigator.geolocation.getCurrentPosition(
  //     (position) => {
  //         console.log('============= get location ===========')
  //         console.log(position);
  //     },
  //     (error) => {
  //       console.log('============= error get location ===========')
  //       console.log(error)
  //     },
  //     { 
  //        enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 
  //     }
  //  );
  }
  }, 1000*1);




        // navigator.geolocation.getCurrentPosition(
        //     (position) => {
        //       this.setState({
        //         latitude: position.coords.latitude,
        //         longitude: position.coords.longitude,
        //         error: null,
        //       });
        //     },
        //     (error) => this.setState({ error: error.message }),
        //     { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        //   );


        //  alert(mylatitude);
        //  alert(mylongitude);

      // alert("select z.* from ( SELECT trip_id,order_id,id_order,pengiriman,addr_pengiriman,lat_pengiriman,lang_pengiriman,"+
      // "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+this.state.latitude+" "+this.state.longitude+")',4326)," +
      // "ST_GeomFromText('POINT(' || lat_pengiriman || ' ' || lang_pengiriman || ')',4326)) As numeric)/1000,2) jarak,"+
      // "status,statustxt,statusnext,customer_reference,remarks,instruksi,weight,dimension FROM mos_trip_detail_v where trip_id="+myvalue+" and status between 6 and 10 ) z order by z.jarak");

      // console.log("select z.* from ( SELECT trip_id,order_id,id_order,pengiriman,addr_pengiriman,lat_pengiriman,lang_pengiriman,"+
      // "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT(0 0)',4326)," +
      // "ST_GeomFromText('POINT(' || lat_pengiriman || ' ' || lang_pengiriman || ')',4326)) As numeric)/1000,2) jarak,"+
      // "status,statustxt,statusnext,customer_reference,remarks,instruksi,weight,dimension FROM mos_trip_detail_v where trip_id="+myvalue+" and status between 6 and 10 ) z order by z.jarak")

      
   

      }


//camera begin

onPress(UrutPhotox) {
  UrutPhoto = UrutPhotox;
  var that = this;
  if (Platform.OS === 'android') {
    async function requestCameraPermission() {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'CameraExample App Camera Permission',
            message: 'CameraExample App needs access to your camera ',
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          //If CAMERA Permission is granted
          //Calling the WRITE_EXTERNAL_STORAGE permission function
          requestExternalWritePermission();
        } else {
          alert('CAMERA permission denied');
        }
      } catch (err) {
        alert('Camera permission err', err);
        console.warn(err);
      }
    }
    async function requestExternalWritePermission() {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'CameraExample App External Storage Write Permission',
            message:
              'CameraExample App needs access to Storage data in your SD Card ',
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          //If WRITE_EXTERNAL_STORAGE Permission is granted
          //Calling the READ_EXTERNAL_STORAGE permission function
          requestExternalReadPermission();
        } else {
          alert('WRITE_EXTERNAL_STORAGE permission denied');
        }
      } catch (err) {
        alert('Write permission err', err);
        console.warn(err);
      }
    }
    async function requestExternalReadPermission() {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          {
            title: 'CameraExample App Read Storage Read Permission',
            message: 'CameraExample App needs access to your SD Card ',
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          //If READ_EXTERNAL_STORAGE Permission is granted
          //changing the state to re-render and open the camera
          //in place of activity indicator
          that.setState({ isPermitted: true });
          // this.setState({ isPermitted: true });
        } else {
          alert('READ_EXTERNAL_STORAGE permission denied');
        }
      } catch (err) {
        alert('Read permission err', err);
        console.warn(err);
      }
    }
    //Calling the camera permission function
    requestCameraPermission();
   // alert('1');
    } else {
    this.setState({ isPermitted: true });
    //alert('22');
  }
}
async onBottomButtonPressed(event) {
  
  // alert('event.type '+event.type);

  const captureImages = JSON.stringify(event.captureImages);
  if (event.type === 'left') {
    this.setState({ isPermitted: false });
  } else if (event.type === 'capture') {

    let mycaptureimages;
    await kompressimage(event.captureImages[0].uri).then(result=>{
        mycaptureimages=result
    });

    if (UrutPhoto=="1") {
    this.setState({ urifoto1: mycaptureimages, isPermitted: false}); }
    else if (UrutPhoto=="2") {
      this.setState({ urifoto2: mycaptureimages, isPermitted: false}); }
      else if (UrutPhoto=="3") {
        this.setState({ urifoto3: mycaptureimages, isPermitted: false}); }
        else if (UrutPhoto=="4") {
          this.setState({ urifoto4: mycaptureimages, isPermitted: false}); }
          else if (UrutPhoto=="5") {
            this.setState({ urifoto5: mycaptureimages, isPermitted: false}); }
            else if (UrutPhoto=="301") {
              this.setState({ urifoto1A: mycaptureimages, isPermitted: false}); }
                
//alert(UrutPhoto);
//alert(this.state.urifoto1);

    //this.setState({ isPermitted: false });

    // Alert.alert(
    //   event.type,
    //   captureImages,
    //   [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
    //   { cancelable: false }
    // );

    // if (event.type=='right') { alert(captureImages)};

  }
}

//camera end


uploadImage1A = (uri, id_order)=> new Promise((resolve, reject)=>{
  // console.log('start1');
  // console.log(id_order+'_201');

  // ad='uploadImage1A 1:'+uri;
  // ae='uploadImage1A 2:'+id_order;

  const imageRef = firebase.storage().ref('FotoPOD/').child(id_order+'_301');
  // const imageRef = firebase.storage().ref('FotoPOD/').child('A_1');
  // console.log('start2');
  imageRef.putFile(uri);
  // console.log('start3');
  let icount=0;
  for (icount = 0; icount < 30; icount++){  
  imageRef.getDownloadURL().then(async (result)=>{
    this.setState({urlfoto1A: result});
    // console.log('result image:'+result);
    resolve(result);
    icount=31;
  })
  .catch(error=>{
    // console.log('errorimage: '+error);
    reject(result);
  })
  // console.log('start4');
  // af='uploadImage1A 3:'+this.state.urlfoto1A;
  this.timehold();
}

})


saveSign() {
  this.sign.saveImage();
  // alert('simpan 1');
}
resetSign() {
  this.sign.resetImage();
  ttdShipper= '';
}
_onSaveEvent(result) {
  //result.encoded - for the base64 encoded png
  //result.pathName - for the file path name
  // console.log(result);
  // alert('simpan 2');
  // this.state.ttdShipper
  // alert(result.pathName);
  // this.setState({ttdShipper: result.pathName});
  ttdShipper = result.pathName;

  // let kuncisaya = Math.floor(Math.random()*1000000);
  // // await 
  // this.uploadImagettd('file://' + ttdShipper, kuncisaya  /*trip_id*/).then(result=>{
  //   console.log(result);
  // });

    
}

_onDragEvent() {
   // This callback will be called when the user enters signature
  console.log("dragged");
}

async timehold(){ //must be async func
  //do something here
  await sleep(1000) //wait 1 seconds
  //continue on...
}

render(){

  if (this.state.isPermitted) {
   // alert(this.state.isPermitted);
    return (

      <View>
      <Modal
         animationType="fade"
        transparent={false}
        alignItems="center"
        >

      <CameraKitCameraScreen
        // Buttons to perform action done and cancel
        actions={{ rightButtonText: '', leftButtonText: 'Tutup' }}
        onBottomButtonPressed={event => this.onBottomButtonPressed(event)}
        flashImages={{
          // Flash button images
          on: require('../../../assets/images/flashon.png'),
          off: require('../../../assets/images/flashoff.png'),
          auto: require('../../../assets/images/flashauto.png'),
        }}
        cameraFlipImage={require('../../../assets/images/flip.png')}
        captureButtonImage={require('../../../assets/images/capture.png')}
      />
      </Modal>
      </View>
    );
  } else {
      return (     
      
        <View style={{height: '100%'}}>
         <ListView
         enableEmptySections='true'
         dataSource={this.state.dataSource}
         renderRow={(rowData) => 

         <View style = {styles.container}>

         <View style={styles.item1}>
         <Text style={{ fontSize: 25, color: 'black' }}>
         {rowData.jarak}
         </Text>
         <Text style={{ fontSize: 10, color: 'black' }}>KM</Text>

{  (rowData.statusasli=='on the way' && (rowData.statuskapal=='0' || rowData.statuskapal=='3')) ?       
<TouchableOpacity  style={styles2.buttonMap}
onPress={() => Linking.openURL('google.navigation:q='+rowData.lat_pengiriman+'+'+rowData.lang_pengiriman)}>
<Text style={{ color: 'white' }}>Panduan</Text>
<Text style={{ color: 'white' }}>Rute</Text>
<Text style={{ color: 'white' }}>Perjalanan</Text>
</TouchableOpacity>
: null
}

{/* {  (rowData.kode_shipper=='SHP') ?       
         <TouchableOpacity  style={styles2.buttonMap}
         onPress={() => this.onGetCodeSimantra(rowData.shipment_number) }>
         <Text style={{ color: 'white' }}>QR Code</Text>
         <Text style={{ color: 'white' }}>Simantra</Text>
         </TouchableOpacity>
         : null
} */}


{ (rowData.statuskapal=='1') ?       
  <Image source={require('../../../assets/images/muatkapal.png')}  style={styles2.ImageStyle04} />

  : null

}  

{ (rowData.statuskapal=='2') ?       
  <Image source={require('../../../assets/images/jalankapal.png')}  style={styles2.ImageStyle04} />

  : null

}  

{ (rowData.statuskapal=='3') ?       
  <Image source={require('../../../assets/images/tibakapal.jpeg')}  style={styles2.ImageStyle04} />

  : null

}  


{ (rowData.status==6) ?       
  <Image source={require('../../../assets/images/delivery.png')}  style={styles2.ImageStyle04} />

  : null

}  


{ (rowData.status==8) ?       
  <Image source={require('../../../assets/images/cargo-finish.png')}  style={styles2.ImageStyle04} />
  : null
}  
{ (rowData.status==9) ?       
  <Image source={require('../../../assets/images/delivery-finish1.png')}  style={styles2.ImageStyle04} />
  : null
}  
{ (rowData.status==10) ?       
  <Image source={require('../../../assets/images/signature.png')}  style={styles2.ImageStyle03} />
  : null
}  

  <Text style={{ fontSize: 12, color: '#000000', marginLeft:15,alignItems: "center" }}>
  {rowData.ketkapal}
  </Text>


  <Text style={{ fontSize: 25, color: '#a5a5a5' }}>
  {rowData.infojarak2}
  </Text>
  <Text style={{ fontSize: 10, color: '#a5a5a5' }}>
  {rowData.infokm2}
  </Text>



           </View>

         
         <View style={styles.item2}>
         <TouchableOpacity style={styles.button} onPress={()=>this._trip(true,rowData.id,rowData.statusjrk)}>
         <View>
         {/* <Text>{rowData.id}</Text> */}
         <Text style={{ color: 'black' }}>{rowData.id_order}</Text>
         <Text>{rowData.id_trip}/{rowData.no_container}</Text>
         <Text>{rowData.txtjadwal_sampai}</Text>
         <Text>{rowData.pengiriman}</Text>
         <Text>{rowData.addr_pengiriman}</Text>
         <Text style={{ color: 'blue' }}>{rowData.statustxt} &#10140; <Text style={{ color: 'green' }}>{rowData.statusnext}</Text></Text>
         {  (rowData.due_date !== null) ?
         <Text style={{ color: 'red' }}>Target Waktu : {rowData.due_date}</Text> : null
         }
         {  (rowData.statusjrk!="") ?
         <Text style={{ color: 'red' }}>{rowData.statusjrk}</Text> : null
         }
         </View>
         </TouchableOpacity>
         </View>


{/* <View>
  <Modal transparent={true}
    visible={(this.state.modalVisible&&this.state.keyid==rowData.id)}>
    <View>
       <Kam/>
    </View>
  </Modal>
</View> */}

       {/* <View>
        <Modal
           animationType="fade"
          transparent={true}
          alignItems="center"
          visible={(this.state.modalQRCodeSimantra)}
          >
          <View style={{ backgroundColor: 'rgba(0,0,0,0.6)', height: '100%' }}>
            <ScrollView>
          <View style={styles.boxShad}>

          <QRCode
          value={this.state.QRCodeSimantra}
          size={100}
          bgColor='black'
          fgColor='white'/>

            <TouchableHighlight style={styles2.button}
                onPress={() => {
                  this.TutupSimantra();
                }}>
                <Text style={{ color: 'white' }}>Tutup</Text>
              </TouchableHighlight>

          </View>
          </ScrollView>
          </View>
        </Modal>
        </View>


 */}
         <View>
        <Modal
           animationType="fade"
          transparent={true}
         //  backgroundColor='rgba(0,0,255,0.8)'
         //  justifyContent= "center"
          alignItems="center"

          visible={(this.state.modalVisible&&this.state.keyid==String(rowData.id))}
          // visible='false'

          //  onRequestClose={() => {
            // Alert.alert('Modal has been closed.');
         //  }}
          >
          <View style={{ backgroundColor: 'rgba(0,0,0,0.6)', height: '100%' }}>
            <ScrollView>
          <View style={styles.boxShad}>
            <View>
              {/* <Text>Hello World!</Text> */}

              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Nomor Order</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.id_order}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Jadwal Sampai</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.txtjadwal_sampai}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>No Referensi Pelanggan</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.customer_reference}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Catatan</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.remarks}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Instruksi</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.instruksi}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Berat</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.weight} KG</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Dimensi</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.dimension} CBM</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Pengiriman</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.pengiriman}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Alamat</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.addr_pengiriman}</Text>
                  </View>
              </View>


{ (rowData.status=='9') ?
              <View style = {styles.container1}>
              <View style={styles.item3}>
                  <Text>Biaya Kuli Rp.</Text>
              </View>
              <View style={styles.item4A}>
              <TextInput
                          style={{flex:1}}
                          underlineColorAndroid='rgba(0,0,0,0)'
                          placeholder="Isi Biaya Kuli disini. Isi 0 jika tidak ada Biaya Kuli."
                          disabled={this.state.selesaiCheck}
                          value={this.state.nilaiBiayaKuli}
                          keyboardType="number-pad"
                          onChangeText={(input)=>this.setState({nilaiBiayaKuli:input})}
                        />
              </View>
          </View>
: null
}
{ (rowData.status=='9') ?
              <View style = {styles.container1}>
              <View style={styles.item3}>
                  <Text>Foto Kwitansi</Text>
              </View>
              <View style={styles.item4A}>
              <TouchableOpacity 
                    onPress={() => {this.onPressTanya('301')} }>
<Image source={require('../../../assets/images/camera.png')} style={{ width: 40, height: 40 }} />
                   </TouchableOpacity>
              </View>
          </View>
: null
}

<View>
{ this.state.urifoto1A !== '' ? 
<View>
{/* <Text style={{ fontSize: 14, fontStyle: "italic"}}>Photo</Text> */}
<Image style={{width: 300, height: 300}} source={{uri: 'file://' + this.state.urifoto1A}}/> 
</View>
: false}  
</View>

{ (rowData.status=='9' && rowData.flag_validasi_sc=='Y') ?
<View>
  <Text> </Text>
</View>
: null
}

{ (rowData.status=='9' && rowData.flag_validasi_sc=='Y') ?
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <View style={{flex: 1, height: 1, backgroundColor: 'gray'}} />
          <View>
          <Text style={{width: 200, fontSize: 20, fontWeight: 'bold' , textAlign: 'center'}}>Diisi oleh Shipper Contact</Text>
          </View>
          <View style={{flex: 1, height: 1, backgroundColor: 'gray'}} />
          </View>

: null
}

{ (rowData.status=='9' && rowData.flag_validasi_sc=='Y') ?
             <View style = {styles.container1}>


             <View style={styles.item3}>
                 <Text>Token</Text>
             </View>
             <View style={styles.item4A}>
             <TextInput
                         style={{flex:1}}
                         underlineColorAndroid='rgba(0,0,0,0)'
                         placeholder="Token Shipper Contact"
                         keyboardType="number-pad"
                         onChangeText={(input)=>this.setState({tokenShipper:input})}
                       />
             </View>
         </View>
: null
}


{/* <Text>{aa}</Text>
<Text>{ab}</Text>
<Text>{ac}</Text>
<Text>{ad}</Text>
<Text>{ae}</Text>
<Text>{af}</Text> */}


{/* tanda tangan */}
{ (rowData.status=='9' && rowData.flag_validasi_sc=='Y') ?
<View style={{ flex: 1, flexDirection: "column" }}>
                <Text style={{alignItems:"center",justifyContent:"center"}}>Tanda Tangan Shipper Contact ({ rowData.nama_shipper_contact })
                </Text>
                <SignatureCapture
                    style={[{flex:1},styles.signature]}
                    ref={input => (this.sign = input)}
                    onSaveEvent={this._onSaveEvent}
                    onDragEvent={this._onDragEvent}
                    saveImageFileInExtStorage={true}
                    backgroundColor="#eff9f1"
                    showNativeButtons={false}
                    showTitleLabel={false}
                    viewMode={"portrait"}/>

                <View style={{ flex: 1, flexDirection: "row" }}>
                    <TouchableHighlight style={styles.buttonStyle}
                        onPress={() => { this.saveSign() } } >
                        <Text>Simpan</Text>
                    </TouchableHighlight>

                    <TouchableHighlight style={styles.buttonStyle}
                        onPress={() => { this.resetSign() } } >
                        <Text>Ulangi</Text>
                    </TouchableHighlight>

                </View>

            </View>
: null
}
{/* tanda tangan */}

              <View>

              {/* <View style = {styles.container1}>
                  <View style={styles.item7}>
{ ('POD'==rowData.statusnext) ? 
<TouchableOpacity 
                    onPress={() => {this.onPress()} }>
<Image source={require('../../../assets/images/camera.png')}  />
                   </TouchableOpacity>
: null
}
</View>
<View style={styles.item8}>
{
  ('POD'==rowData.statusnext) ?  null : null
}
{ ('POD'==rowData.statusnext) ? 
                  <Text style={{ color: 'red'}}>Catatan : Siapkan Dokumen POD untuk difoto                                  
                  </Text> : null
}
</View></View>               */}


<View style = {styles.container1}>
{ ('POD'==rowData.statusnext) ? 
                  <View style={styles.itemfull}>
                  <Text style={{ color: 'blue'}}>Catatan : Siapkan Dokumen POD untuk difoto                                  
                  </Text>
</View>
: null
}
</View>
<View style = {styles.container1}>
<View style={styles.itemlima}>
{ ('POD'==rowData.statusnext) ? 
<TouchableOpacity 
                    onPress={() => {this.onPress('1')} }>
<Image source={require('../../../assets/images/camera.png')} style={{ width: 40, height: 40 }} />
<Text style={{ fontSize: 12}}>Photo 1</Text>
                   </TouchableOpacity>
: null }
</View>
<View style={styles.itemlima}>
{ ('POD'==rowData.statusnext) ? 
<TouchableOpacity 
                    onPress={() => {this.onPress('2')} }>
<Image source={require('../../../assets/images/camera.png')}  style={{ width: 40, height: 40 }}  />
<Text style={{ fontSize: 12}}>Photo 2</Text>
                   </TouchableOpacity>
: null }
</View>
<View style={styles.itemlima}>
{ ('POD'==rowData.statusnext) ? 
<TouchableOpacity 
                    onPress={() => {this.onPress('3')} }>
<Image source={require('../../../assets/images/camera.png')}  style={{ width: 40, height: 40 }}  />
<Text style={{ fontSize: 12}}>Photo 3</Text>
                   </TouchableOpacity>
: null }
</View>
<View style={styles.itemlima}>
{ ('POD'==rowData.statusnext) ? 
<TouchableOpacity 
                    onPress={() => {this.onPress('4')} }>
<Image source={require('../../../assets/images/camera.png')}  style={{ width: 40, height: 40 }}  />
<Text style={{ fontSize: 12}}>Photo 4</Text>
                   </TouchableOpacity>
: null }
</View>
<View style={styles.itemlima}>
{ ('POD'==rowData.statusnext) ? 
<TouchableOpacity 
                    onPress={() => {this.onPress('5')} }>
<Image source={require('../../../assets/images/camera.png')}  style={{ width: 40, height: 40 }}  />
<Text style={{ fontSize: 12}}>Photo 5</Text>
                   </TouchableOpacity>
: null }
</View>
</View>



{/* <TouchableOpacity        
       onPress={() => {this.onPress.bind(this)} }> */}
{/* <Text>{this.state.urifoto}</Text> */}

{ this.state.urifoto1 !== '' ? 
<View>
<Text style={{ fontSize: 14, fontStyle: "italic"}}>Photo 1</Text>
<Image style={{width: 300, height: 300}} source={{uri: 'file://' + this.state.urifoto1}}/> 
</View>
: false}
{ this.state.urifoto2 !== '' ? 
<View>
<Text style={{ fontSize: 14, fontStyle: "italic"}}>Photo 2</Text>
<Image style={{width: 300, height: 300}} source={{uri: 'file://' + this.state.urifoto2}}/> 
</View>
: false}
{ this.state.urifoto3 !== '' ? 
<View>
<Text style={{ fontSize: 14, fontStyle: "italic"}}>Photo 3</Text>
<Image style={{width: 300, height: 300}} source={{uri: 'file://' + this.state.urifoto3}}/> 
</View>
: false}
{ this.state.urifoto4 !== '' ? 
<View>
<Text style={{ fontSize: 14, fontStyle: "italic"}}>Photo 4</Text>
<Image style={{width: 300, height: 300}} source={{uri: 'file://' + this.state.urifoto4}}/> 
</View>
: false}
{ this.state.urifoto5 !== '' ? 
<View>
<Text style={{ fontSize: 14, fontStyle: "italic"}}>Photo 5</Text>
<Image style={{width: 300, height: 300}} source={{uri: 'file://' + this.state.urifoto5}}/> 
</View>
: false}

              </View>


              <View style = {styles.container1}>

 {  (rowData.statusjrk=='' ) ?   
                  <View style={styles.item5}>
              <TouchableHighlight style={styles2.button}
                onPress={() => {
                  this._tripproses(!this.state.modalVisible,rowData.order_id,rowData.status,rowData.trip_id,rowData.token_shipper_contact,rowData.flag_validasi_sc);
                }}>
                <Text style={{ color: 'white' }}>{rowData.statusnext}</Text>
              </TouchableHighlight>
              </View>
: null }

                  <View style={styles.item6}>
              <TouchableHighlight style={styles2.button}
                onPress={() => {
                  this._triptutup(!this.state.modalVisible,0);
                }}>
                <Text style={{ color: 'white' }}>Tutup</Text>
              </TouchableHighlight>
              </View>


              </View>

            </View>

          </View>
          </ScrollView>
          </View>
          {this.state.modalisLoading ?
            // <View style={styles.spinnercontainer}>
            //   <Spinner/>
            //   <Text style={styles.spinnertext}>Loading...</Text>
            // </View>
            false
            :
            false
          }
        </Modal>
      </View>






         </View>

}
/>
{ this.state.isLoading ? 
  // <View style={styles.spinnercontainer}>
  //   <Spinner/>
  //   <Text style={styles.spinnertext}>Loading...</Text>
  // </View>
  false
  : 
    this.state.dataSource.getRowCount() === 0 ? <View style={styles2.nodata}><Text>Tidak ada Data</Text></View> : false
  }
</View>
       ); } //akhir else endif

      }
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'flex-start', // if you want to fill rows left to right
      marginTop: 2,
      marginBottom: 2
    },
    container1: {
      // flex: 1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'flex-start', // if you want to fill rows left to right
      // marginTop: 2,
      // marginBottom: 2
    },
    item1: {
      width: '30%', // is 50% of container width
      //flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      paddingTop: 10
    },
    item2: {
      width: '70%', // is 50% of container width
      backgroundColor: '#eff9f1',
      paddingTop: 5,
      paddingBottom: 5,
      paddingLeft: 10
    },
    item3: {
      width: '35%', // is 50% of container width
      //flex: 1,
      // justifyContent: 'center',
      alignItems: 'flex-start',
      // paddingTop: 10
    },
    item4: {
      width: '65%', // is 50% of container width
      backgroundColor: '#eff9f1',
      alignItems: 'flex-start',
      // paddingTop: 5,
      // paddingBottom: 5,
      // paddingLeft: 10
    },
    item4A: {
      width: '65%', // is 50% of container width
      backgroundColor: '#ffffff',
      alignItems: 'flex-start',
      // paddingTop: 5,
      // paddingBottom: 5,
      // paddingLeft: 10
    },
    item5: {
      width: '50%', // is 50% of container width
      //flex: 1,
      // justifyContent: 'center',
      alignItems: 'flex-end',
      padding: 10,
      paddingTop: 20
    },
    item6: {
      width: '50%', // is 50% of container width
      //flex: 1,
      // justifyContent: 'center',
      alignItems: 'flex-start',
      padding: 10,
      paddingTop: 20
    },
    item7: {
      width: '35%', // is 50% of container width
      //flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 0,
      paddingTop: 0
    },
    item8: {
      width: '65%', // is 50% of container width
      //flex: 1,
      justifyContent: 'center',
      // alignItems: 'flex-center',
      padding: 10,
      paddingTop: 20
    },
    itemfull: {
      width: '100%', // is 50% of container width
      //flex: 1,
      justifyContent: 'center',
      // alignItems: 'flex-center',
      padding: 10,
      paddingTop: 20
    },
    itemlima: {
      width: '20%', // is 50% of container width
      flex: 1,
      justifyContent: 'center',
      // alignItems: 'flex-center',
      padding: 10,
      paddingTop: 20
    },
    boxShad: {
      flexDirection: 'row',
      marginTop: 100,
      // marginBottom: 50,
      // width: '80%',
      borderWidth: 1,
      borderRadius: 8,
      // borderColor: '#ddd',
      // borderBottomWidth: 0,
      alignItems: 'center',
      // flexDirection: 'row',
      shadowColor: '#000',
      shadowOffset: { width: 5, height: 5 },
      // shadowOpacity: 0.2,
      shadowRadius: 5,
      elevation: 1,
      padding: 10,
      backgroundColor: 'white'
    },
    spinnercontainer:{
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      backgroundColor: '#00000033',
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    signature: {
      flex: 1,      
      borderWidth: 1,
      borderColor: "#2a2d30",
      height: 150
  },
  buttonStyle: {
      flex: 1, justifyContent: "center", alignItems: "center", height: 30,
      backgroundColor: "#eeeeee",
      margin: 10
  }    
})

AppRegistry.registerComponent('RNSignatureExample', () => RNSignatureExample);