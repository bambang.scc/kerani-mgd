import React, { Component } from "react";
import {
  Container, 
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Footer,
  FooterTab,
  Left,
  Right,
  Body,
  Badge,
  Grid,
  Col,
  Card,
  CardItem, Tabs, Tab, TabHeading, ScrollableTab, List, ListItem, Spinner, StyleProvider
} from "native-base";
import { View, Image, Dimensions, StyleSheet, YellowBox, WebView, TouchableOpacity } from 'react-native';
import WebViewMOS from './WebViewMOS.js';

import getTheme from '../../../native-base-theme/components';
import platform from '../../../native-base-theme/variables/platform';

// import ViewRedeem from './ViewRedeem.js';

import Tab1 from './Tab1.js';
import Tab2 from './Tab2.js';
import Tab3 from './Tab3.js';
import styles from './styles';
import {encrypt, decrypt, getUrl, pointdecrypt, pointencrypt, version} from '../../../assets/lib/brosky';
import AsyncStorage from '@react-native-community/async-storage'; //untuk simpan local variable
import Axios from 'axios';
import { PermissionsAndroid } from 'react-native';

// https://www.fullstacklabs.co/blog/face-recognition-with-react-native-camera

let url;
let myparamstr = "";
let myencparamstr = "";
let myalamat = "";
let myalamat2="";

let tab1pict = null ; 
// require('../../../assets/images/trip.png');

YellowBox.ignoreWarnings(['WebView'])

class HomeView extends Component {
  constructor(props) {
    super(props);
    this.state={
      isLoading: false,
      mypoint: 0,
      myshowredeem: 0,
      // myalamat: '',
      // myalamat2: '',
      latitude: '',
      longitude: '',
      myuidd: ''
    }
  }



  // encrypt=(text)=>{
  //   let key='redeemmostrans';
  //   key = CryptoJS.MD5(key).toString();
  //   const k1 = key.substring(0, 16);
  //   key = key + k1;
  //   const textWordArray = CryptoJS.enc.Utf8.parse(text);
  //   const keyHex = CryptoJS.enc.Hex.parse(key);
    
  //   const encrypted = CryptoJS.TripleDES.encrypt(textWordArray, keyHex, {
  //     mode: CryptoJS.mode.ECB,
  //     padding: CryptoJS.pad.Pkcs7
  //   });
  //   const base64String = encrypted.toString();
  //   return base64String;
  // }
  // decrypt=(base64String)=>{
  //   let key='redeemmostrans';
  //   key = CryptoJS.MD5(key).toString();
  //   const k1 = key.substring(0, 16);
  //   key = key + k1;

  //   const keyHex = CryptoJS.enc.Hex.parse(key);

  //   const decrypted = CryptoJS.TripleDES.decrypt( {
  //       ciphertext: CryptoJS.enc.Base64.parse(base64String)
  //   }, keyHex, {
  //     mode: CryptoJS.mode.ECB,
  //     padding: CryptoJS.pad.Pkcs7
  //   });

  //   return decrypted.toString(CryptoJS.enc.Utf8);
  // }



  async componentDidMount(){
    let {width, height} = Dimensions.get('window');
      console.log('load');
      console.log(width, height);

      url = await getUrl();

     await AsyncStorage.getItem("uid",(error,value)=>{
        if(value!=null)  myuid=value ; 
        else { this.props.navigation.replace("Login"); }});
     let myuidd = "";
     await decrypt(myuid).then(value=>{
        myuidd=value
     });
 
     this.setState({ myuidd: myuidd});

    //  alert("select poin-poin_redeem totpoin from mt_driver_poin where driver_id="+myuidd+" and tahun=extract(year from (NOW() - interval '31' day))::text");

     if (version.app=='DRIVER')
     { tab1pict = require('../../../assets/images/trip.png'); }
     else if (version.app=='KERANI')
     { tab1pict = require('../../../assets/images/ship.png'); }


    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'Mostrans',
          'message': 'Allow app to access your location '
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        navigator.geolocation.getCurrentPosition(
          async (position) => {
              console.log(position);
             this.setState({
               latitude: position.coords.latitude,
               longitude: position.coords.longitude
             });
        },
          (error) => {
              console.log("Error===============");
              console.log(error.code, error.message);
          },
          { enableHighAccuracy: false, timeout: 360000, maximumAge: 10000 }
      );
      } else {
        alert("Location permission denied");
      }
    } catch (err) {
      console.log("No Access  to location" + err);
    }



    // select poin-poin_redeem totpoin from mt_driver_poin where driver_id="+myuidd+" and tahun=extract(year from (NOW() - interval '31' day))::TEXT"

    await encrypt("select totpoin from mt_driver_poin_v where tahun::text=tahunpembanding::text and driver_id="+myuidd ).then((result)=>{
        myquery = result;
     }
     );
  
     Axios.post(url.select,{
        query: myquery
      })
      .then((response)=>{
        let temp = response.data;
        this.setState({ mypoint: temp.data[0].totpoin});

        // alert(this.state.mypoint);

        console.log(temp.data[0]);
      })
      .catch(error=>{
         console.log("error+1++++++++++++++++++++++++++++++"+error);
      });

      // alert( pointencrypt('10') );

      let myquery = "";

      await encrypt("select id::TEXT sid,transporter_id::TEXT transporter_id,'' acc, '' imei_hp from mt_master_user where id=" + this.state.myuidd ).then((result)=>{
         myquery = result;
      });
      Axios.post(url.select,{
         query: myquery
       })
       .then((response)=>{
        let temp = response.data;
        myparamstr = temp.data[0].sid + "~" + temp.data[0].transporter_id + "~"+"1.4"+"~"+this.state.longitude+"~"+this.state.latitude+"~0~"+temp.data[0].imei_hp;
        myencparamstr = pointencrypt(myparamstr);

       if (version.type=='dev') {
           myalamat = 'https://mostrans.co.id/redeempoint/?q=h9172911333' + myencparamstr;
           myalamat2 = 'https://mostrans.co.id/redeempoint/history?q=h9172911333' + myencparamstr;          
          //  alert(version.type);
          //  alert(alamat);
          //  alert(alamat2);
          //  console.log(version.type);
          //  console.log(alamat);
          //  console.log(alamat1);
       }
       else
       {
        myalamat = 'https://mostrans.co.id/redeempoint/?q=' + myencparamstr;
        myalamat2 = 'https://mostrans.co.id/redeempoint/history?q=' + myencparamstr;          
        // alert(version.type);
        // alert(alamat);
        // alert(alamat2);
        // console.log(version.type);
        // console.log(alamat);
        // console.log(alamat1);
 }
        // alert(myparamstr);
      })
      .catch(error=>{
        console.log('ErrRedeem');
      });
  
    }

  logimei=()=>{
    this.props.navigation.replace('Splash');
  }


  async _switchredeem(){

    // this.setState({ myalamat: 'https://mostrans.co.id/redeempoint/?q=' + myencparamstr});
    // this.setState({ myalamat2: 'https://mostrans.co.id/redeempoint/history?q=' + myencparamstr});


    // alert('https://mostrans.co.id/redeempoint/?q=' + myencparamstr);
    // alert("myparamstr "+myparamstr);

    //matikan dulu
    // if (false) {

    
    if (this.state.myshowredeem==1) {
      this.setState({ myshowredeem: 0});
     } else
     {  
      this.setState({ myshowredeem: 1});
     };

    // }
  };

  // import React, { Component } from 'react'
  // import { WebView } from 'react-native'
  // export default class Customer extends Component {
  //   render() {
  //     return (
  //       <WebView
  //         source={{ uri: 'https://kolabsit.com' }}
  //         style={{ marginTop: 20 }}
  //       />
  //     )
  //   }
  // }

  render() {
    return (

      <StyleProvider style={getTheme(platform)}>
      <Container /*style={{ backgroundColor: "#ffffff" }}*/>
        {/* <Header style={{ backgroundColor: "#008C45" }}> */}
        <Header /*style={{ backgroundColor:  "#008C45"  }}*/>
          <Body>
            <View >
              <Text style={{ color: 'white' }}>
              <Image source={require('../../../assets/images/logokecil.png')}  style={styles.ImageStyle01} />
                 &nbsp; MOSTRANS</Text>
            </View>

            {/* <Title>
              MOSTRANS Driver</Title> */}

          </Body>

          <Right>
          <TouchableOpacity style={styles.buttonkhusus} onPress={()=>this._switchredeem()}>

{  (this.state.myshowredeem==0) ?  
  <View>              
              <Text style={{ color: 'white' }}>

              <Image source={require('../../../assets/images/coingold.png')}  style={styles.ImageStyle01A} />
              &nbsp; {this.state.mypoint} Points
              
              </Text>
</View>  :
  <View>              
  <Text style={{ color: 'white' }}>

  <Image source={require('../../../assets/images/kembali.png')}  style={styles.ImageStyle01A} />
  &nbsp; Kembali
  
  </Text>
</View>
}
            </TouchableOpacity>            
          </Right>
        </Header>


{  (this.state.myshowredeem==0) ?  
  <WebViewMOS   parentUID = {this.state.myuidd} />
  :
  // <ViewRedeem/>

  <Tabs
     renderTabBar={()=> <ScrollableTab />}> 

  <Tab 
         heading={ <TabHeading /*style={{backgroundColor:  "#008C45"}}*/
          >
            <Image source={require('../../../assets/images/redeemcoin.png')}  style={styles.ImageStyle} />
                <Text>Redeem Point</Text>
            </TabHeading>}
          > 
 <View style = {styles2.kontener}>
  <WebView
  // source = {{ uri: this.state.myalamat }}
  source = {{ uri: myalamat }}
  />
   </View>
   </Tab>
   <Tab 
         heading={ <TabHeading style={{backgroundColor:  "#008C45" /*'#0bc406'*/}}
          >
            <Image source={require('../../../assets/images/historycoin.png')}  style={styles.ImageStyle} />
                <Text>History Point</Text>
            </TabHeading>}
          > 
 <View style = {styles2.kontener}>
  <WebView
  // source = {{ uri: this.state.myalamat2 }}
  source = {{ uri: myalamat2 }}
  />
   </View>
   </Tab>

</Tabs>
}

{  (this.state.myshowredeem==0) ?  
 <Tabs  style={{ backgroundColor: "#008C45" }}
 renderTabBar={()=> <ScrollableTab />}> 
         <Tab 
         heading={ <TabHeading   style={{width:"34%"}}
          >
            <Image source={ tab1pict}  style={styles.ImageStyle} />
                        <Text 
                        >Trip</Text>
            </TabHeading>}
          >
          {/* <Content style={{flex: 1}}> */}
          <Tab1 logimei={this.logimei} />

            {/* </Content> */}
          </Tab>

          <Tab heading={ <TabHeading   style={{width:"33%"}}
          >
            <Image source={require('../../../assets/images/mosorder.png')}  style={styles.ImageStyle} />
                        <Text 
                        >Ambil</Text>
            </TabHeading>}
          >
          {/* <Content> */}
          <Tab2 />

            {/* </Content> */}
          </Tab>
          <Tab heading={ <TabHeading  style={{width:"33%"}}
          >
            <Image source={require('../../../assets/images/mosdelivery-truck.png')}  style={styles.ImageStyle} />
                      <Text 
                      >Antar</Text>
            </TabHeading>}
          >
          {/* <Content> */}
          <Tab3/>

            {/* </Content> */}
            </Tab>
</Tabs> 
: null }

{
 this.state.isLoading ? 
  <View style={styles.spinnercontainer}>
    <Spinner/>
    <Text style={styles.spinnertext}>Loading...</Text>
  </View>
  : false
  }
      </Container>
      </StyleProvider>

    );
  }  


}




const styles2 = StyleSheet.create({
  container: {
    // flex: 1,
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start', // if you want to fill rows left to right
    // marginTop: 2,
    // marginBottom: 2
  },
  kontener: {
    flex:1 ,
    height: '80%',
    backgroundColor: "#88f89d"
 },
 item1: {
     width: '75%', // is 50% of container width
     justifyContent: 'center',
     alignItems: 'center',
     paddingTop: 10
   },
   item2: {
    width: '25%', // is 50% of container width
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingTop: 10
    }  
})


export default HomeView;
