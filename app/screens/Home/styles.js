import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container : {
    backgroundColor:'#f3f3f3',
    flex: 1,
    alignItems:'center',
    justifyContent :'center'
  },
  garisHeader: {
    width: 50,
    height: '80%',
    backgroundColor: 'red',
    transform: [
      {rotate: '45deg'}
    ]
  },
  ImageStyle: {
    alignItems: 'center',
    height: 30,
    width: 30,
    resizeMode: 'contain'
  },
  ImageStyle01: {
    alignItems: 'center',
    height: 22,
    width: 35
  },  
  ImageStyle01A: {
    alignItems: 'center',
    height: 22,
    width: 22
  },    
  ImageStyle02: {
    alignItems: 'center',
    height: 35,
    width: 35,
    resizeMode: 'contain'
  },
  ImageStyle03: {
    alignItems: 'center',
    height: 35,
    width: 35,
    resizeMode: 'contain'
  },
  ImageStyle04: {
    alignItems: 'center',
    height: 45,
    width: 45,
    resizeMode: 'contain'
  },
  CardItemStyle: {
    alignItems: 'center',
    textAlign: 'center'
  },    
  button: {
    width: 140,
    height: 40,
    backgroundColor:'#008D45',
    alignItems: 'center',
    justifyContent: 'center',    
     borderRadius: 5,
      marginVertical: 5,
      paddingVertical: 5,
      paddingHorizontal: 5
  },  
  buttonkecil: {
    width: 110,
    height: 50,
    backgroundColor:'#008D45',
    alignItems: 'center',
    justifyContent: 'center',    
     borderRadius: 5,
      marginVertical: 5,
      paddingVertical: 5,
      paddingHorizontal: 5
  },  
  buttonkecilm: {
    width: 110,
    height: 50,
    backgroundColor:'#c7184a',
    alignItems: 'center',
    justifyContent: 'center',    
     borderRadius: 5,
      marginVertical: 5,
      paddingVertical: 5,
      paddingHorizontal: 5
  },  
  buttonMap: {
    width: 80,
    backgroundColor: '#147ec1' /*'#69b3ab'*/,
    alignItems: 'center',
    justifyContent: 'center',    
     borderRadius: 5,
      marginVertical: 2,
      paddingVertical: 2,
      paddingHorizontal: 2
  },
  spinnercontainer:{
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: '#00000033',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonkhusus: {
    width: 140,
    height: 40,
    backgroundColor:'#0b8d45',
    alignItems: 'center',
    justifyContent: 'center',    
     borderRadius: 5,
      marginVertical: 5,
      paddingVertical: 5,
      paddingHorizontal: 5
  },  
  headerkhusus: {
    flex: 1,
    width: 140,
    height: 40,
    backgroundColor:'#0b8d45',
    alignItems: "flex-start",
    justifyContent: "flex-start",    
     borderRadius: 5,
      marginVertical: 5,
      paddingVertical: 5,
      paddingHorizontal: 5
  },  
  nodata:{
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default styles;
