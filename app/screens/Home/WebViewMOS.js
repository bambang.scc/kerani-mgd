import React, { Component } from 'react'
import { View, StyleSheet, YellowBox, WebView } from 'react-native'
import { tsConstructorType } from '@babel/types'
// import { WebView } from 'react-native-webview'

YellowBox.ignoreWarnings(['WebView'])


class WebViewExample extends Component{
   constructor(props){
      super(props);
   }
   render(){
      return (
         // <View style = {styles.container}>
         //    <WebView
         //    source = {{ uri:
         //    'https://sandboxdev.enseval.com/slider-mostrans/' }}
         //    />
         // </View>
         <View style = {styles.container}>
         <WebView
         source = {{ uri:
         'https://mostrans.co.id/slidermostrans/slider/' + this.props.parentUID }}
         />
      </View>
       )
   }
   
}
export default WebViewExample;

// 'https://emosservice3.enseval.com/Payment/slideremos.aspx?custnumber=34748&shipto=35116' }}

const styles = StyleSheet.create({
   container: {
      height: 146,
      backgroundColor: "#fff"
   }
})