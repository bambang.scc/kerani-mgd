import React, { Component } from 'react'
import { View, StyleSheet, YellowBox, WebView } from 'react-native'
// import { WebView } from 'react-native-webview'

YellowBox.ignoreWarnings(['WebView'])

const WebViewExample = () => {
   return (
      <View style = {styles.container}>
         <WebView
         source = {{ uri:
         'https://mostrans.co.id/redeempoint/?q=tke6RSrJQuv4lcoeAHcrQi6mwTLZKSIUUSNWptNDrWqMxDr/y9C0vcp3aLhVSDGu+9he6gC2HB9CJ1f9LkvsA==' }}
         />
      </View>
   )
}
export default WebViewExample;

// 'https://emosservice3.enseval.com/Payment/slideremos.aspx?custnumber=34748&shipto=35116' }}

const styles = StyleSheet.create({
   container: {
      flex:1 ,
      height: '80%'
   }
})