import React, { Component } from 'react'
import {
   StyleSheet,
   View,
   ScrollView,
   ActivityIndicator,
   FlatList,
   Text,TextInput,
   TouchableOpacity, ListView,Modal, TouchableHighlight,Alert, PermissionsAndroid, Platform, Linking, Image
   } from "react-native";
   import AsyncStorage from '@react-native-community/async-storage';   
import {
   Button, Icon, Spinner
} from "native-base";
import {encrypt, decrypt, getUrl, version} from '../../../assets/lib/brosky';
import Axios from 'axios';
import styles2 from './styles';
import {ToastAndroid} from 'react-native';
import QRCode from 'react-native-qrcode';

import { CameraKitCameraScreen } from 'react-native-camera-kit';
import firebase from 'react-native-firebase';
import ImageResizer from 'react-native-image-resizer';

let url;

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
let myuid = 0;

const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

// let modVisible = false;

// let mylatitude = 0;
// let mylongitude = 0;

const kompressimage = (data) => new Promise((resolve)=>{
  ImageResizer.createResizedImage(data, 650, 650, 'JPEG', 85, 0, null)
  .then(response => {
    resolve(response.path);
  })
  .catch(err => {
    resolve(event.captureImages[0].uri);
  });
});

export default class Tab1 extends React.Component {

   constructor(props) {
    super(props);
    this.state = {
      loading: true,
      dataSource:ds,
      keyid: '0',
      latitude: null,
      longitude: null,
      // keyid: 0,
      error: null,
      modalVisible: false,
      myuidd: '0',
      isLoading: true,
      QRCodeSimantra: '',
      modalQRCodeSimantra: false,
      nilaiBiayaKuli: '',
      urifoto1: '',
      urifoto2: '',
      urifoto3: '',
      urifoto4: '',
      urifoto5: '',
      urlfoto1: '',
      urlfoto2: '',
      urlfoto3: '',
      urlfoto4: '',
      urlfoto5: '',      
      isPermitted: false,
      selesaiCheck: false
    //   curTrip: 0,
        };
   }


   componentWillUnmount(){
     clearInterval(this.interval)
   }

  //  async _trip(visible,kunciid,statusjrk){
  //   if (statusjrk=="") {
  //   this.setState({keyid: String(kunciid)});
  //   this.setState({modalVisible: visible});
  //   }
  // };


  // http://www.techup.co.in/react-native-fetch-post-request-registration-form/

clearVar(){
  this.setState({nilaiBiayaKuli:''});
  this.setState({urifoto1:''});
  this.setState({urlfoto1:''});
  this.setState({ selesaiCheck : false });
}

  onPressTanya(UrutPhotox){
    if (this.state.nilaiBiayaKuli == ""){
      Alert.alert('Mostrans','Isi dahulu Biaya Kuli');
    }
    else
    {
    Alert.alert(
      'Mostrans',
      'Simpan Biaya Kuli dan Lanjutkan dengan photo Kwitansi?',
      [
        {
          text: 'Batal',
          onPress: () => this.setState({ selesaiCheck : false }),
          style: 'cancel',
        },
        {text: 'Ya', onPress: () => {
          this.setState({ selesaiCheck : true });
          this.onPress(UrutPhotox);
                }
                },
      ],
      {cancelable: false},
    );
    }
   }

   onPress(UrutPhotox) {
    UrutPhoto = UrutPhotox;
    var that = this;
    if (Platform.OS === 'android') {
      async function requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
              title: 'CameraExample App Camera Permission',
              message: 'CameraExample App needs access to your camera ',
            }
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //If CAMERA Permission is granted
            //Calling the WRITE_EXTERNAL_STORAGE permission function
            requestExternalWritePermission();
          } else {
            alert('CAMERA permission denied');
          }
        } catch (err) {
          alert('Camera permission err', err);
          console.warn(err);
        }
      }
      async function requestExternalWritePermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
              title: 'CameraExample App External Storage Write Permission',
              message:
                'CameraExample App needs access to Storage data in your SD Card ',
            }
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //If WRITE_EXTERNAL_STORAGE Permission is granted
            //Calling the READ_EXTERNAL_STORAGE permission function
            requestExternalReadPermission();
          } else {
            alert('WRITE_EXTERNAL_STORAGE permission denied');
          }
        } catch (err) {
          alert('Write permission err', err);
          console.warn(err);
        }
      }
      async function requestExternalReadPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            {
              title: 'CameraExample App Read Storage Read Permission',
              message: 'CameraExample App needs access to your SD Card ',
            }
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //If READ_EXTERNAL_STORAGE Permission is granted
            //changing the state to re-render and open the camera
            //in place of activity indicator
            that.setState({ isPermitted: true });
            // this.setState({ isPermitted: true });
          } else {
            alert('READ_EXTERNAL_STORAGE permission denied');
          }
        } catch (err) {
          alert('Read permission err', err);
          console.warn(err);
        }
      }
      //Calling the camera permission function
      requestCameraPermission();
     // alert('1');
      } else {
      this.setState({ isPermitted: true });
      //alert('22');
    }
  }
  async onBottomButtonPressed(event) {
    
    // alert('event.type '+event.type);
  
    const captureImages = JSON.stringify(event.captureImages);
    if (event.type === 'left') {
      this.setState({ isPermitted: false });
    } else if (event.type === 'capture') {
    
      let mycaptureimages;
      await kompressimage(event.captureImages[0].uri).then(result=>{
          mycaptureimages=result
      });

      if (UrutPhoto=="201") {
      this.setState({ urifoto1: mycaptureimages, isPermitted: false}); }
      else if (UrutPhoto=="202") {
        this.setState({ urifoto2: mycaptureimages, isPermitted: false}); }
        else if (UrutPhoto=="203") {
          this.setState({ urifoto3: mycaptureimages, isPermitted: false}); }
          else if (UrutPhoto=="204") {
            this.setState({ urifoto4: mycaptureimages, isPermitted: false}); }
            else if (UrutPhoto=="205") {
              this.setState({ urifoto5: mycaptureimages, isPermitted: false}); }
                    
    }
  }

  uploadImage1 = (uri, id_order)=> new Promise((resolve, reject)=>{
    // console.log('start1');
    // console.log(id_order+'_201');
    const imageRef = firebase.storage().ref('FotoPOD/').child(id_order+'_201');
    // const imageRef = firebase.storage().ref('FotoPOD/').child('A_1');
    // console.log('start2');


    imageRef.putFile(uri);
    // console.log('start3');

    let icount=0;
    for (icount = 0; icount < 30; icount++){
      // console.log('icount='+icount);
      imageRef.getDownloadURL().then(async (result)=>{
      this.setState({urlfoto1: result});
      // console.log('result image:'+result);
      resolve(result);
      icount=31;
    })
    .catch(error=>{
      // console.log('errorimage: '+error);
      reject(result);
    })
    this.timehold();

    }
    // console.log('start4');
  })

  async timehold(){ //must be async func
    //do something here
    await sleep(1000) //wait 1 seconds
    //continue on...
  }


  onGetCodeSimantra(nshipmentNumber){

    this.setState({ QRCodeSimantra : "" });

    if (nshipmentNumber!="") {
      
         fetch('https://mostrans.co.id/simantra/getqr', {
                    method: 'POST',
                    headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                      "no_spm" : nshipmentNumber
                    }),
                  }).then((response) => response.json())
                      .then((responseJson) => {
                        if (responseJson.status=='success'){
                             this.setState({ QRCodeSimantra : responseJson.data });                          
                        };
                        // Alert.alert(responseJson.data);
                        // Alert.alert(responseJson.status);
                      })
                      .catch((error) => {
                        console.error(error);
                        Alert.alert(error);
                      });     
    }              
    
    // if (this.state.QRCodeSimantra!=""){    
      this.setState({modalQRCodeSimantra:true});
    // } else {
    //    alert('QR Code Simantra tidak ada');
    // }
  }

  TutupSimantra(){
    this.setState({modalQRCodeSimantra:false});
  }

  onSendVendorGPSXXX(){
    var that = this;
    var url = 'https://mostrans.co.id/GPSVendor/api/testSubmit/';

    alert('XX');

    //  console.log("url:"+url);
  
    fetch(url,{
          method: 'POST',
          body: JSON.stringify(
            {
      "plat_nomor" : "ZZZZZZZ",
      "id_trip" : "TRIP-202017142418",
      "tanggal_sampai" :"2020-01-08"
            }
            )
          }).then(function (response) {
            return response.json();
          }).then(function (result) { 
            // console.log(result);
            if(!result.error){
             that.setState({ status: result.error,
                             wholeResult: result,
                          });
             Alert.alert("User register successfully \n userId: "+that.state.wholeResult.user.uid);
            //  console.log(that.state.wholeResult.user.uid);
         }else{
          Alert.alert(result.error_msg);
          // console.log(result);
    }
 }).catch(function (error) {
    // console.log("-------- error ------- "+error);
    alert("result:"+error)
  });

  // alert('ZZ');

}


  async onSendVendorGPS() {

    // alert("https://mostrans.co.id/GPSVendor/api/testSubmit/");
    // console.log("https://mostrans.co.id/GPSVendor/api/testSubmit/");

    var data = {
      "plat_nomor":"MMMMMM",
      "id_trip":"TRIP-202017142418",
      "tanggal_sampai":"2020-01-08",
      "list_order":
      [
        {
          "id_order":"GCM2001070002",
          "location_asal":{
            "lattitude":"-6.192631899999999",
            "longitude":"106.91011409999999"
          },
          "location_tujuan":{
            "lattitude":"-6.4153219",
            "longitude":"107.4113721"
          }
        }
      ]
    };
    try {
       let response = await fetch(
          "https://mostrans.co.id/GPSVendor/api/testSubmit/",
              {
                method: "POST",
                headers: {
                  "Accept": "application/json",
                  "Content-Type": "application/json; charset=utf-8"
                  },
                  body: JSON.stringify(data)
                }
                );
    // if (response.status >= 200 && response.status < 300) {
    // alert("authenticated successfully!!!");
    // }
    } catch (errors) {   
    alert(errors);
    } 
  };



  // if (statusjrk=="" || statusbatal!="" || version.type=='dev') {


  async _trip(visible,kunciid,statusjrk,statusbatal){
    if (statusjrk=="" || statusbatal!="" ) {
    this.setState({keyid: String(kunciid)});
    this.setState({modalVisible: visible});
    }
  };


  async _triptutup(visible,kunciid,statusjrk){
    this.setState({keyid: String(kunciid)});
    this.setState({modalVisible: visible});

    this.clearVar();
    // modVisible = visible;
  };


  async _tripproses(visible,kunciid,statusid,trip_id){
    // this.props.navigation.navigate('Trip');
    // this.props.navigation.replace('Trip');

    // Alert.alert('test1');

    if( statusid == 5 && this.state.nilaiBiayaKuli == '') {
      Alert.alert('Biaya Kuli', 'Biaya Kuli belum diisi. Isi 0 jika Biaya Kuli tidak ada'); // + kunciid);
    }
    else if( statusid == 5 && this.state.nilaiBiayaKuli != 0 && this.state.urifoto1 == '') {
      Alert.alert('Foto Kwitansi Belum Ada', 'Silahkan Click pada gambar kamera untuk memfoto Kwitansi Biaya Kuli'); // + kunciid);
    }
    else{

    this._tripstatus(kunciid,statusid,trip_id);
    // this._order(kunciid);
    this.setState({keyid: String(kunciid)});
    this.setState({modalVisible: visible});
    // modVisible = visible;
    this.queryUtama(trip_id);
    // this.componentDidMount();
    };
    // if (statusid==2) {      
    // //  alert("order diterima!!!!!");
    //  this.onSendVendorGPS();
    // }

    // Alert.alert('test1');


  };

  async _tripbatal(visible,trip_id){

    let myquery = "";

    console.log("Batal line 1 : update mt_master_trip a set status='confirm' where id="+trip_id+" and "+
    "(select count(1) from mt_trip_order_transaction aa, mt_shipper_order bb where aa.trip_id=a.id and aa.order_id=bb.id and bb.status not in (2,3))=0");

    await encrypt("update mt_master_trip a set status='confirm' where id="+trip_id+" and "+
    "(select count(1) from mt_trip_order_transaction aa, mt_shipper_order bb where aa.trip_id=a.id and aa.order_id=bb.id and bb.status not in (2,3))=0" ).then((result)=>{
    myquery = result;
    });
    Axios.post(url.select,{
    query: myquery
    })
    .then((response)=>{      
      let temp = response.data;
    //  console.log(response.data);
    })
    .catch(error=>{
    //  console.log("error+2++++++++++++++++++++++++++++++"+error);
    null;
    });
    
    console.log("Batal line 2 : update mt_shipper_order set status=2 where id in ( select order_Id from mt_trip_order_transaction where trip_id = "+trip_id+" ) and status=3");

    await encrypt("update mt_shipper_order set status=2 where id in ( select order_Id from mt_trip_order_transaction where trip_id = "+trip_id+" ) and status in (2,3)" ).then((result)=>{
    myquery = result;
    });
    Axios.post(url.select,{
    query: myquery
    })
    .then((response)=>{      
      let temp = response.data;
    //  console.log(response.data);
    })
    .catch(error=>{
    //  console.log("error+3++++++++++++++++++++++++++++++"+error);
    null;
    });


    this.setState({modalVisible: visible});
    // modVisible = visible;
    this.queryUtama(trip_id);
  };




  async _tripstatus(kunciid,statusid,trip_id){

// alert('update mt_shipper_order set status=(status+1) where status='+statusid+' and id='+kunciid);

// alert('1');

let mySQLstatus = "";
let mySQLstring = "";
let mystatusnext = Number(statusid,10)+1;
// alert('mystatusnext  '+mystatusnext);
if (mystatusnext==3) {
    mySQLstatus = "accepted" ;
    // alert('3');
} else if (mystatusnext==4) {
  mySQLstatus = "arrival" ;
  // alert('4');
}  else if (mystatusnext==5) {
  mySQLstatus = "startloaded" ;
}  else if (mystatusnext==6) {
  mySQLstatus = "finishloaded" ;
}  else if (mystatusnext==7) {
  mySQLstatus = "ontheway" ;
}  else if (mystatusnext==8) {
  mySQLstatus = "dropoff" ;
}  else if (mystatusnext==9) {
  mySQLstatus = "startunloaded" ;
}  else if (mystatusnext==10) {
  mySQLstatus = "finishunloaded" ;
} 

// alert('2');

// alert('mySQLstatus '+mySQLstatus);

if (mySQLstatus!="") {
  // alert('finish mystatusnext'+mystatusnext);
  if (mystatusnext==3) {
     mySQLstring = ","+mySQLstatus+"_date=NOW(),"+mySQLstatus+"_by="+this.state.myuidd+","+
           mySQLstatus+"_longitude="+this.state.longitude+","+
           mySQLstatus+"_latitude="+this.state.latitude;
  } else {
    mySQLstring = ","+mySQLstatus+"_date=NOW(),"+
           mySQLstatus+"_longitude="+this.state.longitude+","+
           mySQLstatus+"_latitude="+this.state.latitude;
  }
}

// alert('update mt_shipper_order set status=(status+1)'+mySQLstring+' where status='+statusid+' and id='+kunciid );
// alert('3');


if (this.state.nilaiBiayaKuli == 0 || statusid != 5 ) {
  this.setState({ nilaiBiayaKuli: "0"});
   this.setState({ urlfoto1: ""});
}
else if (this.state.urifoto1 == "") { 
  this.setState({ urlfoto1: ""});
}
else {
await this.uploadImage1('file://' + this.state.urifoto1, kunciid).then(result=>{
  // console.log(result);
  null;
});      
// null;
}

// alert('4');


let myquery = '';
if ( statusid == 5 ) { 
myquery = 'update mt_shipper_order set status=(status+1)'+mySQLstring+',biaya_kuli_muat='+this.state.nilaiBiayaKuli+",kwitansi_kuli_muat='"+this.state.urlfoto1+"' where status="+statusid+' and id='+kunciid;
}
else
{
 myquery = 'update mt_shipper_order set status=(status+1)'+mySQLstring+' where status='+statusid+' and id='+kunciid;
};

// alert(myquery);
// console.log("kenapa+4"+myquery);

await encrypt( myquery ).then((result)=>{
myquery = result;
});
Axios.post(url.select,{
query: myquery
})
.then((response)=>{

  if (mystatusnext==6) {
  this._pilihanorder(kunciid,trip_id);
  }
  
  let temp = response.data;
//  console.log(response.data);
})
.catch(error=>{
//  console.log("error+4++++++++++++++++++++++++++++++"+error);
null;
});


// let myquery = "";
//  alert("insert into mt_log_location (lat,lang,trip_id,create_date,create_by,update_date,update_by) values ("+this.state.latitude+","+this.state.longitude+","+mycurTrip+",NOW(),"+this.state.myuidd+",NOW(),"+this.state.myuidd+")");
// alert('tab2 savetitik begin')
await encrypt("insert into mt_log_location (lat,lang,trip_id,create_date,create_by,update_date,update_by,keterangan,driver_id) values ("+this.state.latitude+","+this.state.longitude+","+trip_id+",NOW(),"+this.state.myuidd+",NOW(),"+this.state.myuidd+",'TAB2',"+this.state.myuidd+")" ).then((result)=>{
   myquery = result;
});
Axios.post(url.select,{
   query: myquery
 })
 .then((response)=>{
    let temp = response.data;
    //console.log(response.data);
 })
 .catch(error=>{
    // console.log("error+5++++++++++++++++++++++++++++++"+error);
    // alert('tab2 savetitik error'+error)
   null;
 });
//  alert('tab2 savetitik end')


if (mystatusnext==3) {
  let data1 = {
    "id" : kunciid,
    "status" : "3"
  };

// alert(JSON.stringify(data1));

  try {
    let response = fetch(
       "https://mostrans.co.id/NotifKontak/sendNotif",
           {
             method: "POST",
             headers: {
               "Accept": "application/json",
               "Content-Type": "application/json; charset=utf-8"
               },
                body: JSON.stringify(data1)
             }
             );
 } catch (errors) {   
 alert(errors);
 } 
};

this.clearVar();

};


async _pilihanorder(order_id,trip_id){

// alert("select id_order,halaman,jarak from (select z.* from (select trip_id,order_id,id_order,halaman, round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+this.state.longitude+" "+this.state.latitude+")',4326),ST_GeomFromText('POINT(' || lang || ' ' || " +
// "lat || ')',4326)) As numeric)/1000,2) jarak, ( case when order_id="+order_id+" then 0 else 1 end) urut_order  from pilihan_order_v) z where trip_id="+trip_id+"  order by z.jarak,z.urut_order) zz limit 1"
// );

  let myquery = "";
  await encrypt("select id_order,halaman,jarak from (select z.* from (select trip_id,order_id,id_order,halaman, round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+this.state.longitude+" "+this.state.latitude+")',4326),ST_GeomFromText('POINT(' || lang || ' ' || " +
  "lat || ')',4326)) As numeric)/1000,2) jarak, ( case when order_id="+order_id+" then 0 else 1 end) urut_order  from pilihan_order_v) z where trip_id="+trip_id+"  order by z.jarak,z.urut_order) zz limit 1"
  ).then((result)=>{
     myquery = result;
  });

  Axios.post(url.select,{
     query: myquery
   })
   .then((response)=>{
    let temp = response.data;
    // this.setState({curTrip: temp.data[0].id});

    Alert.alert(
      'Petunjuk',
      'Langkah selanjutnya yang disarankan adalah memproses Order# '+temp.data[0].id_order
      +' dengan jarak '+temp.data[0].jarak+" KM pada halaman "+temp.data[0].halaman,
      [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );


    // AsyncStorage.setItem("curTrip", temp.data[0].id, ()=>{
    //   console.log('xxxx');
    // });
  })
  .catch(error=>{
    // this.setState({curTrip: 0});
    // AsyncStorage.setItem("curTrip", '0', ()=>{      
    // });
  });

}


    async _settripid(kunciid){

      let myquery = "";
      await encrypt("select id from mt_master_trip where driver_id="+kunciid+" and status='accepted' limit 1" ).then((result)=>{
         myquery = result;
      });

      Axios.post(url.select,{
         query: myquery
       })
       .then((response)=>{
        let temp = response.data;
        // this.setState({curTrip: temp.data[0].id});
        AsyncStorage.setItem("curTrip", temp.data[0].id, ()=>{
          
        });
      })
      .catch(error=>{
        // this.setState({curTrip: 0});
        // AsyncStorage.setItem("curTrip", '0', ()=>{
          
        // alert(error.message);

          if (error.message=="undefined is not an object (evaluating 'n.data[0].id')") {
            this.setState({curTrip: 0});
            AsyncStorage.setItem("curTrip", '0', ()=>{          
            });
            }
    

        // });
      });

    };


  async queryUtama(myvalue){

    this.setState({ isLoading: true});
    let myquery = "";
    let myquery1 = "";

    if (version.app=="DRIVER") { myquery1=" trip_id="+myvalue; }
    else { myquery1=" kerani_id_asal="+this.state.myuidd; }

    // let strquery = "select z.*, "+
    // "(case when (jarak>0.5 and (status+1) in (4,5,6)) then 'Posisi Anda belum berada di lokasi' "+
    // " when (status=5 and NOW() < (startloaded_date + gettimeloading(id) * interval '1 minutes')) then 'Waktu tunggu proses muat sedang berlangsung' " +
    // " when (status=9 and NOW() < (startunloaded_date + gettimeunloading(id) * interval '1 minutes')) then 'Waktu tunggu proses bongkar sedang berlangsung' " +
    // " when (jarak2>0.5 and (status+1) in (8,9,10)) then 'Posisi Anda belum berada di lokasi' "+
    // " when (jarak>0.5 and (status+1) in (7)) then 'Posisi Anda sudah menjauh dari lokasi pengambilan' "+
    // " when (CURRENT_DATE-(jadwal_penjemputan::DATE))>0 and (status+1)=4 then 'Jadwal tiba sudah lewat '||(CURRENT_DATE-(jadwal_penjemputan::DATE))||' hari' "+
    // " when (status=3 and jarak>0.5) then 'Jarak belum dilokasi' else '' end) as statusjrk "+
    // " from ( SELECT id,trip_id,id_trip,no_container,order_id,id_order,pengambilan,addr_pengambilan,lat_pengambilan,lang_pengambilan,"+
    // "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+ this.state.longitude +" "+ this.state.latitude +")',4326)," +
    // "ST_GeomFromText('POINT(' || lang_pengambilan || ' ' || lat_pengambilan || ')',4326)) As numeric)/1000,2) jarak,"+
    // "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+ this.state.longitude +" "+ this.state.latitude +")',4326)," +
    // "ST_GeomFromText('POINT(' || lang_pengiriman || ' ' || lat_pengiriman || ')',4326)) As numeric)/1000,2) jarak2,"+
    // "status,statustxt,statusnext,customer_reference,remarks,instruksi,weight,dimension,statusasli,statusbatal,jadwal_penjemputan,txtjadwal_penjemputan,pengiriman,addr_pengiriman,startloaded_date,startunloaded_date,to_char(due_date,'DD-MON-YYYY HH24:MI') as due_date "+
    // ",shipment_number,kode_shipper FROM mos_trip_detail_v where "+myquery1+" and status between 2 and 5 ) z order by z.jadwal_penjemputan,z.jarak,z.id_order";

    // let strquery = "select z.*, '' as statusjrk "+
    // " from ( SELECT id,trip_id,id_trip,no_container,order_id,id_order,pengambilan,addr_pengambilan,lat_pengambilan,lang_pengambilan,"+
    // "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+ this.state.longitude +" "+ this.state.latitude +")',4326)," +
    // "ST_GeomFromText('POINT(' || lang_pengambilan || ' ' || lat_pengambilan || ')',4326)) As numeric)/1000,2) jarak,"+
    // "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+ this.state.longitude +" "+ this.state.latitude +")',4326)," +
    // "ST_GeomFromText('POINT(' || lang_pengiriman || ' ' || lat_pengiriman || ')',4326)) As numeric)/1000,2) jarak2,"+
    // "status,statustxt,statusnext,customer_reference,remarks,instruksi,weight,dimension,statusasli,statusbatal,jadwal_penjemputan,txtjadwal_penjemputan,pengiriman,addr_pengiriman,startloaded_date,startunloaded_date,to_char(due_date,'DD-MON-YYYY HH24:MI') as due_date "+
    // ",shipment_number,kode_shipper FROM mos_trip_detail_v where "+myquery1+" and status between 2 and 5 ) z order by z.jadwal_penjemputan,z.jarak,z.id_order";
    // " when (kode_shipper<>'SHP' and status=9 and NOW() < (startunloaded_date + gettimeunloading(id) * interval '1 minutes')) then 'Waktu tunggu proses bongkar sedang berlangsung' " +

    let strquery = "select z.*, "+
    "(case "+
    " when (kode_shipper<>'SHP' and status=5 and NOW() < (startloaded_date + gettimeloading(id) * interval '1 minutes')) then 'Waktu tunggu proses muat sedang berlangsung' " +
       " when (jarak>0.5 and (status+1) in (4,6)) then 'Posisi Anda belum berada di lokasi' "+
    " else '' end) as statusjrk "+
    " from ( SELECT id,trip_id,id_trip,no_container,order_id,id_order,pengambilan,addr_pengambilan,lat_pengambilan,lang_pengambilan,"+
    "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+ this.state.longitude +" "+ this.state.latitude +")',4326)," +
    "ST_GeomFromText('POINT(' || lang_pengambilan || ' ' || lat_pengambilan || ')',4326)) As numeric)/1000,2) jarak,"+
    "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+ this.state.longitude +" "+ this.state.latitude +")',4326)," +
    "ST_GeomFromText('POINT(' || lang_pengiriman || ' ' || lat_pengiriman || ')',4326)) As numeric)/1000,2) jarak2,"+
    "status,statustxt,statusnext,customer_reference,remarks,instruksi,weight,dimension,statusasli,statusbatal,jadwal_penjemputan,txtjadwal_penjemputan,pengiriman,addr_pengiriman,startloaded_date,startunloaded_date,to_char(due_date,'DD-MON-YYYY HH24:MI') as due_date "+
    ",shipment_number,kode_shipper FROM mos_trip_detail_v where "+myquery1+" and status between 2 and 5 ) z order by z.jadwal_penjemputan,z.jarak,z.id_order";

    // console.log(strquery);
    await encrypt(strquery
         ).then((result)=>{
       myquery = result;
    });    
    Axios.post(url.select,{
       query: myquery
     })
     .then((response)=>{
       let temp = response.data;
       //console.log(response.data);
       this.setState({dataSource: ds.cloneWithRows(temp.data)});
       this.setState({ isLoading: false});
     })
     .catch(error=>{
        // console.log("error+6++++++++++++++++++++++++++++++"+error);
        null;
     });

  }


   async componentDidMount(){
    url = await getUrl();
    // console.log('tab 2 reload');
    
    await AsyncStorage.getItem("uid",(error,value)=>{
      if(value!=null)  myuid=value ; 
      else { this.props.navigation.replace("Login"); }});

   let myuidd = "";
   let myvalue = 0;
   

   await decrypt(myuid).then(value=>{
      myuidd=value
   });
  //  console.log("myuid ==== "+myuidd);
   this.setState({myuidd: String(myuidd)});

   this._settripid(myuidd);
   await AsyncStorage.getItem("curTrip",(error,value)=>{
     if(value!=null)  myvalue = value ; 
     else { myvalue = 0 }});



this.interval=setInterval(async () => {
  // if (!modVisible)  
  if (!this.state.modalVisible)  
   {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        'title': 'Mostrans',
        'message': 'Allow app to access your location '
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      navigator.geolocation.getCurrentPosition(
        async (position) => {
            // console.log(position);
           this.setState({
             latitude: position.coords.latitude,
             longitude: position.coords.longitude
           });


           this._settripid(myuidd);
           await AsyncStorage.getItem("curTrip",(error,value)=>{
             if(value!=null)  myvalue = value ; 
             else { myvalue = 0 }});
 
      this.queryUtama(myvalue);


      },
        (error) => {
            // See error code charts below.
            ToastAndroid.showWithGravityAndOffset(
              'Tidak mendapatkan sinyal GPS',
              ToastAndroid.LONG,
              ToastAndroid.BOTTOM,
              25,
              50,
            );
            // console.log("Error===============");
            // console.log(error.code, error.message);
        },
        { enableHighAccuracy: false, timeout: 360000, maximumAge: 10000 }
    );
    } else {
      // alert("Location permission denied");
      ToastAndroid.showWithGravityAndOffset(
        'Tidak mendapatkan izin GPS',
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50,
      );
    }
  } catch (err) {
    // console.log("No Access  to location" + err);
    ToastAndroid.showWithGravityAndOffset(
      'Tidak mendapatkan akses lokasi',
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50,
    );
 
  }
  //   navigator.geolocation.getCurrentPosition(
  //     (position) => {
  //         console.log('============= get location ===========')
  //         console.log(position);
  //     },
  //     (error) => {
  //       console.log('============= error get location ===========')
  //       console.log(error)
  //     },
  //     { 
  //        enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 
  //     }
  //  );
}
  }, 1000*1);




        // navigator.geolocation.getCurrentPosition(
        //     (position) => {
        //       this.setState({
        //         latitude: position.coords.latitude,
        //         longitude: position.coords.longitude,
        //         error: null,
        //       });
        //     },
        //     (error) => this.setState({ error: error.message }),
        //     { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        //   );


        //  alert(mylatitude);
        //  alert(mylongitude);

      // alert("select z.* from ( SELECT trip_id,order_id,id_order,pengiriman,addr_pengiriman,lat_pengiriman,lang_pengiriman,"+
      // "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+this.state.latitude+" "+this.state.longitude+")',4326)," +
      // "ST_GeomFromText('POINT(' || lat_pengiriman || ' ' || lang_pengiriman || ')',4326)) As numeric)/1000,2) jarak,"+
      // "status,statustxt,statusnext,customer_reference,remarks,instruksi,weight,dimension FROM mos_trip_detail_v where trip_id="+myvalue+" and status between 6 and 10 ) z order by z.jarak");

      // console.log("select z.* from ( SELECT trip_id,order_id,id_order,pengiriman,addr_pengiriman,lat_pengiriman,lang_pengiriman,"+
      // "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT(0 0)',4326)," +
      // "ST_GeomFromText('POINT(' || lat_pengiriman || ' ' || lang_pengiriman || ')',4326)) As numeric)/1000,2) jarak,"+
      // "status,statustxt,statusnext,customer_reference,remarks,instruksi,weight,dimension FROM mos_trip_detail_v where trip_id="+myvalue+" and status between 6 and 10 ) z order by z.jarak")


      }

   render(){
    if (this.state.isPermitted) {
      // if (false) {
        return (
   
         <View>
         <Modal
            animationType="fade"
           transparent={false}
           alignItems="center"
           >
   
         <CameraKitCameraScreen
           // Buttons to perform action done and cancel
           actions={{ rightButtonText: '', leftButtonText: 'Tutup' }}
           onBottomButtonPressed={event => this.onBottomButtonPressed(event)}
           flashImages={{
             // Flash button images
             on: require('../../../assets/images/flashon.png'),
             off: require('../../../assets/images/flashoff.png'),
             auto: require('../../../assets/images/flashauto.png'),
           }}
           cameraFlipImage={require('../../../assets/images/flip.png')}
           captureButtonImage={require('../../../assets/images/capture.png')}
         />
         </Modal>
         </View>
       );
     } else {   
      return (     
      
        <View style={{height: '100%'}}>
         <ListView
         enableEmptySections='true'
         dataSource={this.state.dataSource}
         renderRow={(rowData) => 

         <View style = {styles.container}>

         <View style={styles.item1}>
         <Text style={{ fontSize: 25, color: 'black' }}>
         {rowData.jarak}
         </Text>
         <Text style={{ fontSize: 10, color: 'black' }}>KM</Text>

{  (rowData.statusasli=='accepted') ?       
         <TouchableOpacity  style={styles2.buttonMap}
         onPress={() => Linking.openURL('google.navigation:q='+rowData.lat_pengambilan+'+'+rowData.lang_pengambilan)}>
         <Text style={{ color: 'white' }}>Panduan</Text>
         <Text style={{ color: 'white' }}>Rute</Text>
         <Text style={{ color: 'white' }}>Perjalanan</Text>
         </TouchableOpacity>
         : null
}

{  (rowData.kode_shipper=='SHP') ?       
         <TouchableOpacity  style={styles2.buttonMap}
         onPress={() => this.onGetCodeSimantra(rowData.shipment_number) }>
         <Text style={{ color: 'white' }}>QR Code</Text>
         <Text style={{ color: 'white' }}>Simantra</Text>
         </TouchableOpacity>
         : null
}

{ (rowData.status==2) ?       
<Image source={require('../../../assets/images/order.png')}  style={styles2.ImageStyle03} />
: null
}
{ (rowData.status==4) ?       
  <Image source={require('../../../assets/images/cargo.png')}  style={styles2.ImageStyle04} />
  : null
} 
{ (rowData.status==5) ?       
  <Image source={require('../../../assets/images/delivery-finish.png')}  style={styles2.ImageStyle04} />
  : null
}  

         </View>

         
         <View style={styles.item2}>
         <TouchableOpacity style={styles.button} onPress={()=>this._trip(true,rowData.id,rowData.statusjrk,rowData.statusbatal)}>
         <View>

         {/* <Text>{rowData.id}</Text> */}

         <Text style={{ color: 'black' }}>{rowData.id_order}</Text>
         <Text>{rowData.id_trip}/{rowData.no_container}</Text>
         <Text>{rowData.txtjadwal_penjemputan}</Text>
         <Text>{rowData.pengambilan}</Text>
         <Text>{rowData.addr_pengambilan}</Text>
         <Text style={{ color: 'blue' }}>{rowData.statustxt} &#10140; <Text style={{ color: 'green' }}>{rowData.statusnext} {rowData.statusbatal}</Text></Text>
         {  (rowData.due_date !== null) ?
         <Text style={{ color: 'red' }}>Target Waktu : {rowData.due_date}</Text> : null
         }
         {  (rowData.statusjrk!="") ?
         <Text style={{ color: 'red' }}>{rowData.statusjrk}</Text> : null
         }
         </View>
         </TouchableOpacity>
         </View>




         <View>
        <Modal
           animationType="fade"
          transparent={true}
          alignItems="center"
          visible={(this.state.modalQRCodeSimantra)}
          >
          <View style={{ backgroundColor: 'rgba(0,0,0,0.6)', height: '100%' }}>
            <ScrollView>
          <View style={styles.boxShad}>
            <View style={{flex:1,flexDirection:'column', justifyContent: 'center',
        alignItems: 'center'}}>

{ ( this.state.QRCodeSimantra != "") ?
        <View style={{ width: 170, height: 170}}>
        <QRCode
          value={this.state.QRCodeSimantra}
          size={150}
          bgColor='black'
          fgColor='white'/>
</View>
: 
// null
<View style={{ width: 170, height: 170}}>
  <Text>QR Code Simantra</Text> 
  <Text>No Shipment : { rowData.shipment_number }  </Text> 
  </View>  
}

<View style={{ width: 170, height: 50}}>
            <TouchableHighlight style={styles2.button}
                onPress={() => {
                  this.TutupSimantra();
                }}>
                <Text style={{ color: 'white' }}>Tutup</Text>
              </TouchableHighlight>
              </View>
          </View>
          </View>
          </ScrollView>
          </View>
        </Modal>
        </View>






         <View>
        <Modal
           animationType="fade"
          transparent={true}
         //  backgroundColor='rgba(0,0,255,0.8)'
         //  justifyContent= "center"
          alignItems="center"
          visible={(this.state.modalVisible&&this.state.keyid==String(rowData.id))}
         //  onRequestClose={() => {
            // Alert.alert('Modal has been closed.');
         //  }}
          >
          <View style={{ backgroundColor: 'rgba(0,0,0,0.6)', height: '100%' }}>
            <ScrollView>
          <View style={styles.boxShad}>
            <View>
              {/* <Text>Hello World!</Text> */}

              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Nomor Order</Text>
                  </View>
                  <View style={styles.item4}>

                  {/* <Text>{this.state.keyid}</Text> */}
                  {/* <Text>{rowData.id}</Text> */}

                      <Text>{rowData.id_order}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Jadwal Penjemputan</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.txtjadwal_penjemputan}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>No Referensi Pelanggan</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.customer_reference}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Catatan</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.remarks}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Instruksi</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.instruksi}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Berat</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.weight} KG</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Dimensi</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.dimension} CBM</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Pengambilan</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.pengambilan}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Alamat</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.addr_pengambilan}
                      </Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Pengiriman</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.pengiriman}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Alamat</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.addr_pengiriman}
                      </Text>
                  </View>
              </View>

{ (rowData.status=='5') ?
              <View style = {styles.container1}>
              <View style={styles.item3}>
                  <Text>Biaya Kuli Rp.</Text>
              </View>
              <View style={styles.item4A}>
              <TextInput
                          style={{flex:1}}
                          underlineColorAndroid='rgba(0,0,0,0)'
                          placeholder="Isi Biaya Kuli disini. Isi 0 jika tidak ada Biaya Kuli."
                          disabled={this.state.selesaiCheck}
                          value={this.state.nilaiBiayaKuli}
                          keyboardType="number-pad"
                          onChangeText={(input)=>this.setState({nilaiBiayaKuli:input})}
                        />
              </View>
          </View>
: null
}
{ (rowData.status=='5') ?
              <View style = {styles.container1}>
              <View style={styles.item3}>
                  <Text>Foto Kwitansi</Text>
              </View>
              <View style={styles.item4A}>
              <TouchableOpacity 
                    onPress={() => {this.onPressTanya('201')} }>
<Image source={require('../../../assets/images/camera.png')} style={{ width: 40, height: 40 }} />
                   </TouchableOpacity>
              </View>
          </View>
: null
}

<View>
{ this.state.urifoto1 !== '' ? 
<View>
{/* <Text style={{ fontSize: 14, fontStyle: "italic"}}>Photo</Text> */}
<Image style={{width: 300, height: 300}} source={{uri: 'file://' + this.state.urifoto1}}/> 
</View>
: false}  
</View>

<View style = {styles.container1}>

{  (rowData.statusjrk=='' ) ?       
                  <View style={styles.item11}>
              <TouchableHighlight style={styles2.buttonkecil}
                onPress={() => {
                  this._tripproses(!this.state.modalVisible,rowData.order_id,rowData.status,rowData.trip_id);
                }}>
                <Text style={{ color: 'white', textAlign: 'center'}}>{rowData.statusnext}</Text>
              </TouchableHighlight>
              </View>
: null }

{  (rowData.statusbatal!='' ) ?       
              <View style={styles.item11}>
              <TouchableHighlight style={styles2.buttonkecilm}
              onPress={() => {
                this._tripbatal(!this.state.modalVisible,rowData.trip_id);
              }}>
              <Text style={{ color: 'white', textAlign: 'center'}}>Batal Pilih Trip ini</Text>
              </TouchableHighlight>
              </View>
: null }

<View style={styles.item11}>
              <TouchableHighlight style={styles2.buttonkecil}
              onPress={() => {
                this._triptutup(!this.state.modalVisible,0);
              }}>
              <Text style={{ color: 'white' }}>Tutup</Text>
              </TouchableHighlight>
              </View>

</View>





            </View>

          </View>
          </ScrollView>
          </View>
        </Modal>
      </View>






         </View>

}
/>
{ this.state.isLoading ? 
  // <View style={styles.spinnercontainer}>
  //   <Spinner/>
  //   <Text style={styles.spinnertext}>Loading...</Text>
  // </View>
  false
  : 
    this.state.dataSource.getRowCount() === 0 ? <View style={styles2.nodata}><Text>Tidak ada Data</Text></View> : false
  }
</View>
       );

      }
}
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'flex-start', // if you want to fill rows left to right
      marginTop: 2,
      marginBottom: 2
    },
    container1: {
      // flex: 1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'flex-start', // if you want to fill rows left to right
      // marginTop: 2,
      // marginBottom: 2
    },
    item1: {
      width: '30%', // is 50% of container width
      //flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      paddingTop: 10
    },
    item11: {
      width: '33%', // is 50% of container width
      //flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      paddingTop: 10
    },
    item2: {
      width: '70%', // is 50% of container width
      backgroundColor: '#eff9f1',
      paddingTop: 5,
      paddingBottom: 5,
      paddingLeft: 10
    },
    item3: {
      width: '35%', // is 50% of container width
      //flex: 1,
      // justifyContent: 'center',
      alignItems: 'flex-start',
      // paddingTop: 10
    },
    item4: {
      width: '65%', // is 50% of container width
      backgroundColor: '#eff9f1',
      alignItems: 'flex-start',
      // paddingTop: 5,
      // paddingBottom: 5,
      // paddingLeft: 10
    },
    item4A: {
      width: '65%', // is 50% of container width
      backgroundColor: '#ffffff',
      alignItems: 'flex-start',
      // paddingTop: 5,
      // paddingBottom: 5,
      // paddingLeft: 10
    },
    item5: {
      width: '50%', // is 50% of container width
      //flex: 1,
      // justifyContent: 'center',
      alignItems: 'flex-end',
      padding: 10,
      paddingTop: 20
    },
    item6: {
      width: '50%', // is 50% of container width
      //flex: 1,
      // justifyContent: 'center',
      alignItems: 'flex-start',
      padding: 10,
      paddingTop: 20
    },
    boxShad: {
      flexDirection: 'row',
      marginTop: 100,
      // marginBottom: 50,
      // width: '80%',
      borderWidth: 1,
      borderRadius: 8,
      // borderColor: '#ddd',
      // borderBottomWidth: 0,
      alignItems: 'center',
      // flexDirection: 'row',
      shadowColor: '#000',
      shadowOffset: { width: 5, height: 5 },
      // shadowOpacity: 0.2,
      shadowRadius: 5,
      elevation: 1,
      padding: 10,
      backgroundColor: 'white'
    },
    spinnercontainer:{
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      backgroundColor: '#00000033',
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    }
})

