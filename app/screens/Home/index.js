// import React, { Component } from "react";
// import ProfileContainer from "../Profile/ProfileContainer";
// import SideBar from "../../components/SideBar.js";
// import { createDrawerNavigator } from "react-navigation";

import React, { Component } from "react";
import HomeContainer from './HomeContainer';
import ProfileContainer from "../Profile/ProfileContainer";
import ChatContainer from "../Chat/ChatContainer";
/*import OrdersContainer from '../Orders/OrdersContainer';
import TripsContainer from '../Trips/TripsContainer';*/
import { createBottomTabNavigator } from "react-navigation";
import {
  Button,
  Text,
  Icon,
  Item,
  Footer,
  FooterTab,
  Label, StyleProvider
} from "native-base";

import getTheme from '../../../native-base-theme/components';
import platform from '../../../native-base-theme/variables/platform';

let BtnNActBgColor = '#0b8d45';
let BtnActBgColor = '#0b8d45' /*'#ffffff'*/;   
let BtnBgColor1 = '#0b8d45' /*'#ffffff'*/;
let BtnBgColor2 = '#0b8d45';
let BtnBgColor3 = '#0b8d45';

export default (MainScreenNavigator = createBottomTabNavigator(
  {  
    Home: { screen: props => <HomeContainer {...props}  /> },
/*    Orders: { screen: props => <OrdersContainer {...props} />},
    Trips: { screen: props => <TripsContainer {...props} />},*/
    Chat: { screen: props => <ChatContainer {...props}  /> },
    Profile: { screen: props => <ProfileContainer {...props}  /> }
  },
  {
    tabBarPosition: "bottom",
    tabBarComponent: props => {
      return (
        <StyleProvider style={getTheme(platform)}>
        <Footer  style={{ marginBottom:0,paddingBottom:0,borderBottomColor:"#0b8d45" }}      
         >
          <FooterTab
               /*style={{ backgroundColor: "#008C45"}}  */
               >
            <Button
              vertical
              active={props.navigation.state.index === 0}
              onPress={() => {
                BtnBgColor1 = BtnActBgColor;
                BtnBgColor2 = BtnNActBgColor;
                BtnBgColor3 = BtnNActBgColor;
                props.navigation.navigate("Home");
              }}
              style={{ backgroundColor: BtnBgColor1,  }}
              >
              <Icon type="MaterialIcons" name="dashboard" />
              <Text>Beranda</Text>
            </Button>
{/*            <Button
              vertical
              active={props.navigation.state.index === 1}
              onPress={() => props.navigation.navigate("Orders")}
            >
              <Icon type="MaterialIcons" name="description" />
              <Text>Orders</Text>
            </Button>
            <Button
              vertical
              active={props.navigation.state.index === 2}
              onPress={() => props.navigation.navigate("Trips")}
            >
              <Icon type="MaterialIcons" name="explore" />
              <Text>Trips</Text>
            </Button>*/}
            <Button
              vertical
              active={props.navigation.state.index === 1}
              onPress={() => {
                BtnBgColor1 = BtnNActBgColor;
                BtnBgColor2 = BtnActBgColor;
                BtnBgColor3 = BtnNActBgColor;                
                props.navigation.navigate("Chat");}}
              style={{ backgroundColor: BtnBgColor2}}
              >
              <Icon type="MaterialIcons" name="chat" />
              <Text>Chat</Text>
            </Button>
            <Button
              vertical
              active={props.navigation.state.index === 2}
              onPress={() => {
                BtnBgColor1 = BtnNActBgColor;
                BtnBgColor2 = BtnNActBgColor;
                BtnBgColor3 = BtnActBgColor;                
                 props.navigation.navigate("Profile");
              }}
              style={{ backgroundColor: BtnBgColor3}}
              >
              <Icon type="MaterialIcons" name="face" />
              <Text>Profil</Text>
            </Button>
          </FooterTab>
        </Footer>
        </StyleProvider>
      );
    }
  }


  ));
