import React, { Component } from 'react'
import {
   StyleSheet,
   View,
   ScrollView,
   ActivityIndicator,
   FlatList,
   Text,
   Platform,
   TouchableOpacity, ListView,Modal, TouchableHighlight,Alert, PermissionsAndroid, Dimensions, Image,
   } from "react-native";
   import AsyncStorage from '@react-native-community/async-storage';   
   import { CheckBox } from 'react-native-elements';
import {
   Button, Icon, Spinner, Body, ListItem,Tabs, Tab, TabHeading, ScrollableTab, StyleProvider
} from "native-base";
// import {encrypt, decrypt, url} from '../../../assets/lib/brosky';
// import CheckBox from 'react-native-check-box';
import {encrypt, decrypt, getUrl, version } from '../../../assets/lib/brosky';
import Axios from 'axios';
// import { SSL_OP_TLS_ROLLBACK_BUG } from 'constants';
// import TripModal from './TripModal.js';
import styles2 from './styles';
import BackgroundTimer from 'react-native-background-timer';
import DeviceInfo from 'react-native-device-info';
// @ts-ignore
import VIForegroundService from "@voximplant/react-native-foreground-service";
import CheckboxFormX from 'react-native-checkbox-form';
import { CameraKitCameraScreen } from 'react-native-camera-kit';
import firebase from 'react-native-firebase';

import ImageResizer from 'react-native-image-resizer';

import getTheme from '../../../native-base-theme/components';
import platform from '../../../native-base-theme/variables/platform';


// import SelectMultiple from 'react-native-select-multiple'

// https://snack.expo.io/@melch-inno/react-native-element-checkbox
// https://www.iconsdb.com/white-icons/cargo-ship-icon.html


let url;
let sound;
let whoosh;
let whoosh2;
let whoosh3;
let whoosh4;

let UrutPhoto = '';
let UrutPhotox = '';

let intcount1=0;
let intcount2=0;
// let intflag=1;
// let mycaptureimages = '';

const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}


const gApplicationName = DeviceInfo.getApplicationName();
const gDeviceId = DeviceInfo.getDeviceId();
const gBatteryLevel = "";
// DeviceInfo.getBatteryLevel();
const gBrand = DeviceInfo.getBrand();
const gDevice = DeviceInfo.getDevice();
const gDeviceName = DeviceInfo.getDeviceName();
const gFreeDiskStorage = DeviceInfo.getFreeDiskStorage();
const gMaxMemory = DeviceInfo.getMaxMemory();
const gModel = DeviceInfo.getModel();
const gPhoneNumber = DeviceInfo.getPhoneNumber();
const gPowerState = ""; 
// DeviceInfo.getPowerState();
const gTotalDiskCapacity = DeviceInfo.getTotalDiskCapacity();
const gTotalMemory = DeviceInfo.getTotalMemory();
const gIsBatteryCharging = "";
// DeviceInfo.isBatteryCharging();
const gDeviceType = DeviceInfo.getDeviceType();
const gUniqueID = DeviceInfo.getUniqueID();

const mockData = [
  {
      label: 'Identitas Lengkap dan Berlaku (SIM, STNK, KIR)',
      value: 'SIM'
  }
];

const mockData2 = [
  {
      label: 'Bebas Sampah dan Kotoran              ',
      value: 'BEBASSAMPAH'
  },
  {
    label: 'Tidak Bergelombang (Rata)               ',
    value: 'BEBASGELOMBANG'
  },
  {
    label: 'Tidak Ada Bagian Tajam                      ',
    value: 'BEBASTAJAM'
  },
  {
    label: 'Tidak Berlubang                                    ',
    value: 'BEBASLUBANG'
  },
  {
    label: 'Kering                                                      ',
    value: 'KERING'
  },
  {
    label: 'Bebas Serangga                                   ',
    value: 'BEBASSERANGGA'
  },
  {
    label: 'Tidak Berbau                                        ',
    value: 'BEBASBAU'
  },
  {
    label: 'Tidak Ada Tetesan Oli                         ',
    value: 'BEBASOLI'
  },
  {
    label: 'Tersedia Alas Terpal                            ',
    value: 'ALASTERPAL'
  }
];

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
let myuid = 0;
// let mylatitude = 0;
// let mylongitude = 0;
// let SIMCheck= false

const kompressimage = (data) => new Promise((resolve)=>{
  ImageResizer.createResizedImage(data, 650, 650, 'JPEG', 85, 0, null)
  .then(response => {
    resolve(response.path);
  })
  .catch(err => {
    resolve(event.captureImages[0].uri);
  });
});

export default class Tab1 extends React.Component {

   constructor(props) {
    super(props);
    this.state = {
      loading: true,
      dataSource:ds,
      dataSource2:ds,
      dataSource3:ds,
      //  dataSource4:ds,
      modalVisible: false,
      modalVisible2: false,
      latitude: null,
      longitude: null,
      keyid: '0',
      myuidd: '0',
      isLoading: true,
      data: [],
      SIMCheck: false,
      SampahCheck: false,
      GelombangCheck: false,
      TajamCheck: false,
      LubangCheck: false,
      KeringCheck: false,
      SeranggaCheck: false,
      BauCheck: false,
      OliCheck: false,
      TerpalCheck: false,
      notrip: '',
      urifoto1: '',
      urifoto2: '',
      urifoto3: '',
      urifoto4: '',
      urifoto5: '',
      urlfoto1: '',
      urlfoto2: '',
      urlfoto3: '',
      urlfoto4: '',
      urlfoto5: '',      
      isPermitted: false,
      selesaiCheck: false
        };
   }


   componentWillUnmount(){
    clearInterval(this.interval);
    clearInterval(this.interval2);
    clearInterval(this.interval3);
  }



  startService=async()=>{
    if (Platform.OS !== 'android') {
        console.log('Only Android platform is supported');
        return;
    }
    if (Platform.Version >= 26) {
        const channelConfig = {
            id: 'ForegroundServiceChannel',
            name: 'Notification Channel',
            description: 'Notification Channel for Foreground Service',
            enableVibration: false,
            importance: 2
        };
        console.log("===============================");
        console.log("Platform.Version: ");
        console.log(Platform.Version);
        await VIForegroundService.createNotificationChannel(channelConfig);
    }
    const notificationConfig = {
        id: 3456,
        title: 'Foreground Service',
        text: 'Foreground service is running',
        icon: 'ic_notification',
        priority: 0
    };
    if (Platform.Version >= 26) {
        notificationConfig.channelId = 'ForegroundServiceChannel';
    }


    this.interval2=setInterval(async () => {
      if (!this.state.modalVisible && !this.state.modalVisible2)
      {
        this._settripid(myuidd);
        let mycurTrip = "0"
        await AsyncStorage.getItem("curTrip",(error,value)=>{
          if(value!=null)  mycurTrip=value ; 
          });

        if (mycurTrip>0) {
          this.ambilTitik(mycurTrip,"TAB1 INT 300000");
        }
      }
     }, 1000*5*60);



    await VIForegroundService.startService(notificationConfig);
    // this._backposnew();
    // alert("aaa");
    // await VIForegroundService.runTask({
    //   taskName: 'Driver Act',
    //   delay: 5
    // });
}



myFClearData(){

  this.setState({urifoto1: ''});
  this.setState({urlfoto1: ''});
  this.setState({urifoto2: ''});
  this.setState({urlfoto2: ''});
  this.setState({urifoto3: ''});
  this.setState({urlfoto3: ''});
  this.setState({urifoto4: ''});
  this.setState({urlfoto4: ''});
  this.setState({urifoto5: ''});
  this.setState({urlfoto5: ''});

  this.setState({ SIMCheck: false  });
  this.setState({ SampahCheck: false  });
  this.setState({ GelombangCheck: false  });
  this.setState({ TajamCheck: false  });
  this.setState({ LubangCheck: false  });
  this.setState({ KeringCheck: false  });

  this.setState({ SeranggaCheck: false  });
  this.setState({ BauCheck: false  });
  this.setState({ OliCheck: false  });
  this.setState({ TerpalCheck: false  });

  this.setState({ selesaiCheck : false });

}


_onSelect = ( item ) => {
  // console.log(item);
  null;
};
  
  async _ChkIMEI(kunciid){

    let myquery = "";
    await encrypt("select (case when imei_hp='' then 0 when imei_hp is null then 0 else 1 end) status_imei, imei_hp from MT_MASTER_USER where id=" + kunciid ).then((result)=>{
       myquery = result;
    });

    Axios.post(url.select,{
       query: myquery
     })
     .then((response)=>{
      let temp = response.data;

      if (temp.data[0].status_imei=='0')
      {

          this._ChkIMEIU(kunciid);


      } else if (temp.data[0].imei_hp!=gUniqueID)
      {
// sengaja ga check imei untuk kerani
//         alert('User Anda sudah digunakan pada handphone lain');
//         AsyncStorage.clear((error)=>{
//           this.props.logimei();
        //    console.log("xx");
        // });  
        
        console.log("xx");
      };

      // });
    })
    .catch(error=>{

      null;
      // console.log("xx");
      // alert('Problem saat mencheck Unique ID');
      // AsyncStorage.clear((error)=>{
      //   this.props.navigation.replace("Login");
      // });
      // this.props.navigation.replace("Login");
  
    });

  };


  


  async _ChkIMEIU(kunciid){
    let myquery = "";
    // await 
    // alert("update mt_master_user set gApplicationName='"+gApplicationName+"',gDeviceId='"+gDeviceId+"',gBatteryLevel='"+gBatteryLevel+"',gBrand='"+gBrand+"',gDevice='"+gDevice+"',gDeviceName='"+gDeviceName+"',gFreeDiskStorage='"+gFreeDiskStorage+"',gMaxMemory='"+gMaxMemory+"',gModel='"+gModel+"',gPhoneNumber='"+gPhoneNumber+"',gPowerState='"+gPowerState+"',gTotalDiskCapacity='"+gTotalDiskCapacity+"',gTotalMemory='"+gTotalMemory+"',gIsBatteryCharging='"+gIsBatteryCharging+"',gDeviceType='"+gDeviceType+"',gUniqueID='"+gUniqueID+"',imei_hp='"+gUniqueID+"' where id="+kunciid);

    await encrypt("update mt_master_user set gApplicationName='"+gApplicationName+"',gDeviceId='"+gDeviceId+"',gBatteryLevel='"+gBatteryLevel+"',gBrand='"+gBrand+"',gDevice='"+gDevice+"',gDeviceName='"+gDeviceName+"',gFreeDiskStorage='"+gFreeDiskStorage+"',gMaxMemory='"+gMaxMemory+"',gModel='"+gModel+"',gPhoneNumber='"+gPhoneNumber+"',gPowerState='"+gPowerState+"',gTotalDiskCapacity='"+gTotalDiskCapacity+"',gTotalMemory='"+gTotalMemory+"',gIsBatteryCharging='"+gIsBatteryCharging+"',gDeviceType='"+gDeviceType+"',gUniqueID='"+gUniqueID+"',imei_hp='"+gUniqueID+"' where id="+kunciid ).then((result)=>{
       myquery = result;
    });
    Axios.post(url.select,{
       query: myquery
     })
     .then((response)=>{
          let temp = response.data;
        console.log(response.data);
     })
     .catch(error=>{
        // console.log("update mt_master_user set gApplicationName='"+gApplicationName+"',gDeviceId='"+gDeviceId+"',gBatteryLevel='"+gBatteryLevel+"',gBrand='"+gBrand+"',gDevice='"+gDevice+"',gDeviceName='"+gDeviceName+"',gFreeDiskStorage='"+gFreeDiskStorage+"',gMaxMemory='"+gMaxMemory+"',gModel='"+gModel+"',gPhoneNumber='"+gPhoneNumber+"',gPowerState='"+gPowerState+"',gTotalDiskCapacity='"+gTotalDiskCapacity+"',gTotalMemory='"+gTotalMemory+"',gIsBatteryCharging='"+gIsBatteryCharging+"',gDeviceType='"+gDeviceType+"',gUniqueID='"+gUniqueID+"',imei_hp='"+gUniqueID+"' where id="+kunciid);
        // console.log(error);
        null;
     });

  }




  async _backpos(){

    let myuidd = "";
    await decrypt(myuid).then(value=>{
       myuidd=value
    });
        
    this._settripid(myuidd);
    let mycurTrip = "0"
    await AsyncStorage.getItem("curTrip",(error,value)=>{
      if(value!=null)  mycurTrip=value ; 
      });

    if (mycurTrip>0) {
      // alert('mycurtrip '+mycurTrip);
      this.ambilTitik(mycurTrip,"TAB1 INT 300000 BACKGR");
    } else {
      // alert('mycurtrip 0000');
      this.ambilTitik('0',"TAB1 INT 300000 BACKGR");
    }

  }


  async _backposnew(){

    let myuidd = "";
    await decrypt(myuid).then(value=>{
       myuidd=value
    });
        
    this._settripid(myuidd);
    let mycurTrip = "0"
    await AsyncStorage.getItem("curTrip",(error,value)=>{
      if(value!=null)  mycurTrip=value ; 
      });

    if (mycurTrip>0) {
      // alert('mycurtrip '+mycurTrip);
      this.ambilTitik(mycurTrip,"TAB1 INT 300000 BACKPOSNEW");
    } else {
      // alert('mycurtrip 0000');
      this.ambilTitik('0',"TAB1 INT 300000 BACKPOSNEW");
    }

  }



   async _trip(visible,kunciid){
      // this.props.navigation.navigate('Trip');
      // this.props.navigation.replace('Trip');
      this._order(kunciid);
      this.setState({keyid: String(kunciid)});
      this.setState({modalVisible: visible});

      if (kunciid==0){       
        // this.setState({ selesaiCheck : false });
        this.myFClearData();
     };
      // if (kunciid==0) { intflag=1 } else { intflag=0 };
      // this.componentDidMount();
      // alert("test");
    };


    uploadImage1 = (uri, id_order)=> new Promise((resolve, reject)=>{
      // console.log('start1');
      // console.log(id_order+'_101');
      const imageRef = firebase.storage().ref('FotoPOD/').child(id_order+'_101');
      // const imageRef = firebase.storage().ref('FotoPOD/').child('A_1');
      // console.log('start2');
      imageRef.putFile(uri);

      let icount=0;
      for (icount = 0; icount < 30; icount++){
        // console.log('i='+icount);
        // console.log('start3');
        imageRef.getDownloadURL().then(async (result)=>{
          this.setState({urlfoto1: result});
          console.log('result image:'+result);
          resolve(result);
          icount=31;
        })
        .catch(error=>{
          // console.log('errorimage: '+error);
          reject(result);
        })
        // console.log('start4');
        this.timehold();
      }
    })

  
    async timehold(){ //must be async func
      //do something here
      await sleep(1000) //wait 1 seconds
      //continue on...
    }


    async _checklist(myidtrip){
      // this.props.navigation.navigate('Trip');
      // this.props.navigation.replace('Trip');
      this.simpanCheckList(myidtrip);



      this.setState({modalVisible2: false});

  // Alert.alert(
  //   'Petunjuk',
  //   'Trip# '+this.state.notrip+' sudah dipilih, silahkan melanjutkan pada halaman AMBIL',
  //   [
  //     {text: 'OK', onPress: () => console.log('OK Pressed')},
  //   ],
  //   {cancelable: false},
  // );
  // intflag=1;


      // this.componentDidMount();
      // alert("test");
    };


    async onSendVendorGPS(kunciid) {
  
      let data1 = null ;
      let data2 = null ;
      let data3 = [];
      // var data1end = "";
      let myquery = "";
      await encrypt("select a.id_trip::varchar id_trip,to_char(a.tanggal_sampai,'YYYY-MM-DD') tanggal_sampai,b.plat_nomor::varchar plat_nomor,d.id_order::varchar id_order,e.lat::varchar lat1,e.lang::varchar lang1,f.lat::varchar lat2,f.lang::varchar lang2 from mt_master_trip a, mt_master_kendaraan b, mt_trip_order_transaction c, mt_shipper_order d, mt_shipper_contact e, mt_shipper_contact f where a.kendaraan_id=b.id and c.trip_id=a.id and c.order_id=d.id and d.asal_id=e.id and d.tujuan_id=f.id and a.id="+kunciid ).then((result)=>{
         myquery = result;
      });
      Axios.post(url.select,{
         query: myquery
       })
       .then((response)=>{
        let temp = response.data;

        // data1 = {
        //   "plat_nomor":temp.data[0].plat_nomor,
        //   "id_trip":temp.data[0].id_trip,
        //   "tanggal_sampai":temp.data[0].tanggal_sampai,
        //   "list_order":
        //   [
        //     {
        //       "id_order":temp.data[0].id_order,
        //       "location_asal":{
        //         "lattitude":temp.data[0].lat1,
        //         "longitude":temp.data[0].lang1
        //       },
        //       "location_tujuan":{
        //         "lattitude":temp.data[0].lat2,
        //         "longitude":temp.data[0].lang2
        //       }
        //     }
        //   ]
        // };
 


        // data1 = "{ 'plat_nomor':'" + temp.data[0].plat_nomor +"'," +
        //   "'id_trip':'"+temp.data[0].id_trip +"'," +
        //   "'tanggal_sampai':'"+temp.data[0].tanggal_sampai +"'," +
        //   "'list_order':[";

        // data1end = "]}";


        // var data2 = "" ;
        for (ij = 0; ij < temp.data.length; ij++) 
        {

          data2 = {"id_order":temp.data[ij].id_order ,
          "location_asal":{
            "lattitude": temp.data[ij].lat1 ,
            "longitude": temp.data[ij].lang1 },
          "location_tujuan":{
            "lattitude": temp.data[ij].lat2 ,
            "longitude": temp.data[ij].lang2 }} ;

          data3.push( data2 ) ;

          //  if (ij>0){ data2 = data2 + "," };

        //  data2 = data2 + "{'id_order':'" + temp.data[ij].id_order +"'," +
        //   "'location_asal':{" +
        //     "'lattitude':'" + temp.data[ij].lat1 + "'," +
        //     "'longitude':'" + temp.data[ij].lang1 + "'}," +
        //   "'location_tujuan':{" +
        //     "'lattitude':'" + temp.data[ij].lat2 + "'," +
        //     "'longitude':'" + temp.data[ij].lang2 + "'}}" ;
        // this.data1.list_order.push(data2);
        };


        data1 = {
          "plat_nomor":temp.data[0].plat_nomor,
          "id_trip":temp.data[0].id_trip,
          "tanggal_sampai":temp.data[0].tanggal_sampai,
          "list_order": data3
        };


        // var datafinal = data1 + data2 + data1end;
        // datafinal = datafinal.replace("'",'"');

        // console.log(datafinal);
        // "https://mostrans.co.id/GPSVendor/api/testSubmit/",

        try {
          let response = fetch(
             "https://mostrans.co.id/GPSVendor/api/submitDataToVendor",
                 {
                   method: "POST",
                   headers: {
                     "Accept": "application/json",
                     "Content-Type": "application/json; charset=utf-8"
                     },
                      body: JSON.stringify(data1)
                    //  body: datafinal
                   }
                   );
       // if (response.status >= 200 && response.status < 300) {
       // alert("authenticated successfully!!!");
       // }
       } catch (errors) {   
       alert(errors);
       } 

      })
      .catch(error=>{ alert(errors);
      });
  
      // alert("https://mostrans.co.id/GPSVendor/api/testSubmit/");
      // console.log("https://mostrans.co.id/GPSVendor/api/testSubmit/");
  
      // try {
      //    let response = await fetch(
      //       "https://mostrans.co.id/GPSVendor/api/testSubmit/",
      //           {
      //             method: "POST",
      //             headers: {
      //               "Accept": "application/json",
      //               "Content-Type": "application/json; charset=utf-8"
      //               },
      //               body: JSON.stringify(data1)
      //             }
      //             );
      // // if (response.status >= 200 && response.status < 300) {
      // // alert("authenticated successfully!!!");
      // // }
      // } catch (errors) {   
      // alert(errors);
      // } 
    };
  

    async _tripproses(visible,kunciid,id_trip){
      // this.props.navigation.navigate('Trip');
      // this.props.navigation.replace('Trip');

      // if( this.state.urifoto1 == '') 
      if(false) 
      {
        Alert.alert('Foto Belum Ada', 'Silahkan Click pada gambar kamera untuk melanjutkan'); // + kunciid);
      }
      else{

        this._checklist(kunciid);
        this._tripstatus(kunciid,id_trip);
      this._order(kunciid);
      this.setState({keyid: String(kunciid)});
      this.setState({modalVisible: visible});
      this.myFClearData();

      console.log("queryutama:tripproses");
      this.queryUtama(this.state.myuidd);

      };
      // this.componentDidMount();
      // alert("test");
    };


    async _orderaccepted(id_trip){

      let myquery = "";
      await encrypt("select id from mt_shipper_order where id in ( select order_id from mt_trip_order_transaction where trip_id="+id_trip+") and status=2"
      ).then((result)=>{
         myquery = result;
      });    
      Axios.post(url.select,{
         query: myquery
       })
       .then((response)=>{
        let temp = response.data;
        for (let tempLoop of temp.data) {
           this._orderAPI(tempLoop.id);
        }
//        temp.data[0].id
      })
      .catch(error=>{
      });
    
    }

    async _orderAPI(id_order) {
      let data1 = {
        "id" : id_order,
        "status" : "3"
      };    
      try {
        let response = fetch(
           "https://mostrans.co.id/NotifKontak/sendNotif",
               {
                 method: "POST",
                 headers: {
                   "Accept": "application/json",
                   "Content-Type": "application/json; charset=utf-8"
                   },
                    body: JSON.stringify(data1)
                 }
                 );
     } catch (errors) {   
     alert(errors);
     } 

     await encrypt("update mt_shipper_order set status=3, update_date=now() where status=2 and id="+id_order ).then((result)=>{
        myquery = result;
     });
     Axios.post(url.select,{
        query: myquery
      })
      .then((response)=>{ 
        null;
        // console.log("sukses+A++++++++++++++++++++++++++++++"+error);
      })
      .catch(error=>{
         null;
        //  console.log("error+A++++++++++++++++++++++++++++++"+error);
      });

    }

    async _tripstatus(kunciid,id_trip){


      // console.log('Mulai Coba upload image! ' );
      // console.log('id_trip:' + kunciid );
      // console.log('file://' + this.state.urifoto1);
      //this.setState({modalisLoading: false});

      if (false) {
      //pindah ke onpress camera
      await this.uploadImage1('file://' + this.state.urifoto1, kunciid).then(result=>{
        null;
        // console.log(result);
      });      
      //pindah ke onpress camera
    }

      // console.log(this.state.urlfoto1);
      // console.log('Selesai Coba upload image! ' );


      let myquery = "";
      console.log("update mt_master_trip set status='accepted', update_date=now() where status='confirm' and id="+kunciid);
      //await encrypt("update mt_master_trip set status='accepted', update_date=now(),foto_checklist='" + this.state.urlfoto1 + "' where status='confirm' and id="+kunciid ).then((result)=>{
        await encrypt("update mt_master_trip set status='accepted', update_date=now() where status='confirm' and id="+kunciid ).then((result)=>{
         myquery = result;
      });
      Axios.post(url.select,{
         query: myquery
       })
       .then((response)=>{

//alert("update mt_master_trip set status='accepted' where status='confirm' and id="+kunciid );

AsyncStorage.setItem("curTrip", kunciid, ()=>{       
});

this.onSendVendorGPS(kunciid);
this._orderaccepted(kunciid);

// this.setState({modalVisible2: true});
// *** dari sini
  Alert.alert(
    'Petunjuk',
    'Trip# '+id_trip+' sudah dipilih, silahkan melanjutkan pada halaman AMBIL',
    [
      {text: 'OK', onPress: () => console.log('OK Pressed')},
    ],
    {cancelable: false},
  );
//   *** dari sini
this.setState({notrip: id_trip});


          let temp = response.data;
          // console.log(response.data);
       })
       .catch(error=>{
          // console.log("error+A++++++++++++++++++++++++++++++"+error);
          null;
       });
//
// await encrypt('update mt_shipper_order set status=3 where status=2 and id in (select order_id from mt_trip_order_transaction where trip_id='+kunciid+')' ).then((result)=>{
//   myquery = result;
// });
// Axios.post(url.select,{
//   query: myquery
// })
// .then((response)=>{
//    let temp = response.data;
//    console.log(response.data);
// })
// .catch(error=>{
//    console.log("error+++++++++++++++++++++++++++++++"+error);
// });
};


    // async _settripid(kunciid){

    //   let myquery = "";
    //   await encrypt("select id from mt_master_trip where driver_id="+kunciid+" and status='accepted' limit 1" ).then((result)=>{
    //      myquery = result;
    //   });

    //   Axios.post(url.select,{
    //      query: myquery
    //    })
    //    .then((response)=>{
    //     let temp = response.data;
    //     AsyncStorage.setItem("curTrip", temp.data[0].id, ()=>{
    //       console.log('xxxx');
    //     });
    //   })
    //   .catch(error=>{
    //     AsyncStorage.setItem("curTrip", '0', ()=>{
    //       console.log('xxxx');
    //     });
    //   });


    // };



    async _settripid(kunciid){


      let myquery = "";
      let nquery = "";
      if (version.app=='DRIVER') {  nquery = "select id from mt_master_trip where driver_id="+kunciid+" and status='accepted' limit 1";  }
      else if (version.app=='KERANI') {  nquery = "select id from mt_master_trip where kerani_id_asal="+kunciid+" and status='accepted' limit 1";  }

      await encrypt( nquery ).then((result)=>{
         myquery = result;
      });

      Axios.post(url.select,{
         query: myquery
       })
       .then((response)=>{
        let temp = response.data;
        // this.setState({curTrip: temp.data[0].id});
        AsyncStorage.setItem("curTrip", temp.data[0].id, ()=>{
          
        });
      })
      .catch(error=>{
        // this.setState({curTrip: 0});
        // AsyncStorage.setItem("curTrip", '0', ()=>{          
        // });

        if (error.message=="undefined is not an object (evaluating 'n.data[0].id')") {
          this.setState({curTrip: 0});
          AsyncStorage.setItem("curTrip", '0', ()=>{          
          });
          }
  

      });


    };



    async _order(kunciid){

      let myquery = "";
      await encrypt("select * from mos_trip_v where trip_id="+kunciid ).then((result)=>{
         myquery = result;
      });

      Axios.post(url.select,{
         query: myquery
       })
       .then((response)=>{
         let temp = response.data;
         this.setState({dataSource2: ds.cloneWithRows(temp.data)});
       })
       .catch(error=>{
         null;
          // console.log("error+B++++++++++++++++++++++++++++++"+error);
       });

    };


    async queryUtama(myuidd){
      let myquery = "";
      let nquery = "";

      // console.log("queryutama:app");
      // console.log("version.app:"+version.app);

      if (version.app=='DRIVER')
      { nquery="select * from mos_drv_trip_v where driver_id="+myuidd+" and role='DRIVER' and selisih_tgl<4 order by tanggal_pengiriman,jam_pengiriman"; }
      else if (version.app=='KERANI')
      { nquery="select * from mos_drv_trip_v where kerani_id_asal="+myuidd+"and role='KERANI'  and selisih_tgl<4 order by tanggal_pengiriman,jam_pengiriman"; }

// console.log("queryutamatab1 : "+nquery);

      await encrypt( nquery ).then((result)=>{
         myquery = result;
      });
      Axios.post(url.select,{
         query: myquery
       })
       .then((response)=>{
         let temp = response.data;
         //console.log(response.data);
         this.setState({dataSource: ds.cloneWithRows(temp.data)});
         this.setState({ isLoading: false});
       })
       .catch(error=>{
          null;
          // console.log("error+C++++++++++++++++++++++++++++++"+error);
       });

    }

    async queryCheck(myuidd){

      
      let myquery = "";
      let nquery = "";

      if (version.app=='DRIVER')
      { nquery="select count(1) jml from mos_drv_trip_v where driver_id="+myuidd+" and statustabtxt='Trip dapat diproses'"; }
      else if (version.app=='KERANI')
      { nquery="select count(1) jml from mos_drv_trip_v where kerani_asal_id="+myuidd+" and statustabtxt='Trip dapat diproses'"; }
      
      await encrypt( nquery ).then((result)=>{
         myquery = result;
      });
      Axios.post(url.select,{
         query: myquery
       })
       .then((response)=>{
         let temp = response.data;
        //  alert(temp.data[0].jml);
         if (temp.data[0].jml>0) {

            // var Sound = require('react-native-sound');
            // Sound.setCategory('Playback', true);              
            // var whoosh = new Sound('../../../assets/voice/trip02.mp3', Sound.MAIN_BUNDLE, (error) => {
              // if (error) {
              //   console.log('failed to load the sound ' + error.text , error);
              //   alert('sound1');

              // } else {
                whoosh2.play((success) => {
                  if (success) {
                    // console.log('successfully finished playing');
                    // alert('sound3');
                    null;
                  } else {
                    // console.log('playback failed due to audio decoding errors');
                    // alert('sound4');
                    null;
                  }
                });              
                whoosh.play((success) => {
                  if (success) {
                    // console.log('successfully finished playing');
                    // alert('sound3');
                    null;
                  } else {
                    // console.log('playback failed due to audio decoding errors');
                    // alert('sound4');
                    null;
                  }
                });              
            //  }
            
            // })

            //  alert('sound5');

              // whoosh.release();

            }
         })
       .catch(error=>{
          // console.log("error+D++++++++++++++++++++++++++++++"+error);
          null;
       });

    }



    async queryCheck2(mycurTrip){

      let myquery = "";
      await encrypt("select count(1) jml "+
      " FROM mos_trip_detail_v where trip_id="+mycurTrip+" and status=6 and " +
      "round(CAST(ST_Distance_Sphere(ST_GeomFromText('POINT("+ this.state.longitude +" "+ this.state.latitude +")',4326)," +
      "ST_GeomFromText('POINT(' || lang_pengambilan || ' ' || lat_pengambilan || ')',4326)) As numeric)/1000,2)>0.5 "
      ).then((result)=>{
         myquery = result;
      });
      Axios.post(url.select,{
         query: myquery
       })
       .then((response)=>{
         let temp = response.data;
        //  alert(temp.data[0].jml);
         if (temp.data[0].jml>0) {

            // var Sound = require('react-native-sound');
            // Sound.setCategory('Playback', true);              
            // var whoosh = new Sound('../../../assets/voice/trip02.mp3', Sound.MAIN_BUNDLE, (error) => {
              // if (error) {
              //   console.log('failed to load the sound ' + error.text , error);
              //   alert('sound1');

              // } else {
                whoosh4.play((success) => {
                  if (success) {
                    // console.log('successfully finished playing');
                    // alert('sound3');
                    null;

                  } else {
                    // console.log('playback failed due to audio decoding errors');
                    null;
                    // alert('sound4');

                  }
                });              
                whoosh3.play((success) => {
                  if (success) {
                    // console.log('successfully finished playing');
                    // alert('sound3');
                    null;

                  } else {
                    // console.log('playback failed due to audio decoding errors');
                    // alert('sound4');
                    null;
                  }
                });              
            //  }
            
            // })

            //  alert('sound5');

              // whoosh.release();

            }
         })
       .catch(error=>{
          // console.log("error+E++++++++++++++++++++++++++++++"+error);
          null;
       });

    }




  async ambilTitik(mycurTrip,ketitik){

    // alert('aa');
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'Mostrans',
          'message': 'Allow app to access your location '
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        navigator.geolocation.getCurrentPosition(
          async (position) => {
              // console.log(position);
             this.setState({
               latitude: position.coords.latitude,
               longitude: position.coords.longitude
             });
            this.saveTitik(mycurTrip,ketitik);
        },
          (error) => {
              // console.log("Error===============");
              // console.log(error.code, error.message);
              null;
          },
          { enableHighAccuracy: false, timeout: 360000, maximumAge: 10000 }
      );
      } else {
        alert("Location permission denied");
      }
    } catch (err) {
      // console.log("No Access  to location" + err);
      navigator.geolocation.getCurrentPosition(
        async (position) => {
            // console.log(position);
           this.setState({
             latitude: position.coords.latitude,
             longitude: position.coords.longitude
           });
          this.saveTitik(mycurTrip,ketitik);

      },
        (error) => {
            // console.log("Error===============");
            // console.log(error.code, error.message);
            null;
        },
        { enableHighAccuracy: false, timeout: 360000, maximumAge: 10000 }
    );
    }
  
  }

  async saveTitik(mycurTrip,ketitik){

    let myquery = "";
    // alert('tab1 savetitik begin')
    //  alert("insert into mt_log_location (lat,lang,trip_id,create_date,create_by,update_date,update_by) values ("+this.state.latitude+","+this.state.longitude+","+mycurTrip+",NOW(),"+this.state.myuidd+",NOW(),"+this.state.myuidd+")");
    await encrypt("insert into mt_log_location (lat,lang,trip_id,create_date,create_by,update_date,update_by,keterangan,driver_id) values ("+this.state.latitude+","+this.state.longitude+","+mycurTrip+",NOW(),"+this.state.myuidd+",NOW(),"+this.state.myuidd+",'"+ketitik+"',"+this.state.myuidd+")" ).then((result)=>{
       myquery = result;
    });
    Axios.post(url.select,{
       query: myquery
     })
     .then((response)=>{
        let temp = response.data;
        //console.log(response.data);
     })
     .catch(error=>{
        // console.log("error+F++++++++++++++++++++++++++++++"+error);
        // alert('tab1 savetitik error '+error)
        null;
      });
    //  alert('tab1 savetitik end')

  }


  async simpanCheckList(myidtrip){

    // this._settripid(this.state.myuidd);
    // let mycurTrip = "0"
    // await AsyncStorage.getItem("curTrip",(error,value)=>{
    //   if(value!=null)  mycurTrip=value ; 
    //   });    


    // alert(myidtrip);

    let mycurTrip = myidtrip;

      let myquery = "insert into mt_checklist_log select * from mt_checklist where trip_id="+mycurTrip;  
      await encrypt(myquery).then((result)=>{
        myquery = result;
     });
     Axios.post(url.select,{
        query: myquery
      })
      .then((response)=>{
         let temp = response.data;
      })
      .catch(error=>{
        //  console.log("error+G++++++++++++++++++++++++++++++"+error);
        null;
       });
  
      myquery = "delete from mt_checklist where trip_id="+mycurTrip;  
       await encrypt(myquery).then((result)=>{
         myquery = result;
      });
      Axios.post(url.select,{
         query: myquery
       })
       .then((response)=>{
          let temp = response.data;
       })
       .catch(error=>{
          // console.log("error+H++++++++++++++++++++++++++++++"+error);
          null;
        });
 





    if (this.state.SIMCheck) 
    { this.execSimpan(mycurTrip,'SIM','Identitas Lengkap dan Berlaku (SIM, STNK, KIR)','Y')
    } else { this.execSimpan(mycurTrip,'SIM','Identitas Lengkap dan Berlaku (SIM, STNK, KIR)','N') };
    this.setState({SIMCheck: false});

    if (this.state.SampahCheck) 
    { this.execSimpan(mycurTrip,'SAMPAH','Bebas Sampah dan Kotoran','Y')
    } else { this.execSimpan(mycurTrip,'SAMPAH','Bebas Sampah dan Kotoran','N') };
    this.setState({SampahCheck: false});

    if (this.state.GelombangCheck) 
    { this.execSimpan(mycurTrip,'GELOMBANG','Tidak Bergelombang (Rata)','Y')
    } else { this.execSimpan(mycurTrip,'GELOMBANG','Tidak Bergelombang (Rata)','N') };
    this.setState({GelombangCheck: false});

    if (this.state.TajamCheck) 
    { this.execSimpan(mycurTrip,'TAJAM','Tidak Ada Bagian Tajam','Y')
    } else { this.execSimpan(mycurTrip,'TAJAM','Tidak Ada Bagian Tajam','N') };
    this.setState({TajamCheck: false});

    if (this.state.LubangCheck) 
    { this.execSimpan(mycurTrip,'LUBANG','Tidak Berlubang','Y')
    } else { this.execSimpan(mycurTrip,'LUBANG','Tidak Berlubang','N') };
    this.setState({LubangCheck: false});

    if (this.state.KeringCheck) 
    { this.execSimpan(mycurTrip,'KERING','Kering','Y')
    } else { this.execSimpan(mycurTrip,'KERING','Kering','N') };
    this.setState({KeringCheck: false});

    if (this.state.SeranggaCheck) 
    { this.execSimpan(mycurTrip,'SERANGGA','Bebas Serangga','Y')
    } else { this.execSimpan(mycurTrip,'SERANGGA','Bebas Serangga','N') };
    this.setState({SeranggaCheck: false});

    if (this.state.BauCheck) 
    { this.execSimpan(mycurTrip,'BAU','Tidak Berbau','Y')
    } else { this.execSimpan(mycurTrip,'BAU','Tidak Berbau','N') };
    this.setState({BauCheck: false});

    if (this.state.OliCheck) 
    { this.execSimpan(mycurTrip,'OLI','Tidak Ada Tetesan Oli','Y')
    } else { this.execSimpan(mycurTrip,'OLI','Tidak Ada Tetesan Oli','N') };
    this.setState({OliCheck: false});

    if (this.state.TerpalCheck) 
    { this.execSimpan(mycurTrip,'TERPAL','Tersedia Alas Terpal','Y')
    } else { this.execSimpan(mycurTrip,'TERPAL','Tersedia Alas Terpal','N') };
    this.setState({TerpalCheck: false});
  }


   async execSimpan(mycurTrip,kodecheck,desccheck,flagcheck){

    let myquery = "insert into mt_checklist (trip_id,checklist_code,checlist_desc,checklist_flag,created_date,created_by,update_date,update_by) values "+
    "("+mycurTrip+",'"+kodecheck+"','"+desccheck+"','"+flagcheck+"',NOW(),'"+this.state.myuidd+"',NOW(),'"+this.state.myuidd+"')";

    await encrypt(myquery).then((result)=>{
      myquery = result;
   });
   Axios.post(url.select,{
      query: myquery
    })
    .then((response)=>{
       let temp = response.data;
    })
    .catch(error=>{
      //  console.log("error+I++++++++++++++++++++++++++++++"+error);
      null;
     });
   }


   async componentDidMount(){
     url = await getUrl();
    //  console.log('tab 1 reload');
      this.setState({ isLoading: true});
      await AsyncStorage.getItem("uid",(error,value)=>{
         if(value!=null)  myuid=value ; 
         else { this.props.navigation.replace("Login"); }});

      let myuidd = "";
      await decrypt(myuid).then(value=>{
         myuidd=value
      });
      // console.log("myuid ==== "+myuidd);
      this.setState({myuidd: myuidd});

      this._ChkIMEI(myuidd)
      this._settripid(myuidd);

      console.log("queryutama:componentdidmount");
      this.queryUtama(myuidd);

      // await AsyncStorage.getItem("curTrip",(error,value)=>{
      //   if(value!=null)  alert('ccc'+value) ; 
      //   else { alert('gak ono') }});



      Sound = require('react-native-sound');
      Sound.setCategory('Playback');
      // https://notevibes.com/cabinet.php
      whoosh = new Sound('infotrip.mp3', Sound.MAIN_BUNDLE, (error) => {
        if (error) {
          // console.log('failed to load the sound ' + error.text , error);
          // alert('sound1');
          null;
        }
      })
      // http://soundbible.com/tags-alert.html
      whoosh2 = new Sound('doorchim.mp3', Sound.MAIN_BUNDLE, (error) => {
        if (error) {
          // console.log('failed to load the sound ' + error.text , error);
          // alert('sound1');
          null;
        }
      })

      whoosh3 = new Sound('alertberangkat.mp3', Sound.MAIN_BUNDLE, (error) => {
        if (error) {
          // console.log('failed to load the sound ' + error.text , error);
          // alert('sound1');
          null;
        }
      })

      whoosh4 = new Sound('vibrate.mp3', Sound.MAIN_BUNDLE, (error) => {
        if (error) {
          // console.log('failed to load the sound ' + error.text , error);
          // alert('sound1');
          null;
        }
      })


  //  let foregroundTask = async (data) => {
  //       await this._backposnew();
  //   }
  //   VIForegroundService.registerForegroundTask("Driver Act", foregroundTask);


      this.startService();
      await BackgroundTimer.stopBackgroundTimer();
      BackgroundTimer.runBackgroundTimer(async ()=>{
        // Axios.get('https://api.telegram.org/bot897893626:AAEpygMIMMIT8NKhXbo3Vik42UYkZGHTyKA/sendMessage?chat_id=384148240&text=hehe').then(response=>{
        //   alert(response.data.result.text);
        // });
        // this.getLocation();
        this._backpos();
        // console.log('service running');
      }, 1000*5*60);
    

    // getLocation=async()=>{
    //   GetLocation.getCurrentPosition({
    //     enableHighAccuracy: true,
    //     timeout: 15000,
    //   })
    //   .then(location => {
    //       console.log(location);
    //       let x = location.longitude + ','+location.latitude;
    //       Axios.get('https://api.telegram.org/bot897893626:AAEpygMIMMIT8NKhXbo3Vik42UYkZGHTyKA/sendMessage?chat_id=384148240&text='+x)
    //   })
    //   .catch(error => {
    //       const { code, message } = error;
    //       console.warn(code, message);
    //       let x = 'Gak dapet location bos';
    //       Axios.get('https://api.telegram.org/bot897893626:AAEpygMIMMIT8NKhXbo3Vik42UYkZGHTyKA/sendMessage?chat_id=384148240&text='+x)
    //   })
  

      this.interval=setInterval(async () => {
        // if (intflag==1) {
          if (!this.state.modalVisible && !this.state.modalVisible2)
          {
          console.log("queryutama:interval");
          this.queryUtama(myuidd);
          }
        // }
      }, 1000*1);

      this.interval3=setInterval(async () => {
        // if (intflag==1) {
        // intcount1=intcount1+1;
        // if (intcount1==5)
        // {
          // intcount1=0;
        if (!this.state.modalVisible && !this.state.modalVisible2)
        {
          this.queryCheck(myuidd);        
          let mycurTrip = "0"
          await AsyncStorage.getItem("curTrip",(error,value)=>{
            if(value!=null)  mycurTrip=value ; 
            });
          if (mycurTrip>0) {
            this.queryCheck2(mycurTrip);
          }
        }
        // }
      // }
    }, 1000*5*60);


      // this.interval2=setInterval(async () => {
      //   if (!this.state.modalVisible && !this.state.modalVisible2)
      //   {
      //     this._settripid(myuidd);
      //     let mycurTrip = "0"
      //     await AsyncStorage.getItem("curTrip",(error,value)=>{
      //       if(value!=null)  mycurTrip=value ; 
      //       });

      //     if (mycurTrip>0) {
      //       this.ambilTitik(mycurTrip,"TAB1 INT 300000");
      //     }
      //   }
      //  }, 1000*5*60);
       

    //  await BackgroundTimer.stopBackgroundTimer();
    //  BackgroundTimer.runBackgroundTimer(async()=>{
    //   this._backpos();
    //  },30000);


    }
   

   onPressTanya(UrutPhotox){
    Alert.alert(
      'Mostrans',
      'Simpan CHeckList dan Lanjutkan dengan photo kendaraan?',
      [
        {
          text: 'Batal',
          onPress: () => this.setState({ selesaiCheck : false }),
          style: 'cancel',
        },
        {text: 'Ya', onPress: () => {
          this.setState({ selesaiCheck : true });
          this.onPress(UrutPhotox);
                }
                },
      ],
      {cancelable: false},
    );
   }


    onPress(UrutPhotox) {
      UrutPhoto = UrutPhotox;
      var that = this;
      if (Platform.OS === 'android') {
        async function requestCameraPermission() {
          try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.CAMERA,
              {
                title: 'CameraExample App Camera Permission',
                message: 'CameraExample App needs access to your camera ',
              }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              //If CAMERA Permission is granted
              //Calling the WRITE_EXTERNAL_STORAGE permission function
              requestExternalWritePermission();
            } else {
              alert('CAMERA permission denied');
            }
          } catch (err) {
            alert('Camera permission err', err);
            console.warn(err);
          }
        }
        async function requestExternalWritePermission() {
          try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
              {
                title: 'CameraExample App External Storage Write Permission',
                message:
                  'CameraExample App needs access to Storage data in your SD Card ',
              }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              //If WRITE_EXTERNAL_STORAGE Permission is granted
              //Calling the READ_EXTERNAL_STORAGE permission function
              requestExternalReadPermission();
            } else {
              alert('WRITE_EXTERNAL_STORAGE permission denied');
            }
          } catch (err) {
            alert('Write permission err', err);
            console.warn(err);
          }
        }
        async function requestExternalReadPermission() {
          try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
              {
                title: 'CameraExample App Read Storage Read Permission',
                message: 'CameraExample App needs access to your SD Card ',
              }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              //If READ_EXTERNAL_STORAGE Permission is granted
              //changing the state to re-render and open the camera
              //in place of activity indicator
              that.setState({ isPermitted: true });
              // this.setState({ isPermitted: true });
            } else {
              alert('READ_EXTERNAL_STORAGE permission denied');
            }
          } catch (err) {
            alert('Read permission err', err);
            console.warn(err);
          }
        }
        //Calling the camera permission function
        requestCameraPermission();
       // alert('1');
        } else {
        this.setState({ isPermitted: true });
        //alert('22');
      }
    }
    async onBottomButtonPressed(event) {
      
      // alert('event.type '+event.type);
    
      const captureImages = JSON.stringify(event.captureImages);
      if (event.type === 'left') {
        this.setState({ isPermitted: false });
      } else if (event.type === 'capture') {
    
        let mycaptureimages;
      await kompressimage(event.captureImages[0].uri).then(result=>{
          mycaptureimages=result
      });

        if (UrutPhoto=="101") {
        this.setState({ urifoto1: mycaptureimages, isPermitted: false}); }
        else if (UrutPhoto=="102") {
          this.setState({ urifoto2: mycaptureimages, isPermitted: false}); }
          else if (UrutPhoto=="103") {
            this.setState({ urifoto3: mycaptureimages, isPermitted: false}); }
            else if (UrutPhoto=="104") {
              this.setState({ urifoto4: mycaptureimages, isPermitted: false}); }
              else if (UrutPhoto=="105") {
                this.setState({ urifoto5: mycaptureimages, isPermitted: false}); }
                      
      }
    }
    





   render(){

    if (this.state.isPermitted) {
      // if (false) {
        return (
   
          <StyleProvider style={getTheme(platform)}>
         <View>
         <Modal
            animationType="fade"
           transparent={false}
           alignItems="center"
           >
   
         <CameraKitCameraScreen
           // Buttons to perform action done and cancel
           actions={{ rightButtonText: '', leftButtonText: 'Tutup' }}
           onBottomButtonPressed={event => this.onBottomButtonPressed(event)}
           flashImages={{
             // Flash button images
             on: require('../../../assets/images/flashon.png'),
             off: require('../../../assets/images/flashoff.png'),
             auto: require('../../../assets/images/flashauto.png'),
           }}
           cameraFlipImage={require('../../../assets/images/flip.png')}
           captureButtonImage={require('../../../assets/images/capture.png')}
         />
         </Modal>
         </View>
         </StyleProvider>
       );
     } else {   
      return (  
        <StyleProvider style={getTheme(platform)}>
      <View style={{height: '100%'}}>
         <ListView
         enableEmptySections='true'
         dataSource={this.state.dataSource}
         renderRow={(rowData) => 

         <View style = {styles.container}>

         <View style={styles.item1}>

{ rowData.selisih_tgl < 0 ? 
         <Text style={{ fontSize: 25 /*30*/,color: 'black' }}>
         H{rowData.selisih_tgl}
         </Text> 
  : rowData.selisih_tgl > 0 ? 
  <Text style={{ fontSize: 25 /*30*/,color: 'black'  }}>
  H+{rowData.selisih_tgl}
  </Text>
  : 
  <Text style={{ fontSize: 20 /*24*/,color: 'black'  }}>
  Hari ini
  </Text>
}        


{  (rowData.statustab=='1') ?       
<Image source={require('../../../assets/images/calendar.png')}  style={styles2.ImageStyle02} />
: 
<Image source={require('../../../assets/images/calendarblue.png')}  style={styles2.ImageStyle02} />
}

         </View>

         
         <View style={styles.item2}>
         <TouchableOpacity style={styles.button} onPress={()=>this._trip(true,rowData.id)}>
         <View>
         {/* <Text>{rowData.id}</Text> */}
         <Text style={{ color: 'black' }}>{rowData.id_trip}</Text>
         <Text>{rowData.no_container}</Text>
         <Text>{rowData.format_tgl} {rowData.jam_pengiriman}</Text>
         <Text>{rowData.asal}</Text>
         <Text>{rowData.tujuan}</Text>
         {  (rowData.statustab=="0") ?
         <Text style={{ color: 'blue' }}>{rowData.statustabtxt}</Text> : <Text style={{ color: 'red' }}>{rowData.statustabtxt}</Text>
         }
         </View>
         </TouchableOpacity>
         </View>


{/* dimulai dari sini 11-03-2020 */}

<View>
         <Modal
            animationType="fade"
           transparent={false}
           alignItems="center"
           visible={(false)}
           >
   
         <CameraKitCameraScreen
           // Buttons to perform action done and cancel
           actions={{ rightButtonText: '', leftButtonText: 'Tutup' }}
           onBottomButtonPressed={event => this.onBottomButtonPressed(event)}
           flashImages={{
             // Flash button images
             on: require('../../../assets/images/flashon.png'),
             off: require('../../../assets/images/flashoff.png'),
             auto: require('../../../assets/images/flashauto.png'),
           }}
           cameraFlipImage={require('../../../assets/images/flip.png')}
           captureButtonImage={require('../../../assets/images/capture.png')}
         />
         </Modal>
         </View>

{/* berakhir di sini 11-03-2020 */}

       <View>
        <Modal
           animationType="fade"
          transparent={true}
         //  backgroundColor='rgba(0,0,255,0.8)'
         //  justifyContent= "center"
          alignItems="center"
          visible={(this.state.modalVisible&&this.state.keyid==rowData.id&&'0'==rowData.statustab&&!this.state.isPermitted)}
         //  onRequestClose={() => {
            // Alert.alert('Modal has been closed.');
         //  }}
          >
          <View style={{ backgroundColor: 'rgba(0,0,0,0.6)', height: '100%'  }}>
            <ScrollView>
          <View style={styles.boxShad}>
            <View>
              {/* <Text>Hello World!</Text> */}

              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Nomor Trip</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.id_trip}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Tanggal</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.format_tgl}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Jam</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.jam_pengiriman}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Asal</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.asal}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Tujuan</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.tujuan}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>Mobil</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.jenis_kendaraan}</Text>
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item3}>
                      <Text>No Pol</Text>
                  </View>
                  <View style={styles.item4}>
                      <Text>{rowData.plat_nomor}</Text>
                  </View>
              </View>


         <Tabs renderTabBar={()=> <ScrollableTab style={{ backgroundColor: "#0b8d45"  }} />}> 
         <Tab 
         heading={ <TabHeading /*style={{backgroundColor:  "white"}}*/
          >
                        <Text style={{ color: 'white' }}>Check List</Text>
            </TabHeading>}
          >


{ (  this.setState.selesaiCheck ) ?
  <View>
                  <Text>Kelengkapan Surat</Text>
                  { (this.state.SIMCheck) ?
                  <Text>[V] Identitas Lengkap dan Berlaku (SIM, STNK, KIR)</Text>
                  :
                  <Text>[ ] Identitas Lengkap dan Berlaku (SIM, STNK, KIR)</Text>
                  } 
                  <Text> </Text>
                  <Text>Kondisi Box Mobil</Text>
                  { (this.state.SampahCheck) ?
                  <Text>[V] Bebas Sampah dan Kotoran</Text>
                  :
                  <Text>[ ] Bebas Sampah dan Kotoran</Text>
                  } 
                  { (this.state.GelombangCheck) ?
                  <Text>[V] Tidak Bergelombang (Rata)</Text>
                  :
                  <Text>[ ] Tidak Bergelombang (Rata)</Text>
                  } 
                  { (this.state.TajamCheck) ?
                  <Text>[V] Tidak Ada Bagian Tajam</Text>
                  :
                  <Text>[ ] Tidak Ada Bagian Tajam</Text>
                  } 
                  { (this.state.LubangCheck) ?
                  <Text>[V] Tidak Berlubang</Text>
                  :
                  <Text>[ ] Tidak Berlubang</Text>
                  } 
                  { (this.state.KeringCheck) ?
                  <Text>[V] Kering</Text>
                  :
                  <Text>[ ] Kering</Text>
                  } 
                  { (this.state.SeranggaCheck) ?
                  <Text>[V] Bebas Serangga</Text>
                  :
                  <Text>[ ] Bebas Serangga</Text>
                  } 
                  { (this.state.BauCheck) ?
                  <Text>[V] Tidak Berbau</Text>
                  :
                  <Text>[ ] Tidak Berbau</Text>
                  } 
                  { (this.state.OliCheck) ?
                  <Text>[V] Tidak Ada Tetesan Oli</Text>
                  :
                  <Text>[ ] Tidak Ada Tetesan Oli</Text>
                  } 
                  { (this.state.TerpalCheck) ?
                  <Text>[V] Tersedia Alas Terpal</Text>
                  :
                  <Text>[ ] Tersedia Alas Terpal</Text>
                  } 
</View>
:
<View>
                  <Text>Kelengkapan Surat</Text>
                  <CheckBox 
                        // center
                        title='Identitas Lengkap dan Berlaku (SIM, STNK, KIR)'
                        checked={this.state.SIMCheck}
                      onPress={() => this.setState({SIMCheck: !this.state.SIMCheck})}
                      disabled={this.state.selesaiCheck}
                      size={18}
                      containerStyle = {{ height: 35, padding: 1 }}
                  />
                  <Text>Kondisi Box Mobil</Text>
                  <CheckBox 
                        // center
                        title='Bebas Sampah dan Kotoran'
                        checked={this.state.SampahCheck}
                      onPress={() => this.setState({SampahCheck: !this.state.SampahCheck})}
                      disabled={this.state.selesaiCheck}
                      size={18}
                      containerStyle = {{ height: 15  }}
                />
                  <CheckBox 
                        // center
                        title='Tidak Bergelombang (Rata)'
                        checked={this.state.GelombangCheck}
                      onPress={() => this.setState({GelombangCheck: !this.state.GelombangCheck})}
                      disabled={this.state.selesaiCheck}
                      size={18}
                      containerStyle = {{ height: 15  }}
                />
                  <CheckBox 
                        // center
                        title='Tidak Ada Bagian Tajam'
                        checked={this.state.TajamCheck}
                      onPress={() => this.setState({TajamCheck: !this.state.TajamCheck})}
                      disabled={this.state.selesaiCheck}
                      size={18}
                      containerStyle = {{ height: 15  }}
                  />
                  <CheckBox 
                        // center
                        title='Tidak Berlubang'
                        checked={this.state.LubangCheck}
                      onPress={() => this.setState({LubangCheck: !this.state.LubangCheck})}
                      disabled={this.state.selesaiCheck}
                      size={18}
                      containerStyle = {{ height: 15  }}
                  />
                  <CheckBox 
                        // center
                        title='Kering'
                        checked={this.state.KeringCheck}
                      onPress={() => this.setState({KeringCheck: !this.state.KeringCheck})}
                      disabled={this.state.selesaiCheck}
                      size={18}
                      containerStyle = {{ height: 15  }}
                  />
                  <CheckBox 
                        // center
                        title='Bebas Serangga'
                        checked={this.state.SeranggaCheck}
                      onPress={() => this.setState({SeranggaCheck: !this.state.SeranggaCheck})}
                      disabled={this.state.selesaiCheck}
                      size={18}
                      containerStyle = {{ height: 15  }}
                  />
                  <CheckBox 
                        // center
                        title='Tidak Berbau'
                        checked={this.state.BauCheck}
                      onPress={() => this.setState({BauCheck: !this.state.BauCheck})}
                      disabled={this.state.selesaiCheck}
                      size={18}
                      containerStyle = {{ height: 15  }}
                  />
                  <CheckBox 
                        // center
                        title='Tidak Ada Tetesan Oli'
                        checked={this.state.OliCheck}
                      onPress={() => this.setState({OliCheck: !this.state.OliCheck})}
                      disabled={this.state.selesaiCheck}
                      size={18}
                      containerStyle = {{ height: 15  }}
                  />
                  <CheckBox 
                        // center
                        title='Tersedia Alas Terpal'
                        checked={this.state.TerpalCheck}
                      onPress={() => this.setState({TerpalCheck: !this.state.TerpalCheck})}
                      disabled={this.state.selesaiCheck}
                      size={18}
                      containerStyle = {{ height: 15  }}
                  />
                  </View>
}
                  {/* </Tab> */}
                  
                  {/* <Tab 
         heading={ <TabHeading
          >
                        <Text style={{ color: 'white' }}>Foto Kendaraan</Text>
            </TabHeading>}
          > */}


{/* ini dihilangkan dulu - start */}
{/* <View>
<TouchableOpacity 
                    onPress={() => {this.onPressTanya('101')} }>
<Image source={require('../../../assets/images/camera.png')} style={{ width: 40, height: 40 }} />
<Text style={{ fontSize: 12}}>Foto Kondisi Kendaraan</Text>
                   </TouchableOpacity>
</View>
<View>
{ this.state.urifoto1 !== '' ? 
<View>
<Image style={{width: 300, height: 300}} source={{uri: 'file://' + this.state.urifoto1}}/> 
</View>
: false}  
</View> */}
{/* ini dihilangkan dulu - end */}


          </Tab>
          <Tab heading={ <TabHeading  /*style={{backgroundColor: "white" }}*/
          >
                        <Text  style={{ color: 'white' }}
                        >Order List</Text>
            </TabHeading>}
          >

<View style={{ flexDirection: 'row', maxHeight: '100%' }}>
        <ListView         
         enableEmptySections='true' 
         dataSource={this.state.dataSource2}
         renderRow={(rowData2) => 
          <View>
             <Text>Order# : {rowData2.id_order}
             </Text>
             <Text>Pengambilan : {rowData2.pengambilan}
             </Text>
             <Text>{rowData2.dtlpengambilan}
             </Text>
             <Text>Pengiriman : {rowData2.pengiriman}
             </Text>
             <Text>{rowData2.dtlpengiriman}
             </Text>
             <Text></Text>
          </View>
         }         
        /> 
   </View>

          </Tab>
   </Tabs> 


   {/* <Text></Text> */}


   {/* <View style={{ flexDirection: 'row', maxHeight: '40%' }}>
        <ListView         
         enableEmptySections='true' 
         dataSource={this.state.dataSource2}
         renderRow={(rowData2) => 
          <View>
             <Text>Order# : {rowData2.id_order}
             </Text>
             <Text>Pengambilan : {rowData2.pengambilan}
             </Text>
             <Text>{rowData2.dtlpengambilan}
             </Text>
             <Text>Pengiriman : {rowData2.pengiriman}
             </Text>
             <Text>{rowData2.dtlpengiriman}
             </Text>
             <Text></Text>
          </View>
         }         
        /> 
   </View> */}




              <View style = {styles.container1}>
                  <View style={styles.item5}>
              <TouchableHighlight style={styles2.button}
                onPress={() => {
                  this._tripproses(!this.state.modalVisible,rowData.id,rowData.id_trip);
                }}>
                <Text style={{ color: 'white' }}>Proses</Text>
              </TouchableHighlight>
              </View>
                  <View style={styles.item6}>
              <TouchableHighlight style={styles2.button}
                onPress={() => {
                  this._trip(!this.state.modalVisible,0);
                }}>
                <Text style={{ color: 'white' }}>Tutup</Text>
              </TouchableHighlight>
              </View>
              </View>

            </View>

          </View>
          </ScrollView>
          </View>
        </Modal>
      </View>



      <View>
        <Modal
           animationType="fade"
          transparent={true}
          alignItems="center"
          visible={(this.state.modalVisible2)}
          >
          <View style={{ backgroundColor: 'rgba(0,0,0,0.1)', height: '100%'  }}>
            <ScrollView>
          <View style={styles.boxShad2}>
            <View>
              <View style = {styles.container1}>
                  <View>
                  <Text style={{ fontSize: 20 }}>CHECK LIST</Text>
                  <Text>Kelengkapan Surat</Text>
                  <CheckBox 
                        // center
                        title='Identitas Lengkap dan Berlaku (SIM, STNK, KIR)'
                        checked={this.state.SIMCheck}
                      onPress={() => this.setState({SIMCheck: !this.state.SIMCheck})}
                      size={18}
                      containerStyle = {{ height: 35, padding: 1 }}
                  />
                  <Text>Kondisi Box Mobil</Text>
                  <CheckBox 
                        // center
                        title='Bebas Sampah dan Kotoran'
                        checked={this.state.SampahCheck}
                      onPress={() => this.setState({SampahCheck: !this.state.SampahCheck})}
                      size={18}
                      containerStyle = {{ height: 15  }}
                />
                  <CheckBox 
                        // center
                        title='Tidak Bergelombang (Rata)'
                        checked={this.state.GelombangCheck}
                      onPress={() => this.setState({GelombangCheck: !this.state.GelombangCheck})}
                      size={18}
                      containerStyle = {{ height: 15  }}
                />
                  <CheckBox 
                        // center
                        title='Tidak Ada Bagian Tajam'
                        checked={this.state.TajamCheck}
                      onPress={() => this.setState({TajamCheck: !this.state.TajamCheck})}
                      size={18}
                      containerStyle = {{ height: 15  }}
                  />
                  <CheckBox 
                        // center
                        title='Tidak Berlubang'
                        checked={this.state.LubangCheck}
                      onPress={() => this.setState({LubangCheck: !this.state.LubangCheck})}
                      size={18}
                      containerStyle = {{ height: 15  }}
                  />
                  <CheckBox 
                        // center
                        title='Kering'
                        checked={this.state.KeringCheck}
                      onPress={() => this.setState({KeringCheck: !this.state.KeringCheck})}
                      size={18}
                      containerStyle = {{ height: 15  }}
                  />
                  <CheckBox 
                        // center
                        title='Bebas Serangga'
                        checked={this.state.SeranggaCheck}
                      onPress={() => this.setState({SeranggaCheck: !this.state.SeranggaCheck})}
                      size={18}
                      containerStyle = {{ height: 15  }}
                  />
                  <CheckBox 
                        // center
                        title='Tidak Berbau'
                        checked={this.state.BauCheck}
                      onPress={() => this.setState({BauCheck: !this.state.BauCheck})}
                      size={18}
                      containerStyle = {{ height: 15  }}
                  />
                  <CheckBox 
                        // center
                        title='Tidak Ada Tetesan Oli'
                        checked={this.state.OliCheck}
                      onPress={() => this.setState({OliCheck: !this.state.OliCheck})}
                      size={18}
                      containerStyle = {{ height: 15  }}
                  />
                  <CheckBox 
                        // center
                        title='Tersedia Alas Terpal'
                        checked={this.state.TerpalCheck}
                      onPress={() => this.setState({TerpalCheck: !this.state.TerpalCheck})}
                      size={18}
                      containerStyle = {{ height: 15  }}
                  />
                  </View>
              </View>
              <View style = {styles.container1}>
                  <View style={styles.item6}>
              <TouchableHighlight style={styles2.button}
                onPress={() => {
                  this._checklist(0);
                }}>
                <Text style={{ color: 'white' }}>Simpan</Text>
              </TouchableHighlight>
              </View>
              </View>
            </View>
          </View>
          </ScrollView>
          </View>
        </Modal>
      </View>




         </View>

}
/>
{ this.state.isLoading ? 
  // <View style={styles.spinnercontainer}>
  //   <Spinner/>
  //   <Text style={styles.spinnertext}>Loading...</Text>
  // </View>
  false
  : 
    this.state.dataSource.getRowCount() === 0 ? <View style={styles2.nodata}><Text>Tidak ada Data</Text></View> : false

}
  
  </View>
  </StyleProvider>   
); }

      }
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'flex-start', // if you want to fill rows left to right
      marginTop: 2,
      marginBottom: 2
    },
    container1: {
      // flex: 1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'flex-start', // if you want to fill rows left to right
      // marginTop: 2,
      // marginBottom: 2
    },
    item1: {
      width: '30%', // is 50% of container width
      //flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      paddingTop: 10,
      color: 'black'
    },
    item2: {
      width: '70%', // is 50% of container width
      backgroundColor: "#eff9f1" /*'#eff9f1'*/,
      paddingTop: 5,
      paddingBottom: 5,
      paddingLeft: 10
    },
    item3: {
      width: '35%', // is 50% of container width
      //flex: 1,
      // justifyContent: 'center',
      alignItems: 'flex-start',
      // paddingTop: 10
    },
    item4: {
      width: '65%', // is 50% of container width
      backgroundColor: '#eff9f1',
      alignItems: 'flex-start',
      // paddingTop: 5,
      // paddingBottom: 5,
      // paddingLeft: 10
    },
    itemCHK3: {
      width: '15%', // is 50% of container width
      paddingTop: 3,
      paddingBottom: 3,
      //flex: 1,
      // justifyContent: 'center',
      alignItems: 'flex-start',
      // paddingTop: 10
    },
    itemCHK4: {
      width: '85%', // is 50% of container width
      paddingTop: 3,
      paddingBottom: 3,
      backgroundColor: '#eff9f1',
      alignItems: 'flex-start',
      // paddingTop: 5,
      // paddingBottom: 5,
      // paddingLeft: 10
    },
    item5: {
      width: '50%', // is 50% of container width
      //flex: 1,
      // justifyContent: 'center',
      alignItems: 'flex-end',
      padding: 10,
      paddingTop: 20
    },
    item6: {
      width: '50%', // is 50% of container width
      //flex: 1,
      // justifyContent: 'center',
      alignItems: 'flex-start',
      padding: 10,
      paddingTop: 20
    },
    boxShad: {
      flexDirection: 'row',
      marginTop: 0,
      // marginBottom: 50,
      // width: '80%',
      borderWidth: 1,
      borderRadius: 8,
      // borderColor: '#ddd',
      // borderBottomWidth: 0,
      alignItems: 'center',
      // flexDirection: 'row',
      shadowColor: '#000',
      shadowOffset: { width: 5, height: 5 },
      // shadowOpacity: 0.2,
      shadowRadius: 5,
      elevation: 1,
      padding: 10,
      backgroundColor: 'white'
    },
    boxShad2: {
      flexDirection: 'row',
      marginTop: 20,
      // marginBottom: 50,
      // width: '80%',
      borderWidth: 1,
      borderRadius: 8,
      // borderColor: '#ddd',
      // borderBottomWidth: 0,
      alignItems: 'center',
      // flexDirection: 'row',
      shadowColor: '#000',
      shadowOffset: { width: 5, height: 5 },
      // shadowOpacity: 0.2,
      shadowRadius: 5,
      elevation: 1,
      padding: 10,
      backgroundColor: 'white'
    },
    spinnercontainer:{
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      backgroundColor: '#00000033',
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    }
})
