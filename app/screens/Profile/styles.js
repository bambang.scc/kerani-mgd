import { StyleSheet } from 'react-native';
import { withTheme } from 'react-native-elements';

const styles = StyleSheet.create({
  container : {
    backgroundColor:'#f3f3f3',
    flex: 1,
    alignItems:'center',
    justifyContent :'center'
  },
  garisHeader: {
    width: 50,
    height: '80%',
    backgroundColor: 'red',
    transform: [
      {rotate: '45deg'}
    ]
  },
    loginForm : {
    flexGrow: 4,
    justifyContent:'center',
    alignItems: 'center'
  },  inputBox: {
    //width:300,
    flex: 1,
    backgroundColor:'rgba(255, 255,255,0.2)',
    borderRadius: 12,
    //paddingHorizontal:16,
    fontSize:16,
    color:'black',
    marginVertical: 10
    //    alignSelf: 'center'
  },
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  MarginLeftView: {
    paddingLeft: 25
  },
  CenterView: {
    alignItems: 'center'
  },
  LeftView: {
    alignItems: 'flex-start'
  },
    button: {
    width:300,
    backgroundColor:'#86938c',
     borderRadius: 10,
      marginVertical: 10,
      paddingVertical: 13
  },
  item1: {
    width: '30%', // is 50% of container width
    //flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingTop: 5,
    paddingBottom: 5,
    paddingTop: 10
  },
  item2: {
    width: '70%', // is 50% of container width
    // backgroundColor: '#f0f3f1',
    color: '#fff',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10
  },
  item3: {
    width: '50%', // is 50% of container width
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10
    // paddingBottom: 10
  },
  item33: {
    width: '25%', // is 50% of container width
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10
    // paddingBottom: 10
  },
  item4: {
    width: 80, // is 50% of container width
    height: 75,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    shadowColor: 'black',
    shadowOffset: { width: 8, height: 8 },
    shadowOpacity: 0.2,
    shadowRadius: 8,
    elevation: 1,    
  },  
  ImageStyle01: {
    alignItems: 'center',
    height: 22,
    width: 35
  },
  ImageStyle01A: {
    alignItems: 'center',
    height: 21,
    width: 21
  }  
});

export default styles;




