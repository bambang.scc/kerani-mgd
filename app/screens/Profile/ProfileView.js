import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Icon,
  Footer,
  FooterTab,
  Left,
  Right,
  Body,
  Badge,
  Grid,
  Col,
  Card,
  CardItem, Button, StyleProvider
} from "native-base";
import { Image, TouchableOpacity, TextInput, YellowBox, View, Linking, Alert, ImageBackground } from 'react-native';
import styles from './styles';
import AsyncStorage from '@react-native-community/async-storage'; //untuk simpan local variable
import {encrypt, decrypt, getUrl} from '../../../assets/lib/brosky';
import Axios from 'axios';

import getTheme from '../../../native-base-theme/components';
import platform from '../../../native-base-theme/variables/platform';


{/* import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements' */}

YellowBox.ignoreWarnings(['Remote debugger']);

let url;

class ProfileView extends Component {

  constructor(props) {
    super(props);
    this.state={
      nama: '',
      hp: '',
      password: '',
      mypoint: 0,
      foto_driver: ''
    }
   }


  _logout(){
    AsyncStorage.clear((error)=>{
      this.props.navigation.replace('Splash');
    });
  }



  async _darurat(){

    // alert('1');

    url = await getUrl();

    // alert('2');

    await AsyncStorage.getItem("uid",(error,value)=>{
       if(value!=null)  myuid=value ; 
       else { this.props.navigation.replace("Login"); }});

      //  alert('3');

     let myuidd = "";
    await decrypt(myuid).then(value=>{
       myuidd=value
    });
    console.log("myuid ==== "+myuidd);

    // alert('4');

    let myquery = "";
    await encrypt("insert into mt_master_emergency (driver_id,trip_id,status,keterangan,create_date,update_date) values ("+myuidd+",0,'A','Emergency',now(),now())" ).then((result)=>{
    myquery = result;
    });
    Axios.post(url.select,{
    query: myquery
    })
    .then((response)=>{      
      let temp = response.data;
     console.log(response.data);

    //  alert('5');

     Alert.alert(
      'Info',
      'Informasi darurat Anda sudah disampaikan ke sistem',
      [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );

    })
    .catch(error=>{

      // alert('6');

     console.log("error+10++++++++++++++++++++++++++++++"+error);
    });
    
  };





  async componentDidMount(){

    url = await getUrl();

    await AsyncStorage.getItem("uid",(error,value)=>{
       if(value!=null)  myuid=value ; 
       else { this.props.navigation.replace("Login"); }});
    let myuidd = "";
    await decrypt(myuid).then(value=>{
       myuidd=value
    });
    console.log("myuid ==== "+myuidd);
    let myquery = "";
    await encrypt("SELECT a.nama,a.hp,a.email,b.foto_driver FROM mt_master_user a, mt_driver_info b where a.id=b.user_id and a.id="+myuidd ).then((result)=>{
       myquery = result;
    }
       );

    Axios.post(url.select,{
       query: myquery
     })
     .then((response)=>{
       let temp = response.data;
       this.setState({ nama: temp.data[0].nama, hp: temp.data[0].hp, email: temp.data[0].email, foto_driver: temp.data[0].foto_driver});
       console.log(temp.data[0]);
     })
     .catch(error=>{
        console.log("error+11++++++++++++++++++++++++++++++"+error);
     });
    

     await encrypt("select poin-poin_redeem totpoin from mt_driver_poin where driver_id="+myuidd+" and tahun=extract(year from (NOW() - interval '31' day))::TEXT" ).then((result)=>{
      myquery = result;
   }
   );

   Axios.post(url.select,{
      query: myquery
    })
    .then((response)=>{
      let temp = response.data;
      this.setState({ mypoint: temp.data[0].totpoin});
      console.log(temp.data[0]);
    })
    .catch(error=>{
       console.log("error+13++++++++++++++++++++++++++++++"+error);
    });
   }






   render() {
    return (
      <StyleProvider style={getTheme(platform)}>
      <Container /*style={{ backgroundColor: "#ffffff" }}*/>
        <Header /*style={{ backgroundColor: "#008C45" }}*/>
        <Body>
            <View>
              {/* <Text style={{ color: 'white' }}>MOSTRANS Driver</Text> */}
              <Text style={{ color: 'white' }}>
              <Image source={require('../../../assets/images/logokecil.png')}  style={styles.ImageStyle01} />
                 &nbsp; MOSTRANS</Text>              
            </View>


            {/* <Title>
              MOSTRANS Driver</Title> */}

          </Body>

          <Right>

{  ( false ) ?
          <View>
              <Text style={{ color: 'white' }}>
              <Image source={require('../../../assets/images/coin.png')}  style={styles.ImageStyle01A} />
              &nbsp; {this.state.mypoint} Points</Text>
            </View>              
  : null
}            

          </Right>
        </Header>

        <Content style={{ backgroundColor: "#ffffff" }} > 

        {/* <Image source={require('../../../assets/images/backdasar.png')} style={{ flex:1, width: null, height: null}}> */}

        <ImageBackground source={require('../../../assets/images/backdasar.png')} style={{ flex: 1 }} >

{/* <View><Text> </Text></View> */}
<View style={{  marginLeft: 16, marginRight:16 }} >


<View style={{ flexDirection: 'row', flex: 1, paddingBottom: 5, justifyContent: 'center' }}>

{ this.state.foto_driver != '' ? 
<Image source={{ uri: this.state.foto_driver}}  style={{width: 125, height: 125, borderRadius: 125/ 2}}
 />
 : null
}


</View>

<View style={{ flexDirection: 'row', flex: 1, paddingTop: 0 }}>
<View style={styles.item1}>
<Text style={{ color: '#000' }}> </Text>
</View>
<View style={styles.item2}>
<Text style={{ color: '#fff', fontSize: 20, fontWeight: 'bold' }}>{this.state.nama}</Text>
<Text style={{ color: '#fff', fontSize: 14 }}>{this.state.email}</Text>                        
<Text style={{ color: '#fff', fontSize: 14 }}>{this.state.hp}</Text>                        
</View>
</View>

{/* <View  style={{ flexDirection: 'row', flex: 1, paddingBottom: 0  }}>
<View style={styles.item1}>
<Text style={{ color: '#000' }}> </Text>
</View>
<View style={styles.item2}>
<Text style={{ color: '#fff', fontSize: 18, fontWeight: 'bold'  }}>{this.state.email}</Text>                        
</View>
</View> */}

{/* <View  style={{ flexDirection: 'row', flex: 1, paddingBottom: 0  }}>
<View style={styles.item1}>
<Text style={{ color: '#fff' }}> </Text>
</View>
<View style={styles.item2}>
<Text style={{ color: '#fff', fontSize: 18, fontWeight: 'bold'  }}>{this.state.hp}</Text>                        
</View>
</View> */}


{/* <View>
<Button  block rounded iconLeft light style={{ marginTop: 8, backgroundColor: '#F6AE2D' }}>
    <Icon name='key'  type='FontAwesome' />
    <Text>Ubah Password</Text>
</Button>
</View> */}
<View>
<Button  block 
// rounded 
iconLeft light style={{ marginTop: 8, marginStart: '50%', backgroundColor: '#fff'}}
  onPress={this._logout.bind(this)}>
    <Icon name='sign-out-alt'  type='FontAwesome5' style={{ color: "#000"}} />
    <Text style={{ color: "#000"}}>Keluar</Text>
</Button>
</View>
</View>


<View  style={{ flexDirection: 'row', flex: 1, paddingBottom: 5, paddingTop: 30  }}> 
<View style={styles.item33}>
<TouchableOpacity 
         onPress={() => Linking.openURL('google.navigation:q=nearby hospital')}>
<View style={styles.item4}>
<Image source={require('../../../assets/images/hospital.png')} style={{ width: 25, height: 25}} />
<Text style={{ color:'black', fontSize: 12 }}>Rumah Sakit</Text>
</View>
</TouchableOpacity>
</View>
<View style={styles.item33}>
<TouchableOpacity 
         onPress={() => Linking.openURL('google.navigation:q=nearby kantor polisi')}>
<View style={styles.item4}>
         <Image source={require('../../../assets/images/policeman.png')} style={{ width: 25, height: 25}} />
<Text style={{ color:'black', fontSize: 12 }}>Pos Polisi</Text>
</View>
</TouchableOpacity>
</View>
{/* </View> */}
{/* <View  style={{ flexDirection: 'row', flex: 1, paddingBottom: 5, paddingTop: 30  }}>  */}
<View style={styles.item33}>
<TouchableOpacity 
         onPress={() => Linking.openURL('google.navigation:q=nearby SPBU')}>
<View style={styles.item4}>
<Image source={require('../../../assets/images/gasoline.png')} style={{ width: 25, height: 25}} />
<Text style={{ color:'black', fontSize: 12 }}>SPBU</Text>
</View>
</TouchableOpacity>
</View>
<View style={styles.item33}>
<TouchableOpacity 
         onPress={() => this._darurat()}>
<View style={styles.item4}>
<Image source={require('../../../assets/images/alarm.png')} style={{ width: 25, height: 25}} />
<Text style={{ color:'black', fontSize: 12 }}>Status</Text>
<Text style={{ color:'black', fontSize: 12 }}>Darurat</Text>
</View>
</TouchableOpacity>
</View>
</View>


       </ImageBackground>

       {/* </Image> */}
         </Content>

      </Container>
      </StyleProvider>
    );
  }
}
export default ProfileView;


