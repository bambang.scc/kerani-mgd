import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Footer,
  FooterTab,
  Left,
  Right,
  Body,
  Badge,
  Grid,
  Col,
  Card,
  CardItem, Tabs, Tab, TabHeading, ScrollableTab, List, ListItem, Spinner, StyleProvider
} from "native-base";
import { View, Image, Dimensions, StyleSheet, YellowBox, WebView, TouchableOpacity } from 'react-native';

import styles from './styles';
import {encrypt, decrypt, getUrl, pointdecrypt, pointencrypt, version} from '../../../assets/lib/brosky';
import AsyncStorage from '@react-native-community/async-storage'; //untuk simpan local variable
import Axios from 'axios';
import { PermissionsAndroid } from 'react-native';

import AesCrypto from 'react-native-aes-kit';
import base64 from 'react-native-base64';

import getTheme from '../../../native-base-theme/components';
import platform from '../../../native-base-theme/variables/platform';


let url;
let myparamstr = "";
let myencparamstr = "";
let myalamat = "";
let myalamat2="";
const secretKey = 'mostrans12345678';
const iv = 'mostrans12345678';

YellowBox.ignoreWarnings(['WebView'])

class ChatView extends Component {
  constructor(props) {
    super(props);
    this.state={
      isLoading: false,
      mypoint: 0,
      myshowredeem: 1,
      // myalamat: '',
      // myalamat2: '',
      latitude: '',
      longitude: '',
      myuidd: ''
    }
  }



  async componentDidMount(){
    let {width, height} = Dimensions.get('window');
      console.log('load');
      console.log(width, height);

      url = await getUrl();

     await AsyncStorage.getItem("uid",(error,value)=>{
        if(value!=null)  myuid=value ; 
        else { this.props.navigation.replace("Login"); }});
     let myuidd = "";
     await decrypt(myuid).then(value=>{
        myuidd=value
     });
 
     this.setState({ myuidd: myuidd});

    //  myalamat = 'http://mostrans.co.id/ChatDriver/?param=dGRSazdiOVU0NkVDRVQ1cWpUMlV6dz09';

     await AesCrypto.encrypt(myuidd,secretKey,iv).then(cipher=>{
      console.log('HasilAES:'+cipher);// return a string type cipher
      myalamat = 'http://mostrans.co.id/ChatDriver/?param=' + base64.encode(cipher);
      // Alert.alert(myalamat);
      this.setState({ HasilAES : myalamat});
      // console.log(this.state.HasilAES);
      // HasilBase64 = cipher;
     }).catch(err=>{
      console.log(err);
     });


    // try {
    //   const granted = await PermissionsAndroid.request(
    //     PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    //     {
    //       'title': 'Mostrans',
    //       'message': 'Allow app to access your location '
    //     }
    //   )
    //   if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    //     navigator.geolocation.getCurrentPosition(
    //       async (position) => {
    //           console.log(position);
    //          this.setState({
    //            latitude: position.coords.latitude,
    //            longitude: position.coords.longitude
    //          });
    //     },
    //       (error) => {
    //           console.log("Error===============");
    //           console.log(error.code, error.message);
    //       },
    //       { enableHighAccuracy: false, timeout: 360000, maximumAge: 10000 }
    //   );
    //   } else {
    //     alert("Location permission denied");
    //   }
    // } catch (err) {
    //   console.log("No Access  to location" + err);
    // }




//     await encrypt("select totpoin from mt_driver_poin_v where tahun::text=tahunpembanding::text and driver_id="+myuidd ).then((result)=>{
//         myquery = result;
//      }
//      );
//      Axios.post(url.select,{
//         query: myquery
//       })
//       .then((response)=>{
//         let temp = response.data;
//         this.setState({ mypoint: temp.data[0].totpoin});
//         console.log(temp.data[0]);
//       })
//       .catch(error=>{
//          console.log("error+1++++++++++++++++++++++++++++++"+error);
//       });

//       let myquery = "";

//       await encrypt("select id::TEXT sid,transporter_id::TEXT transporter_id,'' acc, '' imei_hp from mt_master_user where id=" + this.state.myuidd ).then((result)=>{
//          myquery = result;
//       });
//       Axios.post(url.select,{
//          query: myquery
//        })
//        .then((response)=>{
//         let temp = response.data;
//         myparamstr = temp.data[0].sid + "~" + temp.data[0].transporter_id + "~"+"1.4"+"~"+this.state.longitude+"~"+this.state.latitude+"~0~"+temp.data[0].imei_hp;
//         myencparamstr = pointencrypt(myparamstr);

//        if (version.type=='dev') {
//            myalamat = 'https://mostrans.co.id/redeempoint/?q=h9172911333' + myencparamstr;
//            myalamat2 = 'https://mostrans.co.id/redeempoint/history?q=h9172911333' + myencparamstr;          


//        }
//        else
//        {
//         myalamat = 'https://mostrans.co.id/redeempoint/?q=' + myencparamstr;
//         myalamat2 = 'https://mostrans.co.id/redeempoint/history?q=' + myencparamstr;          
//  }
//       })
//       .catch(error=>{
//         console.log('ErrRedeem');
//       });
  

}

  logimei=()=>{
    this.props.navigation.replace('Splash');
  }


  async _switchredeem(){

    
    // if (this.state.myshowredeem==1) {
    //   this.setState({ myshowredeem: 0});
    //  } else
    //  {  
    //   this.setState({ myshowredeem: 1});
    //  };

     this.setState({ myshowredeem: 1});

     // }
  };


  render() {
    return (

      <StyleProvider style={getTheme(platform)}>
      <Container /*style={{ backgroundColor: "#ffffff" }}*/ >
        {/* <Header style={{ backgroundColor: "#008C45" }}> */}
        <Header style={{ /*backgroundColor:  "#008C45" */  }}>
          <Body>
            <View>
              <Text style={{ color: 'white' }}>
              <Image source={require('../../../assets/images/logokecil.png')}  style={styles.ImageStyle01} />
                 &nbsp; MOSTRANS</Text>
            </View>

            {/* <Title>
              MOSTRANS Driver</Title> */}

          </Body>

          {/* <Right>

          <TouchableOpacity style={styles.button} onPress={()=>this._switchredeem()}>
{  (this.state.myshowredeem==0) ?  
  <View>              
<Text> </Text>
</View>  :
  <View>              
  <Text> </Text>
</View>
}
</TouchableOpacity>            

          </Right> */}
        </Header>


{/* 
  <Tabs renderTabBar={()=> <ScrollableTab />}> 

  <Tab 
         heading={ <TabHeading style={{backgroundColor:  "#008C45" }}
          >
            <Image source={require('../../../assets/images/redeemcoin.png')}  style={styles.ImageStyle} />
                <Text>Redeem Point</Text>
            </TabHeading>}
          >  */}

 <View style = {styles2.kontener}>
  <WebView
  // source = {{ uri: this.state.myalamat }}
  source = {{ uri: myalamat }}
  />
   </View>
   
   {/* </Tab> */}
   {/* <Tab 
         heading={ <TabHeading style={{backgroundColor:  "#008C45" }}
          >
            <Image source={require('../../../assets/images/historycoin.png')}  style={styles.ImageStyle} />
                <Text>History Point</Text>
            </TabHeading>}
          > 
 <View style = {styles2.kontener}>
  <WebView
  source = {{ uri: myalamat2 }}
  />
   </View>
   </Tab> 

// </Tabs>*/}




{
 this.state.isLoading ? 
  <View style={styles.spinnercontainer}>
    <Spinner/>
    <Text style={styles.spinnertext}>Loading...</Text>
  </View>
  : false
  }
      </Container>
      </StyleProvider>
    );
  }  


}




const styles2 = StyleSheet.create({
  container: {
    // flex: 1,
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start', // if you want to fill rows left to right
    // marginTop: 2,
    // marginBottom: 2
  },
  kontener: {
    flex:1 ,
    height: '80%'
 },
 item1: {
     width: '75%', // is 50% of container width
     justifyContent: 'center',
     alignItems: 'center',
     paddingTop: 10
   },
   item2: {
    width: '25%', // is 50% of container width
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingTop: 10
    }  
})


export default ChatView;
