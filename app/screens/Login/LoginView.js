import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, Image, ImageBackground, Alert, KeyboardAvoidingView } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import Logo from '../../components/Logo';
import LinearGradient from 'react-native-linear-gradient';
import {Spinner} from 'native-base';

// import {encrypt, decrypt, url} from '../../../assets/lib/brosky';
import {encrypt, decrypt, getUrl, version} from '../../../assets/lib/brosky';
import Axios from 'axios';// untuk http request
import AsyncStorage from '@react-native-community/async-storage'; //untuk simpan local variable
import {ToastAndroid} from 'react-native';

let url;

// const url = 'https://5070950630364455.ap-southeast-1.fc.aliyuncs.com/2016-08-15/proxy/mostrans/mt_login/';
const role = version.app ;  //"DRIVER";
// let role = 'DRIVER' ; 

class LoginView extends Component {

    constructor(props){
        super(props);
        this.state={
          identity: ' ',
          password: ' ',
          isLoading: false
        }
      }
      

      async componentDidMount(){
        url = await getUrl();
      }

      async _login(){
        this.setState({isLoading: true});
        //this.props.navigation.navigate('MainActivity');

        //this.props.navigation.replace('Home');  //untuk bypass internet

        let encrypted_identity = "";
        let encrypted_password = "";
        await encrypt(this.state.identity.toLowerCase()).then((result)=>{
          encrypted_identity = result;
        });
        await encrypt(this.state.password).then((result)=>{
          encrypted_password = result;
        });
        console.log(1);
        console.log(encrypted_password);
          console.log(encrypted_identity);
          console.log(2);

        console.log("**START LOG URL LOGIN");
        console.log(url.login);
        console.log(encrypted_identity);
        console.log(encrypted_password);
        console.log(role);
        console.log("**END LOG URL LOGIN");

        // query: diencrypt, json
        Axios.post(url.login,{
          identity: encrypted_identity,
          password: encrypted_password,
          role: role
        })
        .then((response)=>{
          
          let temp = response.data;
          console.log(temp);
          if(temp.status == 'success') {
            AsyncStorage.setItem("uid", temp.data, ()=>{
              this.props.navigation.replace('Home');
            });
          }
          else ToastAndroid.showWithGravityAndOffset(
            'Informasi Login Anda salah!',
            ToastAndroid.LONG,
            ToastAndroid.CENTER,
            25,
            50,
          );
          this.setState({isLoading: false});
                    // else Alert.alert(temp.status, "Detail Login anda salah. "+ temp.data);
        })
        .catch(err=>{
          console.log("error login")
          console.log(err);
          this.setState({isLoading: false});
        });
      }

    navigate = () => {
        this.props.onLogin('username', 'password');
    };

    render() {
        return (        
              
<KeyboardAvoidingView style={styles.container} keyboardVerticalOffset={-50} behavior="padding">
            {/* <View style={{width: '100%', height: 200, position: 'absolute', bottom: 0}}> */}
            <View style={{width: '100%', height: 500, position: 'absolute', bottom: 0}}>
              <Image resizeMode='cover' source={require('../../../assets/images/backgroundz.png')} style={{flex: 1, width: undefined, height: undefined, opacity: 0.5}}/>
            </View>
            <Logo/>
    				<View style={styles.loginForm}>
                {/* <Text style={styles.signTitle}>Sign in to DRIVER/KERANI</Text> */}
                <Text style={styles.signTitle}>Sign in to { version.app }</Text>
                <Text style={styles.signTitle}> </Text>
                {/* <Text style={styles.signTitle}>Version 1.3</Text> */}
                <LinearGradient 
                /*colors={['#ffffff', '#ffffff', 'rgba(47, 245, 166, 0.1)']} */
                colors={['#ffffff', '#ffffff', '#ffffff']}
                style={styles.inputBox}>
                    <View style={styles.SectionStyle}>
                        <Image source={require('../../../assets/images/people.png')} style={styles.ImageStyle} />
                        <TextInput
                          style={{flex:1}}
                          underlineColorAndroid='rgba(0,0,0,0)'
                          placeholder="Email | No Handphone"
                          placeholderTextColor = "#1e2427"
                          selectionColor="#1e2427"
                          keyboardType="email-address"
                          onSubmitEditing={()=> this.password.focus()}
                          onChangeText={(input)=>this.setState({identity:input})}
                        />
                    </View>
                </LinearGradient>
                <LinearGradient 
                /*colors={['#ffffff', '#ffffff', 'rgba(47, 245, 166, 0.1)']} */
                colors={['#ffffff', '#ffffff', '#ffffff']} 
                style={styles.inputBox}>
                    <View style={styles.SectionStyle}>
                        <Image source={require('../../../assets/images/lock.png')} style={styles.ImageStyle} />
                        <TextInput
                          style={{flex:1}}
                          underlineColorAndroid='rgba(0,0,0,0)'
                          placeholder="Password"
                          secureTextEntry={true}
                          placeholderTextColor = "#1e2427"
                          ref={(input) => this.password = input}
                          onChangeText={(input)=>this.setState({password:input})}
                        />
                    </View>
                </LinearGradient>
                <TouchableOpacity style={styles.button} onPress={()=>this._login()}>
                    <Text style={styles.buttonText}>MASUK</Text>
                </TouchableOpacity>
                <View style={styles.signupTextCont}>
    					{/* <TouchableOpacity onPress={this.signup}><Text style={styles.signupButton}> *Lupa Password</Text></TouchableOpacity> */}
    					<TouchableOpacity onPress={this.signup}><Text style={styles.signupButton}>Version {version.version} {version.type}</Text></TouchableOpacity>
    				</View>
        		</View>
            { this.state.isLoading ? 
            <View style={styles.spinnercontainer}>
              <Spinner/>
              <Text style={styles.spinnertext}>Loading...</Text>
            </View>
            : false
            }
    			</KeyboardAvoidingView>
        );
    }
}

LoginView.propTypes = {
    onLogin: PropTypes.func
};

export default LoginView;
