import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container : {
    /*backgroundColor: '#b9babe', '#f3f3f3',*/
    flex: 1,
    alignItems:'center',
    justifyContent :'flex-start',
    backgroundColor: '#D1EDF111',
    paddingTop: '10%'
  },

  loginForm : {
    // flexGrow: 4,
    justifyContent:'center',
    alignItems: 'center',
    marginTop: 30
  },
  inputBox: {
    width:300,
    backgroundColor:'rgba(255, 255,255,0.2)',
    borderRadius: 12,
    paddingHorizontal:16,
    fontSize:16,
    color:'#ffffff',
    marginVertical: 10
  },
  button: {
    width:300,
    backgroundColor:'#008D45',
     borderRadius: 10,
      marginVertical: 10,
      paddingVertical: 13
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  },
  signTitle: {
    color: 'black',
    fontFamily: 'Gadugi',
    fontSize: 21,
    fontWeight: 'bold',
    textAlign:'center'
  },
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  ImageStyle: {
    alignItems: 'center',
    height: 20,
    width: 20,
    resizeMode: 'contain'
  },
  spinnercontainer:{
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: '#00000033',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default styles;
