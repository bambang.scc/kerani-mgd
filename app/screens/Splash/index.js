import React, {Component} from 'react';
import {Text, View, Image, BackHandler, Alert, Linking} from 'react-native';
import {Container, Content, Spinner} from 'native-base';
import styles from './styles'
import AsyncStorage from '@react-native-community/async-storage';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import {encrypt, decrypt, getUrl, setUrl, version} from '../../../assets/lib/brosky';
import Axios from 'axios';// untuk http request

let url;

export default class Splash extends Component{

  checkVersion = async (version,app) =>{
    console.log('check version start');
    let id_app = "market://details?id=com.mostrans.kerani";
    let x = await encrypt("select update from mt_master_version where version='" + version + "' and app='" + app + "'");
    await Axios.post(url.select,{
      query: x
    }).then(value=>{
      let x = value.data.data;
      if(x.length === 0 ) Alert.alert('Error', 'Versi tidak sesuai. Silahkan install ulang aplikasi di Play Store',[
        {'text': 'Ok', onPress: ()=>{
          BackHandler.exitApp();
          Linking.openURL(id_app);
        }}
      ], {cancelable: false});
      else {
        if(x[0].update === 'N') return true;
        else Alert.alert('New Updates Available', 'Mohon Update Aplikasi untuk menggunakan Mostrans.',[
          {'text': 'Ok', onPress: ()=>{
            BackHandler.exitApp();
            Linking.openURL(id_app);
          }}
        ], {cancelable: false});
      }
  
    })
    .catch(err=>{
      console.log('Error Check Version');
      console.log(err.Text);
    })    
    ;
    console.log('check version end');
  }

  close(){
    BackHandler.exitApp();
  }
  async componentDidMount(){
    let x = await setUrl();
    url = await getUrl();
    await this.checkVersion(version.version,version.app);
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 1000, fastInterval: 2000})
    .then( data=>{
      this.interval = setInterval(()=>{
        AsyncStorage.getItem("uid", (error, value)=>{
          if(value != null) this.props.navigation.replace('Home');
          else this.props.navigation.replace('Login');
        });
      },1000);
    })
    .catch(err=>{
      Alert.alert("ERROR", "Anda harus menghidupkan GPS untuk melanjutkan.", [
        {
          text: 'Ok',
          onPress: ()=>{
            this.close();
          },
        }
      ], {
        cancelable: false
      });
    })
  }
  componentWillUnmount(){
    clearInterval(this.interval);
  }
    render() {
      return (
        <Container>
          <Content contentContainerStyle={styles.container}>
            {/* <Text style={styles.textstyle}>"Integrated Transportation Business Solution"</Text> */}
            {/* <View style={{width: '100%', height: 200, position: 'absolute', bottom: 0}}> */}
            <View style={{width: '100%', height: 500, position: 'absolute', bottom: 0}}>
              <Image resizeMode='cover' source={require('../../../assets/images/backgroundz.png')} style={{flex: 1, width: undefined, height: undefined, top: 20, opacity: 0.5}}/>
            </View>
            <View style={{position: 'absolute', top: '15%'}}>
            <Image source={require('../../../assets/images/logo.png')} style={styles.logo}/>
            </View>
            {/* <Spinner style={{marginTop: -50}}/> */}
          </Content>
        </Container>
      );
    }
  }