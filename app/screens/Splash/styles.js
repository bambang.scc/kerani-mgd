import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container:{
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
      backgroundColor: '#D1EDF111'
    },
    logo:{
      height: 200,
      width: 200
    },
    textstyle:{
      color: 'white',
      marginTop: 30,
      fontSize: 15
    }
  });
  
export default styles;