import { createStackNavigator, createAppContainer } from 'react-navigation';

import Login from 'app/screens/Login';
import Home from 'app/screens/Home';
import Profile from 'app/screens/Profile';
import Chat from 'app/screens/Chat';
import Trip from 'app/screens/Trip';
import Splash from 'app/screens/Splash';

const RNApp = createStackNavigator(
    {
        Login: {
            screen: Login,
            navigationOptions: { header: null, gesturesEnabled: false }
        },
        Home: {
            screen: Home,
            navigationOptions: { header: null, gesturesEnabled: false }
        },
        Chat: {
            screen: Chat,
            navigationOptions: { header: null, gesturesEnabled: false }            
        },
        Profile: {
            screen: Profile,
            navigationOptions: { header: null, gesturesEnabled: false }            
        },
        Trip: {
            screen: Trip,
            navigationOptions: { header: null, gesturesEnabled: false }
        },
        Splash: {
            screen: Splash,
            navigationOptions: { header: null, gesturesEnabled: false },
        }
    },
    {
        initialRouteName: 'Splash',
        // defaultNavigationOptions: {
        //     cardStyle: { backgroundColor: '#008C45' },
        //   },        
        // headerMode: 'screen',
        // cardStyle: { backgroundColor: 'White' }        
    }
);

export default createAppContainer(RNApp);
