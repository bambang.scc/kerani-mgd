/* App config for apis
 */
const ApiConstants = {
    BASE_URL: 'https://api.enseval.com/',
    LOGIN: 'mostrans/users/login'
};

export default ApiConstants;
